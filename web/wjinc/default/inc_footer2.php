<div class="jc-footer">
			<div class="footer-cn js-lazy">
				<div class="cnRight">
					<div class="cnTop">
						<div class="cn-list">
							<h3>账户相关</h3>
							<ul>
								<li> <a href="/index3">如何注册账号</a></li>
								<li><a href="/index3">怎么找回登录密码</a></li>
								<li><a href="/index3">如何修改手机号码</a></li>
								<li><a href="/index3">如何修改真实姓名</a></li>
							</ul>
						</div>
						<div class="cn-list">
							<h3>充值购彩</h3>
							<ul>
								<li><a href="/index3">如何进行充值</a></li>
								<li><a href="/index3">如何购买彩票</a></li>
								<li><a href="/index3">如何查询购彩记录</a></li>
								<li><a href="/index3">充值没到账怎么办</a></li>
							</ul>
						</div>
						<div class="cn-list">
							<h3>兑奖提款</h3>
							<ul>
								<li><a href="/index3">怎样进行兑奖</a></li>
								<li><a href="/index3">如何进行提款</a></li>
								<li><a href="/index3">提款是否收手续费</a></li>
								<li><a href="/index3">提款不成功怎么办</a></li>
							</ul>
						</div>
						<div class="cn-list service">
							<h3>在线客服</h3>
							<ul>
								<li>QQ咨询：
									<a href="tencent://message/?uin=917500500&amp;Site=500vip彩票&amp;Menu=yes">客服1</a>
									<a href="tencent://message/?uin=917500500&amp;Site=500vip彩票&amp;Menu=yes">客服2</a>
									<a href="tencent://message/?uin=917500500&amp;Site=500vip彩票&amp;Menu=yes">客服3</a>
								</li>
								<li>在线咨询时间：7*24小时</li>
							</ul>
						</div>
					</div>
					<div class="cnBtn">
						<p><span class="explain01 icon"></span>账户安全</p>
						<p><span class="explain02 icon"></span>购彩便捷</p>
						<p><span class="explain03 icon"></span>兑奖简单</p>
						<p><span class="explain04 icon"></span>提款快速</p>
					</div>
				</div>
			</div>
			<div class="clear nospace"></div>
			<div style="display:none;">
				<div class="success_box" id="jq_tip_window_box">
					<div class="alert_border">
						<div class="alert_box">
							<div>
								<div class="success_close" title="关闭"></div>
								<div class="clear"></div>
							</div>
							<div class="alert_main">
								<div class="icon_success"></div>
								<div class="alert_content success_content">
									<h4 class="red"></h4>
									<div class="detail"></div>
								</div>
								<div class="clear"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div style="display:none;">
				<div class="alert_window_box" id="jq_alert_window_box">
					<div class="alert_border">
						<div class="alert_box">
							<div class="alert_top">
								<div class="alert_title">温馨提示</div>
								<div class="alert_close" title="关闭" onclick="jQuery.colorbox.close();"></div>
								<div class="clear"></div>
							</div>
							<div class="alert_main">
								<div class="alert_icon"></div>
								<div class="alert_content">
									<h4></h4>
									<div class="detail"></div>
								</div>
								<div class="clear"></div>
								<div class="alert_push"></div>
								<div class="alert_sbt"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>