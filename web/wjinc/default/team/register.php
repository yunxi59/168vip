<!DOCTYPE html>
<html>
<head lang="en">
	<?php $this->display('inc_base_lr.php',0,'用户注册') ?>
    <link href="/skin/new/css/register.css"  type="text/css" rel="stylesheet" />
</head>
<body>
<?php $this->display('inc_header_lr.php',0,'用户注册'); ?>
    <div class="pageBady custom clearfix"> 
<div class="w12000 clearfix">
    <div class="fl register-main">
        <div class="registerTit clearfix">
            <ul>
                <li class="on">用户注册<i></i></li>
            </ul>
        </div>
        <div class="registerBox clearfix">
            <div id="mobileReg-form" class="reg-from"  >
         <?php if($args[0] && $args[1]){
		$sql="select * from {$this->prename}links where lid=?";
		$linkData=$this->getRow($sql, $args[1]);
		$sql="select * from {$this->prename}members where uid=?";
		$userData=$this->getRow($sql, $args[0]);	
		?>
		<form action="/index.php/user/registered" method="post" onajax="registerBeforSubmit" enter="true" call="registerSubmit" target="ajax">
        	<input type="hidden" name="parentId" value="<?=$args[0]?>" />
            <input type="hidden" name="lid" value="<?=$linkData['lid']?>"  />
                <div class="pr ui-form-item">
                <i class="ico-iphone"></i>
                    <input class="ui-input" type="text" name="username" maxlength="11" id="username" placeholder="请输入您的用户名" data-explain="用户名须是6-12位数字+字母组合。" />
                    <div class="ui-form-explain" style="display: none">
                        <em class="ui-form-arrow"></em>
                    </div>
                </div>
                <div class="pr ui-form-item mt">
                    <i class="ico-pw"></i>
                    <i class="ico-jp"></i>
                    <input class="ui-input" type="password" name="password" maxlength="16" id="password" placeholder="请设置密码" data-explain="6~16位组成，建议使用字母和数字混合。" />
                    <div class="ui-form-explain" style="display: none; width: 230px">
                        <em class="ui-form-arrow"></em>
                    </div>
                </div>
                <div class="pr ui-form-item mt">
                    <i class="ico-pw"></i>
                    <i class="ico-jp"></i>
                    <input class="ui-input" type="password" name="cpasswd" maxlength="16" id="cpasswd" placeholder="请再次输入您设置的密码" data-explain="请再次输入密码" />
                    <div class="ui-form-explain" style="display: none; width: 100px;">
                        <em class="ui-form-arrow"></em>
                    </div>
                </div>
				<div class="pr ui-form-item mt">
                    <i class="ico-ide"></i>
                    <input class="ui-input" type="text" maxlength="11" name="qq" id="qq" placeholder="请输入QQ号码" data-explain="请输入QQ号码" />
                    <div class="ui-form-explain" style="display: none">
                        <em class="ui-form-arrow"></em>
                    </div>
                </div>
				<div class="pr ui-form-item mt item149">
                    <input class="ui-input" type="text" maxlength="6" name="vcode" id="vcode" placeholder="请输入验证码" data-explain="请输入图片中的字符，不区分大小写。" />
                    <span style="position: absolute;right:-140px;top:0;" ><img width="130" height="40" border="0" style="cursor:pointer;margin-bottom:0px;" alt="验证码" align="absmiddle" src="/index.php/user/vcode/<?=$this->time?>"/></span>
                    <div class="ui-form-explain" style="display: none; width: 220px">
                        <em class="ui-form-arrow"></em>
                    </div>
                </div>
                <div class="registerBtn mt" onClick="$(this).closest('form').submit()">注册</div>
              
	    </form>
           <?php }else{?>
           <div style="font-size:20px; color:#999999"><b>该代理注册链接已失效！可联系客服或前往500vip首页自主注册！
		   <br/><br/>
		   <span style="margin-left:190px">感谢您对500vip的支持！</span>
		   <br/><br/>
		   <span style="margin-left:150px">500vip将提供更好更优质的服务！</span>
		   <br/><br/>

		   </div>
           <?php }?>
            </div>
        </div>
    </div>
    <div class="fl register-r">
        <h2>已经注册，有账号<a href="/index.php" >立即登录</a></h2>
		
</div></div>

</body>
</html>