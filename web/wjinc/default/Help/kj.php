<!DOCTYPE html>
<html>
<head lang="en">
<?php $this->display('inc_base.php',0,'500 ') ?>
<style type="text/css">
#help_content {
	width: 1000px;
	margin: 0 auto;
}
#help_page_link {
	color: #0000ff;
	font-size: 14px;
	margin: 5px 0 0 18px;
}
#help_page_solgan {
	color: #7c7c7c;
	font-size: 18px;
	margin: 0px 0 0 18px;
	padding-top: 20px;
}
#help_page_container {
	width: 943px;
	border: 1px solid #CCC;
	margin: 10px 18px;
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	border-radius: 6px;
}
#help_page_container table {
	width: 943px;
	margin: 10px auto;
}
.table_text {
	font-size: 14px;
}
.table_text02 {
	font-size: 14px;
	color: #006;
	font-weight: bold;
}
.table_text03 {
	font-size: 14px;
	color: #666;
	position: relative;
	left: 20px;
}
.table_text tr {
	border-top: #fff 2px solid;
	border-bottom: #fff 2px solid;
}
.table_text td {
	border-left: #fff 2px solid;
	border-right: #fff 2px solid;
	padding: 5px 10px 5px 10px;
	line-height: 20px;
}
.help-seep {
	margin-top: 9px;
	margin-bottom: 20px;
	margin-left: 37px;
}
.help-seep li {
	float: left;
	margin-right: 19px;
	padding: 5px;
	width: 206px;
	height: 127px;
	border: 1px solid #f3f4f4;
}
.help-seep li .seep-item {
	padding: 24px 5px 0 20px;
	height: 103px;
	background: #f3f4f4;
}
.help-seep li .seep-item a {
	display: inline-block;
	margin-top: 20px;
	margin-left: 10px;
	color: #4a9cdc;
	text-align: center;
}
.help-seep li .seep-item em {
}
.help-seep li .ico {
	float: left;
	display: inline-block;
	margin-right: 10px;
	width: 60px;
	height: 62px;
	background: url("/images/new/help_ico01.png") no-repeat;
}
.help-seep li .ico01 {
	background-position: 0 0;
}
.help-seep li .ico02 {
	background-position: 0 -65px;
}
.help-seep li .ico03 {
	background-position: 0 -132px;
}
.help-seep li .ico04 {
	background-position: 0 -197px;
}
.help-seep li .desc {
	float: left;
}
</style>
<link rel="stylesheet" href="/skin/laofan/pclist.css">
</head>
<?php $this->display('inc_header.php') ?>

<div id="help_content">
  <div id="_iframe_divs" class="_iframe_divs_">
    <div id="_games_2">
      <div class="trend-box">
        <div class="trend-title">
          <h2>时时彩</h2>
        </div>
        <div class="trend-main">
          <ul class="trend-lot-ul">
            <li>
              <div class="lottery-logo lottery-logo-big"> <a href='/index.php/chartssc?type=27&count=30' target="_blank"> <img src="/skin/laofan/images/51.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 三分时时彩 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a href='/index.php/chartssc?type=27&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
              <div class="lottery-logo lottery-logo-big"> <a href='/index.php/chartssc?type=1&count=30' target="_blank"> <img src="/skin/laofan/images/5.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 重庆时时彩 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a href='/index.php/chartssc?type=1&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
              <div class="lottery-logo lottery-logo-big"> <a href='/index.php/chartpk?type=20&count=30' target="_blank"> <img src="/skin/laofan/images/9.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 北京PK拾 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a href='/index.php/chartpk?type=20&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
              <div class="lottery-logo lottery-logo-big"> <a href='/index.php/chartpk?type=12&count=30' target="_blank"> <img src="/skin/laofan/images/7.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 新疆时时彩 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a href='/index.php/chartpk?type=12&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
              <div class="lottery-logo lottery-logo-big"> <a href='/index.php/chartpk?type=14&count=30' target="_blank"> <img src="/skin/laofan/images/7.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 五分时时彩 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a href='/index.php/chartpk?type=14&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
              <div class="lottery-logo lottery-logo-big"> <a href='/index.php/chartpk?type=26&count=30' target="_blank"> <img src="/skin/laofan/images/7.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 两分时时彩 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a href='/index.php/chartpk?type=26&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
              <div class="lottery-logo lottery-logo-big"> <a href='/index.php/chartpk?type=5&count=30' target="_blank"> <img src="/skin/laofan/images/7.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 急速分分彩 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a href='/index.php/chartpk?type=5&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
              <div class="lottery-logo lottery-logo-big"> <a href='/index.php/chartpk?type=18&count=30' target="_blank"> <img src="/images/index/10fens.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 重庆幸运农场 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a href='/index.php/chartpk?type=18&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>	
		
          </ul>
        </div>
      </div>
      <div class="trend-box">
        <div class="trend-title">
          <h2>快三</h2>
        </div>
        <div class="trend-main">
          <ul class="trend-lot-ul">
            <li>
              <div class="lottery-logo lottery-logo-big"> <a href='/index.php/chartk3?type=25&count=30' target="_blank"> <img src="/skin/laofan/images/10.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 江苏快三 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a href='/index.php/chartk3?type=25&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
              <div class="lottery-logo lottery-logo-big"> <a href='/index.php/chartk3?type=52&count=30' target="_blank"> <img src="/skin/laofan/images/17.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 广西快三 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a href='/index.php/chartk3?type=52&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
			   <div class="lottery-logo lottery-logo-big"> <a href='/index.php/chartk3?type=60&count=30' target="_blank"> <img src="/skin/laofan/images/17.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 吉林快三 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a href='/index.php/chartk3?type=60&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
						   <div class="lottery-logo lottery-logo-big"> <a href='/index.php/chartk3?type=61&count=30' target="_blank"> <img src="/skin/laofan/images/17.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 河北快三 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a href='/index.php/chartk3?type=61&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
						   <div class="lottery-logo lottery-logo-big"> <a href='/index.php/chartk3?type=61&count=30' target="_blank"> <img src="/skin/laofan/images/17.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 蒙古快三 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a href='/index.php/chartk3?type=62&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
						   <div class="lottery-logo lottery-logo-big"> <a href='/index.php/chartk3?type=63&count=30' target="_blank"> <img src="/skin/laofan/images/17.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 安徽快三 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a href='/index.php/chartk3?type=63&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
						   <div class="lottery-logo lottery-logo-big"> <a href='/index.php/chartk3?type=64&count=30' target="_blank"> <img src="/skin/laofan/images/17.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 福建快三 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a href='/index.php/chartk3?type=64&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
						   <div class="lottery-logo lottery-logo-big"> <a href='/index.php/chartk3?type=65&count=30' target="_blank"> <img src="/skin/laofan/images/17.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 甘肃快三 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a href='/index.php/chartk3?type=65&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
						   <div class="lottery-logo lottery-logo-big"> <a href='/index.php/chartk3?type=66&count=30' target="_blank"> <img src="/skin/laofan/images/17.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 贵州快三 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a href='/index.php/chartk3?type=66count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
						   <div class="lottery-logo lottery-logo-big"> <a href='/index.php/chartk3?type=67&count=30' target="_blank"> <img src="/skin/laofan/images/17.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 河南快三 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a href='/index.php/chartk3?type=67&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
						   <div class="lottery-logo lottery-logo-big"> <a href='/index.php/chartk3?type=69&count=30' target="_blank"> <img src="/skin/laofan/images/17.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 上海快三 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a href='/index.php/chartk3?type=69&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
						   <div class="lottery-logo lottery-logo-big"> <a href='/index.php/chartk3?type=70&count=30' target="_blank"> <img src="/skin/laofan/images/17.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 北京快三 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a href='/index.php/chartk3?type=70&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
              <div class="lottery-logo lottery-logo-big"> <a href='/index.php/chartk3?type=50&count=30' target="_blank"> <img src="/skin/laofan/images/11.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 湖北快三 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a href='/index.php/chartk3?type=50&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
          </ul>
        </div>
      </div>
      <div class="trend-box">
        <div class="trend-title">
          <h2>十一选5</h2>
        </div>
        <div class="trend-main">
          <ul class="trend-lot-ul">
            <li>
              <div class="lottery-logo lottery-logo-big"> <a href='/index.php/chartk3?type=7&count=30' target="_blank"> <img src="/skin/laofan/images/12.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 山东11选5 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a href='/index.php/chartk3?type=7&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
              <div class="lottery-logo lottery-logo-big"> <a href='/index.php/chartk3?type=6&count=30' target="_blank"> <img src="/skin/laofan/images/15.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 广东11选5 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a href='/index.php/chartk3?type=6&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
             <!-- <div class="lottery-logo lottery-logo-big"> <a href='/index.php/chartk3?type=16&count=30' target="_blank"> <img src="/skin/laofan/images/15.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 江西多乐彩 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a href='/index.php/chartk3?type=16&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>-->
              <div class="clear"></div>
            </li>			
          </ul>
        </div>
      </div>
      <div class="trend-box">
        <div class="trend-title">
          <h2>低频彩</h2>
        </div>
        <div class="trend-main">
          <ul class="trend-lot-ul">
            <li>
              <div class="lottery-logo lottery-logo-big"> <a href='/index.php/chartk3?type=9&count=30' target="_blank"> <img src="/skin/laofan/images/1.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 福彩3D </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a href='/index.php/chartk3?type=9&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
              <div class="lottery-logo lottery-logo-big"> <a href='/index.php/chartk3?type=10&count=30' target="_blank"> <img src="/skin/laofan/images/2.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 排列三 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a href='/index.php/chartk3?type=10&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
              <div class="lottery-logo lottery-logo-big"> <a href='/index.php/chartpk?type=30&count=30' target="_blank"> <img src="/images/index/p3s.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 香港六合彩 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a href='/index.php/chartpk?type=30&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>				
          </ul>
        </div>
      </div>
    </div>
    <div class="div_iframe" name="iframe_div"></div>
  </div>
</div>
<?php $this->display('inc_footer.php') ?>
</body></html>
