<!DOCTYPE html>
<html>
<head>
<?php $this->display('inc_base.php',0,'1') ?>
	<link href="/skin/new/css/index.css" type="text/css" rel="stylesheet" />
	<link type="text/css" rel="stylesheet" href="/500vip/statics/css/_home.css?version=2.7"  >
		<link type="text/css" rel="stylesheet" href="/common/statics/css/_tip.css?version=2.7" >
		<link type="text/css" rel="stylesheet" href="/common/statics/css/fonts/style.css?version=2.7" >
		<link type="text/css" rel="stylesheet" href="/500vip/statics/css/home/index.css?version=2.7" >
		
		<script>
			try {
				//下面是静态资源url的前缀
				if(typeof(_prefixURL) != "object") {
					window._prefixURL = {
						statics: "/500vip/statics",
						common: "/common/statics"
					};
				}
			} catch(e) {console.log(e);}
			try {
				//注：下一行代码毋删除，或重复定义，或在其他文件出现一样的代码 ,也不要在其他文件定义window.name（  __openWin 方法会用到这个变量）
				if(!window.opener) {
					window.name = "wn_1_home_first";
				} else if(window.name != "wn_1_home_first") {
					window.name = "wn_1_home_first";
				}
			} catch(e) {console.log(e);}
		</script>
		<script type="text/javascript" src="/common/statics/js/jquery-1.11.2.min.js"></script>
		<script type="text/javascript" src="/common/statics/js/_user_.js?_v=1"></script>
		<script type="text/javascript" src="/500vip/statics/js/_home_menu.js?version=2.7"></script>
		
</head>

<body ><div class="bg"style="
    background: url(/images/index/wrap_bg.jpg) top no-repeat;
">
<style>
.topNav{margin:30px auto 0px auto;width:100%;}
</style>
<?php $this->display('inc_header.php') ?>
    <script type="text/javascript">
	    //added by kent 2016 09.01
	    $(document).ready(function () {
	        $('#lotterysList').hide();
	        $('#lotterys').mouseover(function () {
	            $('#lotterysList').show();
	        }).mouseout(function () {
	            $('#lotterysList').hide();
	        });
	        //added by ian
	        $('#lottery-list-box').on('mouseover', '.allGames', function (e) {
	            var tag = $(this).data('type');
	            $(this).addClass('allGames-on').siblings('li').removeClass('allGames-on').find('.moreGames').css({'display': 'none'});
	            $('#moreGames_' + tag).css({'display': 'block'});
	            $('#lotterysList').addClass('lotterys-list-hd-border1');
	            $(this).find('.icon').hide();
	            $(this).find('.line-fff').show();
	        });
	        $('#lottery-list-box').on('mouseout', '.allGames', function (e) {
	            $(this).removeClass('allGames-on').find('.moreGames').css({'display': 'none'});
	            $('#lotterysList').removeClass('lotterys-list-hd-border1');
	            $(this).find('.icon').show();
	            $(this).find('.line-fff').hide();
	        });
	    });

        // below from @ Moda 2016-09-02
        $(function(){
	        //彩票列表
	        _home_menu.getGameList("#lottery-list-box");
	        //检查是否登录并显示余额和用户id
	        _home_menu.checkLogin();
	        //跑马灯公告
	        _home_menu.getMqData('#sys_tip_outer');
	        
	        //高亮头部menu
			var currenturl = window.location.href;
			var aurl=$('#head_menu a');
			for (var i = 0; i < aurl.length; i++) {
			    var url = aurl.eq(i).data('url'); 
			    if( "/" == url ){
	                    continue;
	               }			    
			    if (currenturl.indexOf(url) != -1) { /*如果链接的href值在当前页面地址中有匹配*/
			        aurl.eq(i).parent().addClass('on');
			    }
			}	        
	    });
	</script>      
    <style>
	* { margin:0; padding: 0 }
	.wap-bg { background: url(/500vip/statics/images/phone/apptwo_bg.jpg) no-repeat top center; height: 672px; position: relative; }
	.wap-wrapper { width:1000px; margin:auto; position: relative; z-index: 1;}
	.wap-phone { background: url(/500vip/statics/images/phone/qpp_phone.png) no-repeat; width: 380px; height: 408px; margin:67px 0 0 31px; }

	.wap-left { width:420px; float: left; position: relative; }
	.wap-right,.app-right { width: 528px; float: right; position: relative; }
	.app-right { width: 475px }
	.wap-paper { background: url(/500vip/statics/images/phone/wap_paper.png) no-repeat; width:247px; height: 159px; background-size: 247px 159px; position: absolute; left:90px; top: 33px; z-index: 5; }
	.wap-paper-pic { background: url(/500vip/statics/images/phone/ico_first_cp2.png); width: 419px; height: 76px; background-size: 419px 76px; position: absolute; left:40px ; top: 36px; z-index: 5;}
	.wap-tit { background: url(/500vip/statics/images/phone/right-text.png) no-repeat; width: 492px; height: 244px; background-size: 492px 244px; margin:70px 0 0 23px;}
	.wap-two { margin-top:47px; overflow: hidden;  }
	.wap-two-left { float: left; width: 315px; font:20px/24px Microsoft Yahei; }
	.wap-two-txt { color: #cf1d29; font-size: 20px }
	.wap-search { margin-top:30px; }
	.wap-two-right { float: left; width:198px; text-align: center; font:14px/24px Microsoft Yahei; color:#cf1d29;margin-left: 56px;margin-top: 6px;}
	.wap-two-left{ float: left; width:198px; text-align: center; font:14px/24px Microsoft Yahei; color:#cf1d29;margin-left: 30px;margin-top: 6px;}
	.wap-two-bg { background: url(/500vip/statics/images/phone/wap_twopic.png) no-repeat; background-size: 198px 198px; width: 198px; height: 198px; padding:15px; box-sizing: border-box; position: relative;  }
	.wap-two-bg img { width:167px; height: 167px; }
	.wap-search-txt { background: url(/500vip/statics/images/phone/app_search.png) no-repeat; width:292px; height: 42px; color:#cf1d29; line-height: 42px; padding-left: 35px; box-sizing: border-box; position: relative; margin-top:20px; }
	.wap-search-txt a { width:57px; height: 42px; float: right;  }

	.line-parallax {width: 100%; position: absolute; top: 0; left: 0px; height: 672px;transform: translate3d(0px, 0px, 0px); transform-style: preserve-3d; backface-visibility: hidden;}
	.layer { transform: translate3d(-2%,0.1%,0);  width: 100%; height: 100%; display: block; position:absolute; left: 0; top: 0; transform-style: preserve-3d; backface-visibility: hidden;}
	.sectiOneLine1 { background: url(/500vip/statics/images/phone/ico_bg_line1.png) no-repeat; width: 237px; height: 117px; background-size: 237px 117px; position: absolute; left: 20px; top: 89px; z-index: 99 }
	.sectiOneLine2 { background: url(/500vip/statics/images/phone/ico_bg_line2.png) no-repeat; width: 269px; height: 160px; background-size: 269px 160px; position: absolute; left:140px; top: 252px; z-index: 99}
	.sectiOneLine3 { background: url(/500vip/statics/images/phone/ico_bg_line1.png) no-repeat; width: 237px; height: 117px; background-size: 237px 117px; position: absolute; right:98px; top: 57px; z-index: 99 }
	.sectiOneLine4 { background: url(/500vip/statics/images/phone/ico_bg_line2.png) no-repeat; width: 269px; height: 160px; background-size: 269px 160px; position: absolute; right:128px; top: 205px; z-index: 99}
	/*11.10xi样式修改*/
	 .app-right {margin-top: -15px;}
	.app-two{width: 490px;}
	.app-phone { background: url("/500vip/statics/images/phone/app_phone.png") no-repeat; width: 482px; height: 428px; background-size: 482px 428px; margin:50px 0 0 30px; box-sizing: border-box; float: left;/*filter: drop-shadow(18px 15px 5px black);*/}
	.app-right .app-tit { background: url(/500vip/statics/images/phone/app-right-text.png) no-repeat; width: 445px; height: 244px; background-size: 445px 244px; margin:70px 0 50px 26px;/*filter: drop-shadow(18px 15px 2px black);*/}
	.app-two-left {width: 515px; margin: -18px 0 0 -10px;}
	.app-btn1,.app-btn2{ float: left; margin-left: 20px; width: 237px;}
	.app_wap_menu { height:50px;  }
	.app_center div { width:50%; background: #ee6f02; float: left; cursor: pointer; }
	.app_center .menu-on { width:50%; background: #dd0303;}
	a.app_menu,a.wap_menu { height:50px; text-align:right; color: #fff; font:20px/50px Microsoft Yahei; box-sizing: border-box; display: block; text-decoration: none; }
	.app_menu { padding-right: 250px; }
	a.wap_menu { text-align: left; padding-left: 250px; }

	.wap-txt { font:32px/54px "Helvetica Neue",Helvetica,Arial,"Microsoft Yahei","Hiragino Sans GB","Heiti SC","WenQuanYi Micro Hei",sans-serif; color: #DA202B; margin: 20px 0; }
	.txt500 { margin:-30px 0 0 25px;}
</style>
<div class="wap-app-ma">
	<div class="wap-bg">

		<div class="app-ma">
			<div class="wap-wrapper">
				<div class="app-phone"></div>
				<div class="app-right">
					<div class="wap-paper-pic"></div>
					<div class="app-tit"></div>
					<div class="txt500"><img src="/500vip/statics/images/phone/500vip.png"></div>
					<div class="app-two" style="margin-top: 30px">
						<div class="app-two-left">
							<div class="app-btn1"><a href="javascript:;" ><img src="/500vip/statics/images/phone/ios_down.png"></a></div>
							<div class="app-btn2"><a href="javascript:;" ><img src="/500vip/statics/images/phone/android_down.png"></a></div>
						</div>
						<div id="app-phone"  class="wap-two-left">
							<div class="wap-two-bg" ><img src="/qrCode/getIosPng.html"></div>
							<p>扫一扫二维码下载Iphone版</p>
						</div>
						<div id="app-android"  class="wap-two-right">
							<div class="wap-two-bg" ><img src="/qrCode/getAndroidPng.html"></div>
							<p>扫一扫二维码下载Android版</p>
						</div>
					</div>
					<!-- 去掉扫码点击事件 -->
					<script>
					// 	$('.app-btn1').click(function(){
					// 		$('#app-phone').fadeIn();
					// 		$('#app-android').hide();
					// 	})
					// 	$('.app-btn2').click(function(){
					// 		$('#app-phone').hide();
					// 		$('#app-android').fadeIn();
					// 	})
					</script>
				</div>
			</div>
		</div>
		<div class="app-ma" style="display: none;">

			<div class="wap-wrapper">
				<div class="wap-left">
					<div class="wap-phone">
						<div class="wap-paper"></div>
					</div>
				</div>
				<div class="wap-right">
					<div class="wap-paper-pic"></div>
					<div class="wap-tit"></div>

					<div class="txt500" style="margin:20px 0 0 60px"><img src="/500vip/statics/images/phone/500vip.png"></div>
				</div>
			</div>
		</div>
		<div class="line-parallax">
			<div class="layer" data-depth="0.2">
				<div class="sectiOneLine1"></div>
			</div>
			<div class="layer" data-depth="0.2">
				<div class="sectiOneLine2"></div>
			</div>
			<div class="layer" data-depth="0.2">
				<div class="sectiOneLine3"></div>
			</div>
			<div class="layer" data-depth="0.2">
				<div class="sectiOneLine4"></div>
			</div>
		</div>

	</div>
	<div class="app_wap_menu">
		<div class="app_center">
			<div class="menu-on">
				<a href="javascript:;" class="app_menu">手机客户端</a>
			</div>
			<div class="">
				<a href="javascript:;" class="wap_menu">触屏版访问</a>
			</div>
		</div>

	</div>
</div>

</html>
<script>
	$('.wap-app-ma').find('.app_center').find('div').click(function(){

		$('.app_center').find('div').attr('class','');
		$('.wap-app-ma').find('.app-ma').hide();

		$(this).attr('class','menu-on');
		$('.wap-app-ma').find('.app-ma').eq($(this).index()).show();
	})
</script>
  <?php $this->display('inc_footer2.php') ?> 
    <?php $this->display('inc_footer3.php') ?>
<!-- 更新浏览器 -->
<div class="ie-alert-wrap" style="display: none;">
    <h1>是时候升级你的浏览器了</h1>
    <p>你正在使用 Internet Explorer 的早期版本（IE9以下版本 或使用该内核的浏览器）。这意味着在升级浏览器前，你将无法访问此网站。</p>
    <hr>
    <h2>请注意：Windows XP 及 Internet Explorer 早期版本的支持已经结束</h2>
    <p>自 2016 年 1 月 12 日起，Microsoft 不再为 IE 11 以下版本提供相应支持和更新。没有关键的浏览器安全更新，您的 PC 可能易受有害病毒、间谍软件和其他恶意软件的攻击，它们可以窃取或损害您的业务数据和信息。请参阅 <a href="https://www.microsoft.com/zh-cn/WindowsForBusiness/End-of-IE-support">微软对 Internet Explorer 早期版本的支持将于 2016 年 1 月 12 日结束的说明</a> 。</p>
    <hr>
    <h2>更先进的浏览器</h2>
    <p>推荐使用以下浏览器的最新版本。如果你的电脑已有以下浏览器的最新版本则直接使用该浏览器访问<b id="referrer"></b>即可。</p>
    <ul class="browser">
      <li class="browser-chrome"><a href="http://www.google.cn/chrome/browser/index.html?hl=zh-CN&standalone=1" target="_blank"> 谷歌浏览器<span>Google Chrome</span></a></li>
      <li class="browser-360"><a href="http://se.360.cn/" target="_blank"> 360安全浏览器 <span>360用户推荐</span></a></li> <!-- 2016-08-06 -->
      <li class="browser-firefox"><a href="http://www.firefox.com.cn/" target="_blank"> 火狐浏览器<span>Mozilla Firefox</span></a></li>
      <li class="browser-ie"><a href="http://windows.microsoft.com/zh-cn/internet-explorer/download-ie" target="_blank"> IE浏览器<span>Internet Explorer</span></a></li>
      <li class="browser-qq"><a href="http://browser.qq.com/" target="_blank"> QQ浏览器9 <span>全新升级版本</span></a></li> <!-- 9.4.8187.400 -->
      
      <div class="clean"></div>
    </ul>
    <hr>
    <h2>为什么会出现这个页面？</h2>
    <p>如果你不知道升级浏览器是什么意思，请请教一些熟练电脑操作的朋友。如果你使用的不是IE6/7/8/9/10，而是360浏览器、QQ浏览器、搜狗浏览器等，出现这个页面是因为你使用的不是该浏览器的最新版本，升级至最新即可。</p>
    <hr>
</div>
<div class="ie-alert-bg" style="display: none;"></div>
<!-- start :  浮动图标  -->
<div id="_leftAD" class="_float_AD l_AD">
    <img src="/500vip/statics/images/home/float_left.png?v=1">
    <div name="close_btn"></div>
    <a class="top1_btn" href="tencent://message/?uin=917500500&amp;Site=500vip彩票&amp;Menu=yes"></a>
    <div class="top2_btn" onclick="__openWin('reg','/register/regPlay.html')" ></div>
    <div class="two_img"><img src="/qrCode/getAndroidPng.html"></div>
</div>
<div id="_rightAD" class="_float_AD r_AD">
    <img src="/500vip/statics/images/home/float_right.png?v=1">
    <div name="close_btn"></div>
    <div class="top1_btn"  onclick="__openWin('other','http://www.cpzxkf.com/')"></div>
    <div class="top2_btn" onclick="__openWin('other','http://www.500vip.cc')"></div>
    <div class="two_img"><img src="/qrCode/getIosPng.html"></div>
</div>
<script type="text/javascript">
    $(function(){
        $("#_leftAD [name=close_btn],#_rightAD [name=close_btn]").click(function(){
            $(this).parent().hide();
        });
    });
</script>
<!-- end :  浮动图标  -->

<!-- 倒计时维护 -->
<div class="maintain-countdown" style="display: none;">
  <div class="maintain-time" id="main_count_down">  
    维护倒计时<br>
    <div id="mian_time"></div>
  </div>
</div>
<script>
/*----------  倒计时功能  ----------*/
//倒计时
leftTime = 0;
interval = 1000;
leftTimeCounter = '';
function checkMaint() {
    $.ajax({
        type: 'POST',
        url: '/index/ajaxMaintData.html',
        data: '',
        dataType: 'json',
        timeout: '30000',
        success: function (data) {
            try
            {
                if ( session_timeout(data) === false )
                {
                    return false;
                }
            } catch(e){ console.log(e);}
            if (data.Result == null || data.Result == undefined || data.Result != true) {//没有正确获得数据
                return false;
            }
            if (data.sysStatus == 0 && data.leftTime > 0) {
                leftTime = data.leftTime;
                leftTimeCounter = setInterval(setLeftTime, interval);
            }
        }
    });
}

function setLeftTime() {
    if (leftTime <= 0) {
        clearInterval(leftTimeCounter);
        $('#main_count_down').parent().css('display', 'none');
        __openWin('home','/index/index.html'); //时间为0时跳转到维护页面
    }
    leftTime--;
    if (leftTime > 1800) {
        return;
    }
    var minute = parseInt(leftTime / 60);
    var second = leftTime % 60;
    var txt = '剩余'+minute+'分'+second+'秒';
    //将 txt 显示到页面上
    $('#main_count_down').parent().css('display', 'block');
    $('#mian_time').text(txt);
}

$(function() {
    checkMaint();
});
</script>
<script>
    //全屏高度，防止内容过短的时候，下面出现白边
    $(function(){
        var winH = document.documentElement.clientHeight?document.documentElement.clientHeight:document.body.scrollHeight;
        $("body>div.wrap-bg").css("minHeight", ( winH - $("body>#header_plus").height() - $("body>.jc-footer").height() ) + "px" );
    })
</script>

   
                                              
</body>

</html>