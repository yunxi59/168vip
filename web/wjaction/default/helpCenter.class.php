<?php
class helpCenter extends WebLoginBase{

	public final function index()
	{		
		$this->display('helpCenter/index.html');
	}

	public final function helpInfo()
	{		
		$this->display('helpCenter/helpInfo.html');
	}
}