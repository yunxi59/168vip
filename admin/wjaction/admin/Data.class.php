<?php

/**
 * 与开奖数据有关
 */
class Data extends AdminBase{
	public $pageSize=15;
	private $encrypt_key='QQ:446284379-CMKGLt#20BO(,u=PHsGr@NI*N/On?u8v(^Pa~Gl#n=0w:%o5G$8QCOsdFf2(yz1Fz:ZB#uAR;pt`4Wg;*$+G<EWhZ~I+@l,k$y5r75Q/)';	// 256位随便密码
	private $dataPort=65531;
	
	public final function index($type){
		$this->type=$type;
		$this->display('data/index.php');
	}
	
	public final function add($type, $actionNo, $actionTime){
		$para=array(
			'type'=>$type,
			'actionNo'=>$actionNo,
			'actionTime'=>$actionTime
		);
		$this->display('data/add-modal.php', 0, $para);
	}
	
	public final function lhc(){
		
		$this->type=30;
	
		$this->display('data/lhc.php');
	
		
		
	}
	
	public final function kj(){
		$para=$_GET;
		print_r($_POST);exit;
		$para['key']=$this->encrypt_key;
		$url=$GLOBALS['conf']['node']['access'] . '/data/kj';
		echo $this->http_post($url, $para);
	}
	
	//幸运卡丁车
	public final function kading(){
		$this->display('data/kading.php');
	}
	//开奖数据中的订单
	public final function kading_order(){
		$this->display('data/kading_order.php');
	}
	
	public final function added(){
		$para=$_POST;
		$para['type']=intval($para['type']);
		$para['key']=$this->encrypt_key;
		
		$url=$GLOBALS['conf']['node']['access'] . '/data/add';
		if(!$this->getValue("select data from {$this->prename}data where type={$para['type']} and number='{$para['number']}'")) $this->addLog(17,$this->adminLogType[17].'['.$para['data'].']', 0, $this->getValue("select shortName from {$this->prename}type where id=?",$para['type']).'[期号:'.$para['number'].']');
		echo $this->http_post($url, $para);
	}
	
	
	public final function lhcadded(){
		//throw '000';
		if($_POST['id']!="" && $_POST['data']!=""){
			
			try{
				$para=array();
				$para['ball']=$_POST['data'];
				$para['actionTime']=$_POST['actionTime'];
				$para['actionNo']=$_POST['actionNo'];
				$para['opentime']=$this->time;
				$this->beginTransaction();
				if($this->updateRows($this->prename .'auto_0', $para,'id='.$_POST['id'])){
					
					
					if($this->getValue("select data from {$this->prename}data where type=30 and number='{$_POST['actionNo']}'")){		
						$paras=array();
						$paras['data']=$_POST['data'];
						$this->updateRows($this->prename .'data', $paras,'type=30 and number='.$_POST['actionNo']);												
					}
					
					$this->commit();
					return "添加成功！";
				}
			}catch(Exception $e){
				$this->rollBack();
				throw $e;
			}
			
		}else{ 
		
			try{
				$para=array();
				$para['actionTime']=$_POST['actionTime'];
				$para['actionNo']=$_POST['actionNo'];
				$this->beginTransaction();
				
				$this->insertRow($this->prename .'auto_0', $para);
				$this->commit();
				return "添加成功！";
			
			}catch(Exception $e){
				$this->rollBack();
				throw $e;
			}
			
		}
		
	}
	
	
	public function http_post($url, $data) {
		$data_url = http_build_query ($data);
		$data_len = strlen ($data_url);
	
		return file_get_contents ($url, false, stream_context_create (array ('http'=>array ('method'=>'POST'
				, 'header'=>"Connection: close\r\nContent-Length: $data_len\r\n"
				, 'content'=>$data_url
				))));
	}
}
