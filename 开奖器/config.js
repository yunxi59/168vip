exports.cp=[
	{                                                                                                
		title:'lhc',                                                                               
		source:'lhcxml',                                                                                 		
		name:'lhc',                                                                                           
		enable:true,                                                                                            
		timer:'lhc',                                                                                          
		option:{                                                                                                
			host:"127.0.0.1",
			timeout:50000,
			path: '/index.php/xyylcp/xml6',                                                                 
			headers:{                                                                                           
				"User-Agent": "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0) "                             
			}                                                                                                   
		},                                                                                                      
		parse:function(str){                                                                                    
			try{                                                                                                
				str=str.substr(0,200);	                                                                        
				var reg=/<row expect="([\d\-]+?)" opencode="([\d\,]+?)" opentime="([\d\:\- ]+?)"/;              
				var m;                                                                                          
				if(m=str.match(reg)){                                                                           
					return {                                                                                    
						type:30,                                                                                 
						time:m[3],                                                                              
						number:m[1],                                                                            
						data:m[2]                                                                               
					};                                                                                          
				}					                                                                            
			}catch(err){                                                                                        
				throw('--------16彩票重庆时时彩解析数据不正确');                                                
			}                                                                                                   
		}                                                                                                       
	},	
	
	{
		title:'重庆时时彩',
		source:'360彩票网',
		name:'cqssc',
		enable:true,
		timer:'cqssc', 

		option:{
			host:"cp.360.cn",
			timeout:50000,
			path: '/ssccq/',
			headers:{
				"User-Agent": "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0)"
			}
		},
		parse:function(str){
			try{
				return getFrom360CP(str,1);
			}catch(err){
				throw('重庆时时彩解析数据不正确');
			}
		}
	},
	

	
	{
		title:'新疆时时彩',
		source:'123',
		name:'xjssc',
		enable:true,
		timer:'xjssc', 

		option:{
			host:"ho.apiplus.net",
			timeout:50000,
			path: '/te0e464e36aa14960k/xjssc.xml',
			headers:{
				"User-Agent": "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0)"
			}
		},
		parse:function(str){
			try{
				str=str.substr(0,500);
				var m;
				var reg=/<row expect="([\d\-]+?)" opencode="([\d\,]+?)" opentime="([\d\:\- ]+?)"/; 
                                        
				if(m=str.match(reg)){
					return {
						type:12,
						time:m[3],
						number:m[1],
						data:m[2]
					};
				}
			}catch(err){
				throw('快乐10解析数据不正确');
			}
		}
	},

	{
		title:'天津时时彩',
		source:'123',
		name:'tjssc',
		enable:true,
		timer:'tjssc',

		option:{
			host:"ho.apiplus.net",
			timeout:30000,
			path: '/te0e464e36aa14960k/tjssc.xml',
			headers:{
				"User-Agent": "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0)"
			}
		},
		
		parse:function(str){
			try{
				str=str.substr(0,500);
				var m;
				var reg=/<row expect="([\d\-]+?)" opencode="([\d\,]+?)" opentime="([\d\:\- ]+?)"/; 
	
				if(m=str.match(reg)){
					return {
						type:3,
						time:m[3],
						number:m[1],
						data:m[2]
					};
				}	
			}catch(err){
				throw('天津时时彩解析数据不正确');
			}
		}
    },
	
/*	{
		title:'重庆快乐十分百度22222222222222222',
		source:'百度',
		name:'klsf',
		enable:true,
		timer:'klsf2',

		option:{
			host:"baidu.lecai.com",
			timeout:10000,
			path: '/lottery/draw/view/566',
			headers:{
				"User-Agent": "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Win64; x64; Trident/4.0)"
			}
		},
		
		parse:function(str){
			try{	
				return getBaiduData(str,18,'幸运农场');
				
			}catch(err){
				throw('重庆快乐十分百度解析数据不正确');
			}
		}
	},*/
	
	
	/*{
		title:'江苏快3',
		source:'qq',
		name:'jsk3',
		enable:true,
		timer:'jsk3',
 

		option:{
			host:"cp.360.cn",
			timeout:50000,
			path: '/k3js/',
			headers:{
				"User-Agent": "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0)"
			}
		},
		parse:function(str){
			try{
				return getFrom360CPK3(str,25);
			}catch(err){
				throw('江西时时彩解析数据不正确');
			}
		}
	},*/////////////
/*	{
		title:'广西快3',
		source:'qq',
		name:'k3-gx',
		enable:true,
		timer:'k3-gx',
 

		option:{
			host:"cp.360.cn",
			timeout:50000,
			path: '/k3gx/',
			headers:{
				"User-Agent": "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0)"
			}
		},
		parse:function(str){
			try{
				return getFrom360CPK3(str,52);
			}catch(err){
				throw('广西快三解析数据不正确');
			}
		}
	},*/////////////
/*		{
		title:'湖北快3',
		source:'qq',
		name:'k3-hb',
		enable:true,
		timer:'k3-hb',
 

		option:{
			host:"cp.360.cn",
			timeout:50000,
			path: '/k3hb/',
			headers:{
				"User-Agent": "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0)"
			}
		},
		parse:function(str){
			try{
				return getFrom360CPK3(str,50);
			}catch(err){
				throw('广西快三解析数据不正确');
			}
		}
	},*/////////////
	/*{
		title:'福彩3D',
		source:'500wan',
		name:'fc3d',
		enable:true,
		timer:'fc3d',

		option:{
			host:"www.500wan.com",
			timeout:50000,
			path: '/static/info/kaijiang/xml/sd/list10.xml',
			headers:{
				"User-Agent": "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0)"
			}
		},
		
		parse:function(str){
			try{
				str=str.substr(0,300);
				var m;
				var reg=/<row expect="(\d+?)" opencode="([\d\,]+?)" opentime="([\d\:\- ]+?)" trycode="[\d\,]*?" tryinfo="" \/>/;
                                        
				if(m=str.match(reg)){
					return {
						type:9,
						time:m[3],
						number:m[1],
						data:m[2]
					};
				}
			}catch(err){
				throw('福彩3D解析数据不正确');
			}
		}
	},
	{
		title:'排列3',
		source:'500wan',
		name:'pai3',
		enable:true,
		timer:'pai3',

		option:{
			host:"www.500wan.com",
			timeout:50000,
			path: '/static/info/kaijiang/xml/pls/list10.xml',
			headers:{
				"User-Agent": "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0)"
			}
		},
		
		parse:function(str){
			try{
				str=str.substr(0,300);
				var m;	 
				var reg=/<row expect="(\d+?)" opencode="([\d\,]+?)" opentime="([\d\:\- ]+?)"/;
				if(m=str.match(reg)){
					return {
						type:10,
						time:m[3],
						number:20+m[1],
						data:m[2]
					};
				}
			}catch(err){
				throw('排3解析数据不正确');
			}
		}
	},*/
	{
		title:'广东11选5',
		source:'360彩票网',
		name:'gd11x5',
		enable:true,
		timer:'gd11x5',

 

		option:{
			host:"cp.360.cn",
			timeout:50000,
			path: '/gd11/',
			headers:{
				"User-Agent": "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0)"
			}
		},
		parse:function(str){
			try{
				return getFrom360CP(str,6);
			}catch(err){
				//throw('广东11选5解析数据不正确');
			}
		}
	},
	{
		title:'江西11选5',
		source:'360彩票网',
		name:'jx11x5',
		enable:true,
		timer:'jx11x5',
 

		option:{
			host:"cp.360.cn",
			timeout:50000,
			path: '/dlcjx/',
			headers:{
				"User-Agent": "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0)"
			}
		},
		parse:function(str){
			try{
				return getFrom360CP(str,16);
			}catch(err){
				//throw('江西多乐彩解析数据不正确');
			}
		}
	},
	 {
		title:'山东11选5',
		source:'123',
		name:'sd11x5',
		enable:true,
		timer:'sd11x5',

		option:{
			host:"ho.apiplus.net",
			timeout:50000,
			path: '/te0e464e36aa14960k/sd11x5.xml',
			headers:{
				"User-Agent": "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0)"
			}
		},
		parse:function(str){
			try{
				str=str.substr(0,500);
				var m;
				var reg=/<row expect="([\d\-]+?)" opencode="([\d\,]+?)" opentime="([\d\:\- ]+?)"/; 
                                        
				if(m=str.match(reg)){
					return {
						type:7,
						time:m[3],
						number:m[1],
						data:m[2]
					};
				}
			}catch(err){
				throw('解析数据不正确');
			}
		}
	},
	/*{
		title:'山东11选5',
		source:'360彩票网',
		name:'sd11x5',
		enable:true,
		timer:'sd11x5', 

		option:{
			host:"cp.360.cn",
			timeout:50000,
			path: '/yun11/',
			headers:{
				"User-Agent": "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0; Sleipnir/2.9.8) "
			}
		},
		parse:function(str){
			try{
				return getFrom360sd11x5(str,7);
			}catch(err){
				//throw('山东11选5解析数据不正确');
			}
		}
	},*/
	//{{{
	/*{
		title:'北京PK10',
		source:'万金娱乐平台',
		name:'bjpk10',
		enable:true,
		timer:'bjpk10',

		option:{

			host:"www.bwlc.net",
			timeout:50000,
			path: '/bulletin/trax.html',
			headers:{
				"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/29.0.1271.64 Safari/537.11"
			}
		},
		
		parse:function(str){
			try{
				return getFromPK10(str,20);
			}catch(err){
				throw('解析数据不正确');
			}
		}
	},*/////////////
     
     {
		title:'北京PK10',
		source:'123',
		name:'bjpk10',
		enable:true,
		timer:'bjpk10',

		option:{
			host:"ho.apiplus.net",
			timeout:50000,
			path: '/te0e464e36aa14960k/bjpk10.xml',
			headers:{
				"User-Agent": "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0)"
			}
		},
		parse:function(str){
			try{
				str=str.substr(0,500);
				var m;
				var reg=/<row expect="([\d\-]+?)" opencode="([\d\,]+?)" opentime="([\d\:\- ]+?)"/; 
                                        
				if(m=str.match(reg)){
					return {
						type:20,
						time:m[3],
						number:m[1],
						data:m[2]
					};
				}
			}catch(err){
				throw('解析数据不正确');
			}
		}
	},

	/*{
		title:'360彩票安徽3',                                                                           
		source:'360彩票',                                                                               
		name:'ahk3',                                                                                     
		enable:true,                                                                                   
		timer:'ahk3',                                                                                           
		option:{                                                                                          
			host:"127.0.0.1",                                                                               
			timeout:50000,                                                                                    
			path: '/xml/ahk3.php',                                                                      	
			headers:{
				"User-Agent": "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0) "                             
			}                                                                                                   
		},                                                                                                      
		parse:function(str){                                                                                    
			try{                                                                                                
				str=str.substr(0,200);	                                                                        
				var reg=/<row expect="([\d\-]+?)" opencode="([\d\,]+?)" opentime="([\d\:\- ]+?)"/;              
				var m;                                                                                          
				if(m=str.match(reg)){                                                                           
					return {                                                                                    
						type:63,                                                                                
						time:m[3],                                                                              
						number:m[1],                                                                            
						data:m[2]
					}; 
				}
			}catch(err){  
				throw('--------360彩票安徽快3解析数据不正确QQ:158115595'); 
							}
		}
	},*/////////////
	
	
	/*{
		title:'360彩票吉林3',                                                                           
		source:'360彩票',                                                                               
		name:'jlk3',                                                                                     
		enable:true,                                                                                   
		timer:'jlk3',                                                                                           
		option:{                                                                                          
			host:"127.0.0.1",                                                                               
			timeout:50000,                                                                                    
			path: '/xml/jlk3.php',                                                                      	
			headers:{
				"User-Agent": "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0) "                             
			}                                                                                                   
		},                                                                                                      
		parse:function(str){                                                                                    
			try{                                                                                                
				str=str.substr(0,200);	                                                                        
				var reg=/<row expect="([\d\-]+?)" opencode="([\d\,]+?)" opentime="([\d\:\- ]+?)"/;              
				var m;                                                                                          
				if(m=str.match(reg)){                                                                           
					return {                                                                                    
						type:60,                                                                                
						time:m[3],                                                                              
						number:m[1],                                                                            
						data:m[2]
					}; 
				}
			}catch(err){  
				throw('--------360彩票吉林快3解析数据不正确QQ:158115595'); 
							}
		}
	},*/////////////
	
	
	
	/*{
		title:'360彩票河北快3',                                                                           
		source:'360彩票',                                                                               
		name:'hbk3',                                                                                     
		enable:true,                                                                                   
		timer:'hbk3',                                                                                           
		option:{                                                                                          
			host:"127.0.0.1",                                                                               
			timeout:50000,                                                                                    
			path: '/xml/hbk3.php',                                                                      	
			headers:{
				"User-Agent": "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0) "                             
			}                                                                                                   
		},                                                                                                      
		parse:function(str){                                                                                    
			try{                                                                                                
				str=str.substr(0,200);	                                                                        
				var reg=/<row expect="([\d\-]+?)" opencode="([\d\,]+?)" opentime="([\d\:\- ]+?)"/;              
				var m;                                                                                          
				if(m=str.match(reg)){                                                                           
					return {                                                                                    
						type:61,                                                                                
						time:m[3],                                                                              
						number:m[1],                                                                            
						data:m[2]
					}; 
				}
			}catch(err){  
				throw('--------360彩票河北快3解析数据不正确QQ:158115595'); 
							}
		}
	},*/////////////
	/*{
		title:'360彩票蒙古3',                                                                           
		source:'360彩票',                                                                               
		name:'mgk3',                                                                                     
		enable:true,                                                                                   
		timer:'mgk3',                                                                                           
		option:{                                                                                          
			host:"127.0.0.1",                                                                               
			timeout:50000,                                                                                    
			path: '/xml/mgk3.php',                                                                      	
			headers:{
				"User-Agent": "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0) "                             
			}                                                                                                   
		},                                                                                                      
		parse:function(str){                                                                                    
			try{                                                                                                
				str=str.substr(0,200);	                                                                        
				var reg=/<row expect="([\d\-]+?)" opencode="([\d\,]+?)" opentime="([\d\:\- ]+?)"/;              
				var m;                                                                                          
				if(m=str.match(reg)){                                                                           
					return {                                                                                    
						type:62,                                                                                
						time:m[3],                                                                              
						number:m[1],                                                                            
						data:m[2]
					}; 
				}
			}catch(err){  
				throw('--------360彩票蒙古快3解析数据不正确QQ:158115595'); 
											}
		}
	},*/////////////
	/*{
		title:'360彩票福建3',                                                                           
		source:'360彩票',                                                                               
		name:'hjk3',                                                                                     
		enable:true,                                                                                   
		timer:'hjk3',                                                                                           
		option:{                                                                                          
			host:"127.0.0.1",                                                                               
			timeout:50000,                                                                                    
			path: '/xml/hjk3.php',                                                                      	
			headers:{
				"User-Agent": "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0) "                             
			}                                                                                                   
		},                                                                                                      
		parse:function(str){                                                                                    
			try{                                                                                                
				str=str.substr(0,200);	                                                                        
				var reg=/<row expect="([\d\-]+?)" opencode="([\d\,]+?)" opentime="([\d\:\- ]+?)"/;              
				var m;                                                                                          
				if(m=str.match(reg)){                                                                           
					return {                                                                                    
						type:64,                                                                                
						time:m[3],                                                                              
						number:m[1],                                                                            
						data:m[2]
					}; 
				}
			}catch(err){  
				throw('--------360彩票福建快3解析数据不正确QQ:158115595'); 
															}
		}
	},*/////////////
	/*{
		title:'360彩票甘肃3',                                                                           
		source:'360彩票',                                                                               
		name:'gsk3',                                                                                     
		enable:true,                                                                                   
		timer:'gsk3',                                                                                           
		option:{                                                                                          
			host:"127.0.0.1",                                                                               
			timeout:50000,                                                                                    
			path: '/xml/gsk3.php',                                                                      	
			headers:{
				"User-Agent": "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0) "                             
			}                                                                                                   
		},                                                                                                      
		parse:function(str){                                                                                    
			try{                                                                                                
				str=str.substr(0,200);	                                                                        
				var reg=/<row expect="([\d\-]+?)" opencode="([\d\,]+?)" opentime="([\d\:\- ]+?)"/;              
				var m;                                                                                          
				if(m=str.match(reg)){                                                                           
					return {                                                                                    
						type:65,                                                                                
						time:m[3],                                                                              
						number:m[1],                                                                            
						data:m[2]
					}; 
				}
			}catch(err){  
				throw('--------360彩票甘肃快3解析数据不正确QQ:158115595'); 
			}
		}
	},*/////////////
	/*{
		title:'360彩票贵州3',                                                                           
		source:'360彩票',                                                                               
		name:'gzk3',                                                                                     
		enable:true,                                                                                   
		timer:'gzk3',                                                                                           
		option:{                                                                                          
			host:"127.0.0.1",                                                                               
			timeout:50000,                                                                                    
			path: '/xml/gzk3.php',                                                                      	
			headers:{
				"User-Agent": "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0) "                             
			}                                                                                                   
		},                                                                                                      
		parse:function(str){                                                                                    
			try{                                                                                                
				str=str.substr(0,200);	                                                                        
				var reg=/<row expect="([\d\-]+?)" opencode="([\d\,]+?)" opentime="([\d\:\- ]+?)"/;              
				var m;                                                                                          
				if(m=str.match(reg)){                                                                           
					return {                                                                                    
						type:66,                                                                                
						time:m[3],                                                                              
						number:m[1],                                                                            
						data:m[2]
					}; 
				}
			}catch(err){  
				throw('--------360彩票贵州快3解析数据不正确QQ:158115595'); 
			}
		}
	},*/////////////
	/*{
		title:'360彩票河南3',                                                                           
		source:'360彩票',                                                                               
		name:'hnk3',                                                                                     
		enable:true,                                                                                   
		timer:'hnk3',                                                                                           
		option:{                                                                                          
			host:"127.0.0.1",                                                                               
			timeout:50000,                                                                                    
			path: '/xml/hnk3.php',                                                                      	
			headers:{
				"User-Agent": "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0) "                             
			}                                                                                                   
		},                                                                                                      
		parse:function(str){                                                                                    
			try{                                                                                                
				str=str.substr(0,200);	                                                                        
				var reg=/<row expect="([\d\-]+?)" opencode="([\d\,]+?)" opentime="([\d\:\- ]+?)"/;              
				var m;                                                                                          
				if(m=str.match(reg)){                                                                           
					return {                                                                                    
						type:67,                                                                                
						time:m[3],                                                                              
						number:m[1],                                                                            
						data:m[2]
					}; 
				}
			}catch(err){  
				throw('--------360彩票河南快3解析数据不正确QQ:158115595'); 
			}
		}
	},*/////////////
	/*{
		title:'360彩票上海3',                                                                           
		source:'360彩票',                                                                               
		name:'shk3',                                                                                     
		enable:true,                                                                                   
		timer:'shk3',                                                                                           
		option:{                                                                                          
			host:"127.0.0.1",                                                                               
			timeout:50000,                                                                                    
			path: '/xml/shk3.php',                                                                      	
			headers:{
				"User-Agent": "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0) "                             
			}                                                                                                   
		},                                                                                                      
		parse:function(str){                                                                                    
			try{                                                                                                
				str=str.substr(0,200);	                                                                        
				var reg=/<row expect="([\d\-]+?)" opencode="([\d\,]+?)" opentime="([\d\:\- ]+?)"/;              
				var m;                                                                                          
				if(m=str.match(reg)){                                                                           
					return {                                                                                    
						type:69,                                                                                
						time:m[3],                                                                              
						number:m[1],                                                                            
						data:m[2]
					}; 
				}
			}catch(err){  
				throw('--------360彩票上海快3解析数据不正确QQ:158115595'); 
			}
		}
	},*/////////////
	/*{
		title:'360彩票北京3',                                                                           
		source:'360彩票',                                                                               
		name:'bjk3',                                                                                     
		enable:true,                                                                                   
		timer:'bjk3',                                                                                           
		option:{                                                                                          
			host:"127.0.0.1",                                                                               
			timeout:50000,                                                                                    
			path: '/xml/bjk3.php',                                                                      	
			headers:{
				"User-Agent": "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0) "                             
			}                                                                                                   
		},                                                                                                      
		parse:function(str){                                                                                    
			try{                                                                                                
				str=str.substr(0,200);	                                                                        
				var reg=/<row expect="([\d\-]+?)" opencode="([\d\,]+?)" opentime="([\d\:\- ]+?)"/;              
				var m;                                                                                          
				if(m=str.match(reg)){                                                                           
					return {                                                                                    
						type:70,                                                                                
						time:m[3],                                                                              
						number:m[1],                                                                            
						data:m[2]
					}; 
				}
			}catch(err){  
				throw('--------360彩票北京快3解析数据不正确QQ:158115595'); 
			}
		}
	},
	{
		title:'五分彩',
		source:'平台彩票',
		name:'qtllc',
		enable:true,
		timer:'qtllc',

		option:{
			host:"127.0.0.1",
			timeout:50000,
			path: '/index.php/xyylcp/xml',
			headers:{
				"User-Agent": "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0) "
			}
		},
		parse:function(str){
			try{
				str=str.substr(0,200);
				var reg=/<row expect="([\d\-]+?)" opencode="([\d\,]+?)" opentime="([\d\:\- ]+?)"/; 			
				var m;
				if(m=str.match(reg)){
					return {
						type:14,
						time:m[3],
						number:m[1],
						data:m[2]
					};
				}
			}catch(err){
				throw('五分彩解析数据不正确');
			}
		}
	},
	{
		title:'分分彩',
		source:'ffc',
		name:'ffc',
		enable:true,
		timer:'ffc',
		option:{
			host:"127.0.0.1",
			timeout:50000,
			path: '/index.php/xyylcp/xml3',
			headers:{
				"User-Agent": "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0) "
			}
		},
		parse:function(str){
			try{
				str=str.substr(0,200);	
				var reg=/<row expect="([\d\-]+?)" opencode="([\d\,]+?)" opentime="([\d\:\- ]+?)"/; 
				var m;
				if(m=str.match(reg)){
					return {
						type:5,
						time:m[3],
						number:m[1],
						data:m[2]
					};
				}					
			}catch(err){
				throw('分分彩解析数据不正确');
			}
		}
	},*/
		{
		title:'六合彩',
		source:'qq',
		name:'lhc',
		enable:true,
		timer:'lhc',
		option:{
			host:"127.0.0.1",            //务必修改成你自己的域名
			timeout:50000,
			path: '/index.php/xyylcp/xml4',
			headers:{
				"User-Agent": "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0) "
			}
		},
		parse:function(str){
			try{
				str=str.substr(0,200);
				var reg=/<row expect="([\d\-]+?)" opencode="([\d\,]+?)" opentime="([\d\:\- ]+?)"/;
				var m;
				if(m=str.match(reg)){
					return {
						type:30,
						time:m[3],
						number:m[1],
						data:m[2]
					};
				}					
			}catch(err){
				throw('lhc解析数据不正确');
			}
		}
	},
	/*{
		title:'3分彩',
		source:'sfc',
		name:'sfc',
		enable:true,
		timer:'sfc',
		option:{
			host:"127.0.0.1",
			timeout:50000,
			path: '/index.php/xyylcp/xml5',
			headers:{
				"User-Agent": "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0) "
			}
		},
		parse:function(str){
			try{
				str=str.substr(0,200);	
				var reg=/<row expect="([\d\-]+?)" opencode="([\d\,]+?)" opentime="([\d\:\- ]+?)"/; 
				var m;
				if(m=str.match(reg)){
					return {
						type:27,
						time:m[3],
						number:m[1],
						data:m[2]
					};
				}					
			}catch(err){
				throw('3分彩解析数据不正确');
			}
		}
	},*/

	
	/*{
		title:'2分彩',
		source:'lfc',
		name:'lfc',
		enable:true,
		timer:'lfc',
		option:{
			host:"127.0.0.1",
			timeout:50000,
			path: '/index.php/xyylcp/xml2',
			headers:{
				"User-Agent": "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0) "
			}
		},
		parse:function(str){
			try{
				str=str.substr(0,200);	
				var reg=/<row expect="([\d\-]+?)" opencode="([\d\,]+?)" opentime="([\d\:\- ]+?)"/; 
				var m;
				if(m=str.match(reg)){
					return {
						type:26,
						time:m[3],
						number:m[1],
						data:m[2]
					};
				}					
			}catch(err){
				throw('2分彩解析数据不正确');
			}
		}
	}*/
	
];

// 出错时等待 10
exports.errorSleepTime=10;

// 重启时间间隔，以小时为单位，0为不重启
//exports.restartTime=0.4;
exports.restartTime=0;

exports.submit={

	host:'500a.207k.com',
	path:'/admin778899.php/dataSource/kj'
}

exports.dbinfo={
	host:'localhost',
	user:'root',
	password:'root',
	database:'500vip'

}

global.log=function(log){
	var date=new Date();
	console.log('['+date.toDateString() +' '+ date.toLocaleTimeString()+'] '+log)
}
function getFromXJFLCPWeb(str, type){
	str=str.substr(str.indexOf('<td><a href="javascript:detatilssc'), 300).replace(/[\r\n]+/g,'');
         
	var reg=/(\d{10}).+(\d{2}\:\d{2}).+<p>([\d ]{9})<\/p>/,
	match=str.match(reg);
	
	if(!match) throw new Error('数据不正确');
	try{
		var data={
			type:type,
			time:match[1].replace(/^(\d{4})(\d{2})(\d{2})\d{2}/, '$1-$2-$3 ')+match[2],
			number:match[1].replace(/^(\d{8})(\d{2})$/, '$1-$2'),
			data:match[3].split(' ').join(',')
		};
		//console.log(data);
		return data;
	}catch(err){
		throw('解析数据失败');
	}
}


function getFromCaileleWeb(str, type, slen){
	if(!slen) slen=500;
	str=str.substr(str.indexOf('<span class="cz_name">'),slen);
	//console.log(str);
	var reg=/<td.*?>(\d+)<\/td>[\s\S]*?<td.*?>([\d\- \:]+)<\/td>[\s\S]*?<td.*?>((?:[\s\S]*?<span class="red_ball">\d+<\/span>){3,5})\s*<\/td>/,
	match=str.match(reg);
	if(match.length>1){
		
		if(match[1].length==7) match[1]='2014'+match[1].replace(/(\d{4})(\d{3})/,'$1-$2');
		if(match[1].length==8){
			if(parseInt(type)!=11){
				match[1]='20'+match[1].replace(/(\d{6})(\d{2})/,'$1-0$2');
			}else{match[1]='20'+match[1].replace(/(\d{6})(\d{2})/,'$1-$2');}
		}
		if(match[1].length==9) match[1]='20'+match[1].replace(/(\d{6})(\d{2})/,'$1-$2');
		if(match[1].length==10) match[1]=match[1].replace(/(\d{8})(\d{2})/,'$1-0$2');
		var mynumber=match[1].replace(/(\d{8})(\d{3})/,'$1-$2');
	try{
		var data={
			type:type,
			time:match[2],
			number:mynumber
		}
		reg=/<div.*>(\d+)<\/div>/g;
		data.data=match[3].match(reg).map(function(v){
			var reg=/<div.*>(\d+)<\/div>/;
			return v.match(reg)[1];
		}).join(',');
		
		//console.log(data);
		return data;
	}catch(err){
		throw('解析数据失败');
	}
   }
}
function getFromBwlcWeb(str, type){
	str=str.substr(str.indexOf('<tr class="'), 300).replace(/[\r\n]+/g,'');
         
	var reg=/<td>(\d{6}).+([\d+,]{29}).+([\d\- \:]{16})<\/td>/,
				 //<td>374454</td><td>04,10,01,03,05,09,06,07,02,08</td><td>2013-07-25 23:57</td>

	match=str.match(reg);
	
	if(!match) throw new Error('数据不正确');
                    
	try{
		var data={
			type:type,
			time:match[3],
			number:match[1],
			data:match[2]
		};
		//console.log(data);
		return data;
	}catch(err){
		throw('解析数据失败');
	}
}
function getFrom360CP(str, type){

	str=str.substr(str.indexOf('<em class="red" id="open_issue">'),380);
	//console.log(str);
	var reg=/[\s\S]*?(\d+)<\/em>[\s\S].*?<ul id="open_code_list">((?:[\s\S]*?<li class=".*?">\d+<\/li>){3,5})[\s\S]*?<\/ul>/,
	match=str.match(reg);
	var myDate = new Date();
	var year = myDate.getFullYear();       //年   
    var month = myDate.getMonth() + 1;     //月   
    var day = myDate.getDate();            //日
	if(month < 10) month="0"+month;
	if(day < 10) day="0"+day;
	var mytime=year + "-" + month + "-" + day + " " +myDate.toLocaleTimeString();
	//console.log(match);
	if(match.length>1){
		if(match[1].length==7) match[1]=year+match[1].replace(/(\d{8})(\d{3})/,'$1-$2');
		if(match[1].length==6) match[1]=year+match[1].replace(/(\d{4})(\d{2})/,'$1-0$2');
		if(match[1].length==9) match[1]='20'+match[1].replace(/(\d{6})(\d{2})/,'$1-$2');
		if(match[1].length==10) match[1]=match[1].replace(/(\d{8})(\d{2})/,'$1-0$2');
		var mynumber=match[1].replace(/(\d{8})(\d{3})/,'$1-$2');
		
		try{
			var data={
				type:type,
				time:mytime,
				number:mynumber
			}
			
			reg=/<li class=".*?">(\d+)<\/li>/g;
			data.data=match[2].match(reg).map(function(v){
				var reg=/<li class=".*?">(\d+)<\/li>/;
				return v.match(reg)[1];
			}).join(',');
			
			//console.log(data);
			return data;
		}catch(err){
			throw('解析数据失败');
		}
	}
}

function getFrom360CPK3(str, type){

	str=str.substr(str.indexOf('<em class="red" id="open_issue">'),380);
	//console.log(str);
	var reg=/[\s\S]*?(\d+)<\/em>[\s\S].*?<ul id="open_code_list">((?:[\s\S]*?<li class=".*?">\d+<\/li>){3,5})[\s\S]*?<\/ul>/,
	match=str.match(reg);
	var myDate = new Date();
	var year = myDate.getFullYear();       //年   
    var month = myDate.getMonth() + 1;     //月   
    var day = myDate.getDate();            //日
	if(month < 10) month="0"+month;
	if(day < 10) day="0"+day;
	var mytime=year + "-" + month + "-" + day + " " +myDate.toLocaleTimeString();
	//console.log(match);
	match[1]=match[1].replace(/(\d{4})(\d{2})/,'$1$2');
		
		try{
			var data={
				type:type,
				time:mytime,
				number:match[1]
			}
			
			reg=/<li class=".*?">(\d+)<\/li>/g;
			data.data=match[2].match(reg).map(function(v){
				var reg=/<li class=".*?">(\d+)<\/li>/;
				return v.match(reg)[1];
			}).join(',');
			
			//console.log(data);
			return data;
		}catch(err){
			throw('解析数据失败');
		}
}

function getFromPK10(str, type){

	str=str.substr(str.indexOf('<div class="lott_cont">'),350).replace(/[\r\n]+/g,'');
    //console.log(str);
	var reg=/<tr class=".*?">[\s\S]*?<td>(\d+)<\/td>[\s\S]*?<td>(.*)<\/td>[\s\S]*?<td>([\d\:\- ]+?)<\/td>[\s\S]*?<\/tr>/,
	match=str.match(reg);
	if(!match) throw new Error('数据不正确');
	//console.log(match);
	try{
		var data={
			type:type,
			time:match[3],
			number:match[1],
			data:match[2]
		};
		//console.log(data);
		return data;
	}catch(err){
		throw('解析数据失败');
	}
	
}

function getFromK8(str, type){

	str=str.substr(str.indexOf('<div class="lott_cont">'),450).replace(/[\r\n]+/g,'');
    //console.log(str);
	var reg=/<tr class=".*?">[\s\S]*?<td>(\d+)<\/td>[\s\S]*?<td>(.*)<\/td>[\s\S]*?<td>(.*)<\/td>[\s\S]*?<td>([\d\:\- ]+?)<\/td>[\s\S]*?<\/tr>/,
	match=str.match(reg);
	if(!match) throw new Error('数据不正确');
	//console.log(match);
	try{
		var data={
			type:type,
			time:match[4],
			number:match[1],
			data:match[2]+'|'+match[3]
		};
		//console.log(data);
		return data;
	}catch(err){
		throw('解析数据失败');
	}
	
}


function getFromCJCPWeb(str, type){

	//console.log(str);
	str=str.substr(str.indexOf('<table class="qgkj_table">'),1200);
	
	//console.log(str);
	
	var reg=/<tr>[\s\S]*?<td class=".*">(\d+).*?<\/td>[\s\S]*?<td class=".*">([\d\- \:]+)<\/td>[\s\S]*?<td class=".*">((?:[\s\S]*?<input type="button" value="\d+" class=".*?" \/>){3,5})[\s\S]*?<\/td>/,
	match=str.match(reg);
	
	//console.log(match);
	
	if(!match) throw new Error('数据不正确');
	try{
		var data={
			type:type,
			time:match[2],
			number:match[1].replace(/(\d{8})(\d{2})/,'$1-0$2')
		}
		
		reg=/<input type="button" value="(\d+)" class=".*?" \/>/g;
		data.data=match[3].match(reg).map(function(v){
			var reg=/<input type="button" value="(\d+)" class=".*?" \/>/;
			return v.match(reg)[1];
		}).join(',');
		
		//console.log(data);
		return data;
	}catch(err){
		throw('解析数据失败');
	}
	
}

function getFromCaileleWeb_1(str, type){
	str=str.substr(str.indexOf('<tbody id="openPanel">'), 120).replace(/[\r\n]+/g,'');
         
	var reg=/<tr.*?>[\s\S]*?<td.*?>(\d+)<\/td>[\s\S]*?<td.*?>([\d\:\- ]+?)<\/td>[\s\S]*?<td.*?>([\d\,]+?)<\/td>[\s\S]*?<\/tr>/,
	match=str.match(reg);
	if(!match) throw new Error('数据不正确');
	//console.log(match);
	var number,_number,number2;
	var d = new Date();
	var y = d.getFullYear();
	if(match[1].length==9 || match[1].length==8){number='20'+match[1];}else if(match[1].length==7){number='2014'+match[1];}else{number=match[1];}
	_number=number;
	if(number.length==11){number2=number.replace(/^(\d{8})(\d{3})$/, '$1-$2');}else{number2=number.replace(/^(\d{8})(\d{2})$/, '$1-0$2');_number=number.replace(/^(\d{8})(\d{2})$/, '$10$2');}
	try{
		var data={
			type:type,
			time:_number.replace(/^(\d{4})(\d{2})(\d{2})\d{3}/, '$1-$2-$3 ')+match[2],
			number:number2,
			data:match[3]
		};
		//console.log(data);
		return data;
	}catch(err){
		throw('解析数据失败');
	}
}

function getFrom360sd11x5(str, type){

	str=str.substr(str.indexOf('<em class="red" id="open_issue">'),380);
	//console.log(str);
	var reg=/[\s\S]*?(\d+)<\/em>[\s\S].*?<ul id="open_code_list">((?:[\s\S]*?<li class=".*?">\d+<\/li>){3,5})[\s\S]*?<\/ul>/,
	match=str.match(reg);
	var myDate = new Date();
	var year = myDate.getFullYear();       //年   
    var month = myDate.getMonth() + 1;     //月   
    var day = myDate.getDate();            //日
	if(month < 10) month="0"+month;
	if(day < 10) day="0"+day;
	var mytime=year + "-" + month + "-" + day + " " +myDate.toLocaleTimeString(); 
	//console.log(mytime);
	//console.log(match);
	
	if(!match) throw new Error('数据不正确');
	try{
		var data={
			type:type,
			time:mytime,
			number:year+match[1].replace(/(\d{4})(\d{2})/,'$1-0$2')
		}
		
		reg=/<li class=".*?">(\d+)<\/li>/g;
		data.data=match[2].match(reg).map(function(v){
			var reg=/<li class=".*?">(\d+)<\/li>/;
			return v.match(reg)[1];
		}).join(',');
		
		//console.log(data);
		return data;
	}catch(err){
		throw('解析数据失败');
	}
}

function getFromklsfweb(str, type){
	str=str.substr(0,1000);	
	//console.log(str);	
	var reg=/<row expect="(\d+?)" opencode="([\d\,]+?)" specail="" opentime="([\d\:\- ]+?)"/;
	match=str.match(reg);
	if(!match) throw new Error('数据不正确');
	var myDate = new Date();
	var year = myDate.getFullYear();       //年   
    var month = myDate.getMonth() + 1;     //月   
    var day = myDate.getDate();            //日
	if(month < 10) month="0"+month;
	if(day < 10) day="0"+day;
	var mynumber=match[1];
	var mydata=match[2];
	var mytime=match[3];
	//console.log(mynumber);
	try{
		var data={
			type:type,
			time:mytime,
			number:mynumber,
			data:mydata
		};
		//console.log(data);
		return data;
	}catch(err){
		throw('解析数据失败');
	}
} 



function getBaiduData(str, type,gameName){
	
	
	str = str.substr(str.indexOf('name="description"'),100).replace(/[\r\n]+/g,'');
	
	var reg =new RegExp(gameName+"(\\d+)期，开奖结果：(\\d+)。",""); 

	var match=str.match(reg);

	if(!match) throw new Error('-------------------------'+gameName+'百度数据不正确');
	

	var ano =  match[1];
	
	var datastr= match[2]+'';
	var dataarr = datastr.split("");
	var data='';
	for(var i in dataarr){
		
		if(i%2==0 && i!=0){
			
			data+=','+dataarr[i];
		}else{
			data+=dataarr[i];
		}
		
	}
	var myDate = new Date();
	var year = myDate.getFullYear();       //年   
    var month = myDate.getMonth() + 1;     //月   
    var day = myDate.getDate();            //日
	if(month < 10) month="0"+month;
	if(day < 10) day="0"+day;
	var mytime=year + "-" + month + "-" + day + " " +myDate.toLocaleTimeString();
	
	if(ano.length==7) ano=year+ano.replace(/(\d{8})(\d{3})/,'$1-$2');
	if(ano.length==6) ano=year+ano.replace(/(\d{4})(\d{2})/,'$1-0$2');
	if(ano.length==9) ano='20'+ano.replace(/(\d{6})(\d{2})/,'$1-$2');
	if(ano.length==10) ano=ano.replace(/(\d{8})(\d{2})/,'$1-0$2');
	var mynumber=ano.replace(/(\d{8})(\d{3})/,'$1-$2');
	

	try{
		var data={
			type:type,
			time:mytime,
			number:mynumber,
			data:data
		}
		
	//	console.log(gameName+"------------------------------ baidu");
	//	console.log(data);
		return data;
	}catch(err){
		throw('解析'+gameName+'官网数据失败');
	}
}
//从彩经网获取重庆时时彩 



function getFromCaileleWeb_2(str, type){

	str=str.substr(str.indexOf('<tbody id="openPanel">'), 500).replace(/[\r\n]+/g,'');
	//console.log(str);
	var reg=/<tr>[\s\S]*?<td>(\d+)<\/td>[\s\S]*?<td>([\d\:\- ]+?)<\/td>[\s\S]*?<td>([\d\,]+?)<\/td>[\s\S]*?<\/tr>/,
	match=str.match(reg);
	if(!match) throw new Error('数据不正确');
	//console.log(match);
	var number,_number,number2;
	var d = new Date();
	var y = d.getFullYear();
	if(match[1].length==9 || match[1].length==8){number='20'+match[1];}else if(match[1].length==7){number='2014'+match[1];}else{number=match[1];}
	_number=number;
	if(number.length==11){number2=number.replace(/^(\d{8})(\d{3})$/, '$1-$2');}else{number2=number.replace(/^(\d{8})(\d{2})$/, '$1-0$2');_number=number.replace(/^(\d{8})(\d{2})$/, '$10$2');}
	try{
		var data={
			type:type,
			time:_number.replace(/^(\d{4})(\d{2})(\d{2})\d{3}/, '$1-$2-$3 ')+match[2],
			number:number2,
			data:match[3]
		};
		//console.log(data);
		return data;
	}catch(err){
		throw('解析数据失败');
	}
}