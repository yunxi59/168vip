<?php
include_once 'Tip.class.php';
@session_start();
class User extends WebBase{
	public $title='FUN娱乐平台';
	private $vcodeSessionName='ssc_vcode_session_name';
	
	/**
	 * 用户登录页面
	 */
	public final function login(){
		
		$this->display('user/login.php');
	}
	

	/**
	 * 用户登录页面2
	 */
	public final function loginto(){
		$this->display('user/loginto.php');
	}
	
	/**
	 * 用户登出操作
	 */
	public final function logout(){
		$_SESSION=array();
		if($this->user['uid']){
			$this->update("update {$this->prename}member_session set isOnLine=0 where uid={$this->user['uid']}");
		}
		header('location: /index.php/user/login');
	}
	
	public final function bulletin(){
		$this->display('user/bulletin.php');
	}
	
	private function getBrowser(){
		$flag=$_SERVER['HTTP_USER_AGENT'];
		$para=array();
		
		// 检查操作系统
		if(preg_match('/Windows[\d\. \w]*/',$flag, $match)) $para['os']=$match[0];
		
		if(preg_match('/Chrome\/[\d\.\w]*/',$flag, $match)){
			// 检查Chrome
			$para['browser']=$match[0];
		}elseif(preg_match('/Safari\/[\d\.\w]*/',$flag, $match)){
			// 检查Safari
			$para['browser']=$match[0];
		}elseif(preg_match('/MSIE [\d\.\w]*/',$flag, $match)){
			// IE
			$para['browser']=$match[0];
		}elseif(preg_match('/Opera\/[\d\.\w]*/',$flag, $match)){
			// opera
			$para['browser']=$match[0];
		}elseif(preg_match('/Firefox\/[\d\.\w]*/',$flag, $match)){
			// Firefox
			$para['browser']=$match[0];
		}else{
			$para['browser']='unkown';
		}
		//print_r($para);exit;
		return $para;
	}
	
	/**
	 * 用户登录检查
	 */
	public final function logined(){
		$username=wjStrFilter($_POST['username']);
		$vcode=$_POST['vcode'];
        if(!ctype_alnum($username)) throw new Exception('用户名包含非法字符,请重新输入');

		if(!isset($username)){
			throw new Exception('请输入用户名');
		}
		if(!isset($vcode)){
			throw new Exception('请输入验证码');
		}
		
		if($vcode!=$_SESSION[$this->vcodeSessionName]){
			throw new Exception('验证码不正确。');
		}
		
		$sql="select * from {$this->prename}members where isDelete=0 and admin=0 and username=?";
		if(!$user=$this->getRow($sql, $username)){
			throw new Exception('用户名不正确');
		}
		if(!$user['enable']){
			throw new Exception('您的帐号被冻结，请联系管理员。');
		}
		setcookie('username',$user['username']);
		if($user['care']){
		   setcookie('care',$user['care']);
		}else{
		   setcookie('care',"尚未设置，请尽快设置吧。");
		}
	}
	/**
	 * 用户登录检查2
	 */
	public final function loginedto(){

	    $username=wjStrFilter($_POST['username']);
        $password=wjStrFilter($_POST['password']);

		if(!ctype_alnum($username)) throw new Exception('用户名包含非法字符,请重新登陆');
		
		if(!$username){
			throw new Exception('请输入用户名');
		}
		
		if(!$password){
			throw new Exception('不允许空密码登录');
		}
		
		$sql="select * from {$this->prename}members where isDelete=0 and admin=0 and username=?";
		if(!$user=$this->getRow($sql, $username)){
			throw new Exception('用户名或密码不正确');
		}
		
		if(md5($password)!=$user['password']){
			throw new Exception('密码不正确');
		}

		if(!$user['enable']){
			throw new Exception('您的帐号被冻结，请联系管理员。');
		}

		$session=array(
			'uid'=>$user['uid'],
			'username'=>$user['username'],
			'session_key'=>session_id(),
			'loginTime'=>$this->time,
			'accessTime'=>$this->time,
			'loginIP'=>self::ip(true)
		);
		//存cookie
		
		setcookie('username',$username,time()+3600*24*7);
		setcookie('password',$password,time()+3600*24*7);

		$session=array_merge($session, $this->getBrowser());
		
		if($this->insertRow($this->prename.'member_session', $session)){
			$user['sessionId']=$this->lastInsertId();
		}
		$_SESSION[$this->memberSessionName]=serialize($user);
		
		// 把别人踢下线
		$this->update("update ssc_member_session set isOnLine=0 where uid={$user['uid']} and id < {$user['sessionId']}");

		return $user;
	}

	/**
	 * 验证码产生器
	 */
	public final function vcode($rmt=null){
		$lib_path=$_SERVER['DOCUMENT_ROOT'].'/lib/';
		include_once $lib_path .'classes/CImage.class';
		$width=72;
		$height=24;
		$img=new CImage($width, $height);
		$img->sessionName=$this->vcodeSessionName;
		$img->printimg('png');
	}
	
	/**
	 * 推广注册
	 */
	public final function r($userxxx){
		if(!$userxxx){
			//throw new Exception('链接错误！');
			$this->display('team/register.php');
		}else{
			include_once $_SERVER['DOCUMENT_ROOT'].'/lib/classes/Xxtea.class';
			$userxxx=str_replace(array('-','*',''), array('+','/','='), $userxxx);
			$userxxx=base64_decode($userxxx);
			$LArry=Xxtea::decrypt($userxxx, $this->urlPasswordKey);
			$LArry=explode(",",$LArry);
			$lid=$LArry[0];
			$uid=$LArry[1];

			if(!$this->getRow("select uid from {$this->prename}members where uid=?",$uid)){
				//throw new Exception('链接失效！');
				$this->display('team/register.php');
			}else{
				$this->display('team/register.php',0,$uid,$lid);
			}
		}
	}
	public final function registered(){
		$urlshang = $_SERVER['HTTP_REFERER']; //上一页URL
		$urldan = $_SERVER['SERVER_NAME']; //本站域名
		$urlcheck=substr($urlshang,7,strlen($urldan));
		if($urlcheck<>$urldan)  throw new Exception('数据包被篡改，请重新操作');

		if(!$_POST)  throw new Exception('提交数据出错，请重新操作');

		//表单过滤
		$lid=intval($_POST['lid']);
		// $parentId=intval($_POST['parentId']);
		$user=wjStrFilter($_POST['username']);
		$qq=wjStrFilter($_POST['qq']);
		$vcode=wjStrFilter($_POST['vcode']);
		$password=md5($_POST['password']);
		//验证推荐码
		$tuijian=wjStrFilter($_POST['tuijian']);
		if(!$tuijian) throw new Exception('推荐码不能为空');
		
		if($vcode!=$_SESSION[$this->vcodeSessionName]) throw new Exception('验证码不正确。');

		//清空验证码session
	    $_SESSION[$this->vcodeSessionName]="";

		if(!ctype_alnum($user)) throw new Exception('用户名包含非法字符');
		if(!ctype_digit($qq)) throw new Exception('QQ包含非法字符');
		
		$sql="select * from {$this->prename}links where lid=?";
		$linkData=$this->getRow($sql, $lid);
		if(!$_POST['lid']) $para['lid']=$lid;
		if(!$linkData) throw new Exception('不存在此注册链接。');
		// if(!$parentId) throw new Exception('链接错误');

		$para=array(
			'username'=>$user,
			// 'type'=>$linkData['type'],
			'password'=>$password,
			// 'parentId'=>$parentId,
			'parents'=>$this->getValue("select parents from {$this->prename}members where uid=?",$para['parentId']),
			// 'fanDian'=>$linkData['fanDian'],
			// 'fanDianBdw'=>$linkData['fanDianBdw'],
			'qq'=>$qq,
			'regIP'=>$this->ip(true),
			'regTime'=>$this->time
			);

		//查询推荐类型
		$daili_sql="select * from {$this->prename}members where daili_tj='".$tuijian."'";
		$daili_tj=$this->getRow($daili_sql);
		if (!empty($daili_tj)) {
			//是代理推荐
			$para['parentId'] = $daili_tj['uid'];
			$para['type'] = 1;//表示是代理
			$para['daili_tj'] = 'daili'.rand(0,999999);//代理推荐码
			$para['huiyuan_tj'] = 'huiyuan'.rand(0,999999);//会员推荐码
		}else{
			//会员推荐
			$huiyuan_sql="select * from {$this->prename}members where huiyuan_tj='".$tuijian."'";
			$huiyuan_tj=$this->getRow($huiyuan_sql);
			$para['parentId'] = $huiyuan_tj['uid'];
		}

        //$regtime=$this->getrow("select * from {$this->prename}members where regIP=? order by regTime DESC limit 1",ip2long($this->ip(true)));
		//$time=strtotime($this->time)-$this->iff($regtime['regTime'],$regtime['regTime'],strtotime($this->time)-400);
		//if($time<300) throw new Exception('同一IP 5 分钟内只能注册一次');

		if(!$para['nickname']) $para['nickname']='未设昵称';
		if(!$para['name']) $para['name']=$para['username'];
		$this->beginTransaction();
		try{
			$sql="select username from {$this->prename}members where username=?";
			if($this->getValue($sql, $para['username'])) throw new Exception('用户"'.$para['username'].'"已经存在');
			if($this->insertRow($this->prename .'members', $para)){
				$id=$this->lastInsertId();
				$sql="update {$this->prename}members set parents=concat(parents, ',', $id) where `uid`=$id";
				$this->update($sql);
				$this->commit();
				return '注册成功';
			}else{
				throw new Exception('注册失败');
			}	
		}catch(Exception $e){
			$this->rollBack();
			throw $e;
		}
	}

	//计算订单-进行统计
	public final function orderproces($rand_data,$qishu,$shoudong){
		$rands = explode(',', $rand_data);
		$rand1 = $rands[0];
		$rand2 = $rands[1];
		$rand3 = $rands[2];
		$rand4 = $rands[3];
		$rand5 = $rands[4];
		// $rand1 = 7;
		// $rand2 = 0;
		// $rand3 = 5;
		// $rand4 = 0;
		// $rand5 = 7;

		//赔率
		$daxiao = $this->getRow("select * from {$this->prename}odds where odd_id=1");
		$danshuang = $this->getRow("select * from {$this->prename}odds where odd_id=2");
		$longhu = $this->getRow("select * from {$this->prename}odds where odd_id=3");
		$danxuan = $this->getRow("select * from {$this->prename}odds where odd_id=4");
		

		$orderssc=$this->getRows("select * from {$this->prename}orderssc where qishu=".$qishu."");
		// $rands = $this->bijiao();
		
		foreach ($orderssc as $key => $val) {
			$order = explode(',', $val['class']);
			foreach ($order as $km => $vm) {
				//这个是判断大小
				if ($val['type'] == 1) {
					// print_r($vm);
					//万位
					if ($vm == 110 || $vm == 120 ) {
						if ($rand1 == 0 || $rand1 == 1 || $rand1 == 2 || $rand1 == 3 || $rand1 == 4) {
							$wx = 120;//万小
							if ($vm == $wx) {
								$orderssc[$key]['rand1_pei'] = 2 * $val['mom'] * $daxiao['odds'];//小
							}
						}else{
							$wd = 110;//万大
							if ($vm == $wd) {
								$orderssc[$key]['rand1_pei'] = 2 * $val['mom'] * $daxiao['odds'];//大
							}
						}
					}
					//千位
					if ($vm == 111 || $vm == 121 ) {
						if ($rand2 == 0 || $rand2 == 1 || $rand2 == 2 || $rand2 == 3 || $rand2 == 4) {
							$qx = 121;//千小
							if ($vm == $qx) {
								$orderssc[$key]['rand2_pei'] = 2 * $val['mom'] * $daxiao['odds'];//小
							}
						}else{
							$qd = 111;
							if ($vm == $qd) {
								$orderssc[$key]['rand2_pei'] = 2 * $val['mom'] * $daxiao['odds'];//大
							}
						}
					}
					//百位
					if ($vm == 112 || $vm == 122 ) {
						if ($rand3 == 0 || $rand3 == 1 || $rand3 == 2 || $rand3 == 3 || $rand3 == 4) {
							$bx = 122;//百小
							if ($vm == $bx) {
								$orderssc[$key]['rand3_pei'] = 2 * $val['mom'] * $daxiao['odds'];//小
							}
						}else{
							$bd = 112;
							if($vm == $bd){
								$orderssc[$key]['rand3_pei'] = 2 * $val['mom'] * $daxiao['odds'];//大
							}						
						}
					}
					//十位
					if ($vm == 113 || $vm == 123 ) {
						if ($rand4 == 0 || $rand4 == 1 || $rand4 == 2 || $rand4 == 3 || $rand4 == 4) {
							$sx = 123;//十小
							if ($vm == $sx) {
								$orderssc[$key]['rand4_pei'] = 2 * $val['mom'] * $daxiao['odds'];//小
							}
						}else{
							$sd = 113;
							if ($vm == $sd) {
								$orderssc[$key]['rand4_pei'] = 2 * $val['mom'] * $daxiao['odds'];//大
							}
						}
					}
					//个位
					if ($vm == 114 || $vm == 124 ) {
						if ($rand5 == 0 || $rand5 == 1 || $rand5 == 2 || $rand5 == 3 || $rand5 == 4) {
							$gx = 124;//个小
							if ($vm == $gx) {
								$orderssc[$key]['rand5_pei'] = 2 * $val['mom'] * $daxiao['odds'];//小
							}
						}else{
							$gd = 114;
							if ($vm == $gd) {
								$orderssc[$key]['rand5_pei'] = 2 * $val['mom'] * $daxiao['odds'];//大
							}
						}
					}
					$orderssc[$key]['daixao_peilv'] = $orderssc[$key]['rand1_pei'] + $orderssc[$key]['rand2_pei'] + $orderssc[$key]['rand3_pei'] + $orderssc[$key]['rand4_pei'] + $orderssc[$key]['rand5_pei'];

					$orderssc[$key]['daixao_order_peilv'] = $orderssc[$key]['rand1_pei'] + $orderssc[$key]['rand2_pei'] + $orderssc[$key]['rand3_pei'] + $orderssc[$key]['rand4_pei'] + $orderssc[$key]['rand5_pei'];
					
				}
				//这是判断单双
				if ($val['type'] == 2) {
					//万位
					if ($vm == 230 || $vm == 240 ) {
						if ($rand1 == 1 || $rand1 == 3 || $rand1 == 5 || $rand1 == 7 || $rand1 == 9) {
							$wd = 230;//万单
							if ($vm == $wd) {
								$orderssc[$key]['rand1_dan'] = 2 * $val['mom'] * $danshuang['odds'];//单
							}
						}else{
							$ws = 240;
							if ($vm == $ws) {
								$orderssc[$key]['rand1_dan'] = 2 * $val['mom'] * $danshuang['odds'];//双
							}
						}
					}
					//千位
					if ($vm == 231 || $vm == 241 ) {
						if ($rand2 == 1 || $rand2 == 3 || $rand2 == 5 || $rand2 == 7 || $rand2 == 9) {
							$qd = 231;//千单
							if ($vm == $qd) {
								$orderssc[$key]['rand2_dan'] = 2 * $val['mom'] * $danshuang['odds'];//单
							}
						}else{
							$qs = 241;
							if ($vm == $qs) {
								$orderssc[$key]['rand2_dan'] = 2 * $val['mom'] * $danshuang['odds'];//双
							}						
						}
					}
					//百位
					if ($vm == 232 || $vm == 242 ) {
						if ($rand3 == 1 || $rand3 == 3 || $rand3 == 5 || $rand3 == 7 || $rand3 == 9) {
							$bd = 232;//百单
							if ($vm == $bd) {
								$orderssc[$key]['rand3_dan'] = 2 * $val['mom'] * $danshuang['odds'];//单
							}
						}else{
							$bs = 242;
							if($vm == $bs){
								$orderssc[$key]['rand3_dan'] = 2 * $val['mom'] * $danshuang['odds'];//双
							}				
						}
					}
					//十位
					if ($vm == 233 || $vm == 243 ) {
						if ($rand4 == 1 || $rand4 == 3 || $rand4 == 5 || $rand4 == 7 || $rand4 == 9) {
							$sd = 233;//十单
							if ($vm == $sd) {
								$orderssc[$key]['rand4_dan'] = 2 * $val['mom'] * $danshuang['odds'];//单
							}
						}else{
							$ss = 243;
							if ($vm == $ss) {
								$orderssc[$key]['rand4_dan'] = 2 * $val['mom'] * $danshuang['odds'];//双
							}
						}
					}
					//个位
					if ($vm == 234 || $vm == 244 ) {
						if ($rand5 == 1 || $rand5 == 3 || $rand5 == 5 || $rand5 == 7 || $rand5 == 9) {
							$gd = 234;//个单
							if ($vm == $gd) {
								$orderssc[$key]['rand5_dan'] = 2 * $val['mom'] * $danshuang['odds'];//单
							}
						}else{
							$gs = 244;
							if($vm == $gs){
								$orderssc[$key]['rand5_dan'] = 2 * $val['mom'] * $danshuang['odds'];//双
							}						
						}
					}
					
					$orderssc[$key]['danshuang_peilv'] = $orderssc[$key]['rand1_dan'] + $orderssc[$key]['rand2_dan'] + $orderssc[$key]['rand3_dan'] + $orderssc[$key]['rand4_dan'] + $orderssc[$key]['rand5_dan'];

					$orderssc[$key]['danshuang_order_peilv'] = $orderssc[$key]['rand1_dan'] + $orderssc[$key]['rand2_dan'] + $orderssc[$key]['rand3_dan'] + $orderssc[$key]['rand4_dan'] + $orderssc[$key]['rand5_dan'];

				}
				//这是判断龙虎,13784
				if ($val['type'] == 3) {
					//先预设好 龙 虎 和 玩法的各种中奖结果
					if ($vm == 350 || $vm == 360 || $vm == 380) {
						if ($rand1 > $rand2) {//万千龙虎
							$wq_long = 350;
							if ($vm == $wq_long) {
								$orderssc[$key]['rand1_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}else{
							$wq_hu = 360;
							if ($vm == $wq_hu) {
								$orderssc[$key]['rand1_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}
						//和
						if($rand1 == $rand2){//万千 和
							$wq_he = 380;
							if ($vm == $wq_he) {
								$orderssc[$key]['rand1_lhh'] = 2 * $val['mom'] * $longhuhe['odds'];
							}
						}
					}
					
					if ($vm == 351 || $vm == 361 || $vm == 381) {
						if ($rand1 > $rand3) {//万百
							$wb_long = 351;
							if ($vm == $wb_long) {
								$orderssc[$key]['rand2_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}else{
							$wb_hu = 361;
							if ($vm == $wb_hu) {
								$orderssc[$key]['rand2_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}
						//和
						if($rand1 == $rand3){//万百 和
							$wb_he = 381;
							if ($vm == $wb_he) {
								$orderssc[$key]['rand2_lhh'] = 2 * $val['mom'] * $longhuhe['odds'];
							}
						}
					}
					if ($vm == 352 || $vm == 362 || $vm == 382) {
						if ($rand1 > $rand4) {//万十
							$ws_long = 352;
							if ($vm == $ws_long) {
								$orderssc[$key]['rand3_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}else{
							$ws_hu = 362;
							if ($vm == $ws_hu) {
								$orderssc[$key]['rand3_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}
						//和
						if ($rand1 == $rand4) {
							$ws_he = 382;
							if ($vm == $ws_he) {
								$orderssc[$key]['rand3_lhh'] = 2 * $val['mom'] * $longhuhe['odds'];
							}
						}
					}
					if ($vm == 353 || $vm == 363 || $vm == 383) {
						if ($rand1 > $rand5) {//万个
							$wg_long = 353;
							if ($vm == $wg_long) {
								$orderssc[$key]['rand4_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}else{
							$wg_hu = 363;
							if ($vm == $wg_hu) {
								$orderssc[$key]['rand4_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}
						//和
						if ($rand1 == $rand5) {
							$wg_he = 383;
							if ($vm == $wg_he) {
								$orderssc[$key]['rand4_lhh'] = 2 * $val['mom'] * $longhuhe['odds'];	
							}
						}
					}
					if ($vm == 354 || $vm == 364 || $vm == 384) {
						if ($rand2 > $rand3) {//千百
							$qb_long = 354;
							if ($vm == $qb_long) {
								$orderssc[$key]['rand5_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}else{
							$qb_hu = 364;
							if ($vm == $qb_hu) {
								$orderssc[$key]['rand5_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}
						//和
						if ($rand2 == $rand3) {
							$qb_he = 384;
							if ($vm == $qb_he) {
								$orderssc[$key]['rand5_lhh'] = 2 * $val['mom'] * $longhuhe['odds'];
							}
						}
					}
					if ($vm == 355 || $vm == 365 || $vm == 385) {
						if ($rand2 > $rand4) {//千十
							$qs_long = 355;
							if ($vm == $qs_long) {
								$orderssc[$key]['rand6_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}else{
							$qs_hu = 365;
							if ($vm == $qs_hu) {
								$orderssc[$key]['rand6_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}
						//和
						if ($rand2 == $rand4) {
							$qs_he = 385;
							if ($vm == $qs_he) {
								$orderssc[$key]['rand6_lhh'] = 2 * $val['mom'] * $longhuhe['odds'];	
							}
						}
					}
					if ($vm == 356 || $vm == 366 || $vm == 386) {
						if ($rand2 > $rand5) {//千个
							$qg_long = 356;
							if ($vm == $qg_long) {
								$orderssc[$key]['rand7_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}else{
							$qg_hu = 366;
							if ($vm == $qg_hu) {
								$orderssc[$key]['rand7_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}
						//和
						if ($rand2 == $rand5) {
							$qg_he = 386;
							if ($vm == $qg_he) {
								$orderssc[$key]['rand7_lhh'] = 2 * $val['mom'] * $longhuhe['odds'];	
							}
						}
					}
					if ($vm == 357 || $vm == 367 || $vm == 387) {
						if ($rand3 > $rand4) {//百十
							$bs_long = 357;
							if ($vm == $bs_long) {
								$orderssc[$key]['rand8_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}else{
							$bs_hu = 367;
							if ($vm == $bs_hu) {
								$orderssc[$key]['rand8_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}
						//和
						if ($rand3 == $rand4) {
							$bs_he = 387;
							if ($vm == $bs_he) {
								$orderssc[$key]['rand8_lhh'] = 2 * $val['mom'] * $longhuhe['odds'];
							}
						}
					}
					if ($vm == 358 || $vm == 368 || $vm == 388) {
						if ($rand3 > $rand5) {//百个
							$bg_long = 358;
							if ($vm == $bg_long) {
								$orderssc[$key]['rand9_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}else{
							$bg_hu = 368;
							if ($vm == $bg_hu) {
								$orderssc[$key]['rand9_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}
						//和
						if ($rand3 == $rand5) {
							$bg_he = 388;
							if ($vm == $bg_he) {
								$orderssc[$key]['rand9_lhh'] = 2 * $val['mom'] * $longhuhe['odds'];	
							}
						}
					}
					if ($vm == 359 || $vm == 369 || $vm == 389) {
						if ($rand4 > $rand5) {//十个
							$sg_long = 359;
							if ($vm == $sg_long) {
								$orderssc[$key]['rand0_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}else{
							$sg_hu = 369;
							if ($vm == $sg_hu) {
								$orderssc[$key]['rand0_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}
						//和
						if ($rand4 == $rand5) {
							$sg_he = 389;
							if ($vm == $sg_he) {
								$orderssc[$key]['rand0_lhh'] = 2 * $val['mom'] * $longhuhe['odds'];
							}
						}
					}

					$orderssc[$key]['longhu_peilv'] = $orderssc[$key]['rand1_lh'] + $orderssc[$key]['rand2_lh'] + $orderssc[$key]['rand3_lh'] + $orderssc[$key]['rand4_lh'] + $orderssc[$key]['rand5_lh'] + $orderssc[$key]['rand6_lh'] + $orderssc[$key]['rand7_lh'] + $orderssc[$key]['rand8_lh'] + $orderssc[$key]['rand9_lh'] + $orderssc[$key]['rand0_lh'];
					$orderssc[$key]['longhu_order_peilv'] = $orderssc[$key]['rand1_lh'] + $orderssc[$key]['rand2_lh'] + $orderssc[$key]['rand3_lh'] + $orderssc[$key]['rand4_lh'] + $orderssc[$key]['rand5_lh'] + $orderssc[$key]['rand6_lh'] + $orderssc[$key]['rand7_lh'] + $orderssc[$key]['rand8_lh'] + $orderssc[$key]['rand9_lh'] + $orderssc[$key]['rand0_lh'];

					//龙虎和
					$orderssc[$key]['longhuhe_peilv'] = $orderssc[$key]['rand1_lhh'] + $orderssc[$key]['rand2_lhh'] + $orderssc[$key]['rand3_lhh'] + $orderssc[$key]['rand4_lhh'] + $orderssc[$key]['rand5_lhh'] + $orderssc[$key]['rand6_lhh'] + $orderssc[$key]['rand7_lhh'] + $orderssc[$key]['rand8_lhh'] + $orderssc[$key]['rand9_lhh'] + $orderssc[$key]['rand0_lhh'];
					$orderssc[$key]['longhuhe_order_peilv'] = $orderssc[$key]['rand1_lhh'] + $orderssc[$key]['rand2_lhh'] + $orderssc[$key]['rand3_lhh'] + $orderssc[$key]['rand4_lhh'] + $orderssc[$key]['rand5_lhh'] + $orderssc[$key]['rand6_lhh'] + $orderssc[$key]['rand7_lhh'] + $orderssc[$key]['rand8_lhh'] + $orderssc[$key]['rand9_lhh'] + $orderssc[$key]['rand0_lhh'];
					
				
				}
				//这是判断直选的13784
				if ($val['type'] == 4) {
					//万位
					if ($rand1 == 0) {$wan = 4700;}
					if ($rand1 == 1) {$wan = 4701;}
					if ($rand1 == 2) {$wan = 4702;}
					if ($rand1 == 3) {$wan = 4703;}
					if ($rand1 == 4) {$wan = 4704;}
					if ($rand1 == 5) {$wan = 4705;}
					if ($rand1 == 6) {$wan = 4706;}
					if ($rand1 == 7) {$wan = 4707;}
					if ($rand1 == 8) {$wan = 4708;}
					if ($rand1 == 9) {$wan = 4709;}

					if ($rand2 == 0) {$qian = 4710;}
					if ($rand2 == 1) {$qian = 4711;}
					if ($rand2 == 2) {$qian = 4712;}
					if ($rand2 == 3) {$qian = 4713;}
					if ($rand2 == 4) {$qian = 4714;}
					if ($rand2 == 5) {$qian = 4715;}
					if ($rand2 == 6) {$qian = 4716;}
					if ($rand2 == 7) {$qian = 4717;}
					if ($rand2 == 8) {$qian = 4718;}
					if ($rand2 == 9) {$qian = 4719;}

					if ($rand3 == 0) {$bai = 4720;}
					if ($rand3 == 1) {$bai = 4721;}
					if ($rand3 == 2) {$bai = 4722;}
					if ($rand3 == 3) {$bai = 4723;}
					if ($rand3 == 4) {$bai = 4724;}
					if ($rand3 == 5) {$bai = 4725;}
					if ($rand3 == 6) {$bai = 4726;}
					if ($rand3 == 7) {$bai = 4727;}
					if ($rand3 == 8) {$bai = 4728;}
					if ($rand3 == 9) {$bai = 4729;}

					if ($rand4 == 0) {$shi = 4730;}
					if ($rand4 == 1) {$shi = 4731;}
					if ($rand4 == 2) {$shi = 4732;}
					if ($rand4 == 3) {$shi = 4733;}
					if ($rand4 == 4) {$shi = 4734;}
					if ($rand4 == 5) {$shi = 4735;}
					if ($rand4 == 6) {$shi = 4736;}
					if ($rand4 == 7) {$shi = 4737;}
					if ($rand4 == 8) {$shi = 4738;}
					if ($rand4 == 9) {$shi = 4739;}

					if ($rand5 == 0) {$ge = 4740;}
					if ($rand5 == 1) {$ge = 4741;}
					if ($rand5 == 2) {$ge = 4742;}
					if ($rand5 == 3) {$ge = 4743;}
					if ($rand5 == 4) {$ge = 4744;}
					if ($rand5 == 5) {$ge = 4745;}
					if ($rand5 == 6) {$ge = 4746;}
					if ($rand5 == 7) {$ge = 4747;}
					if ($rand5 == 8) {$ge = 4748;}
					if ($rand5 == 9) {$ge = 4749;}
					// $orderssc[$key]['danxuan_peilv'] = 0;
					if ($vm == 4700 || $vm == 4701 || $vm == 4702 || $vm == 4703 || $vm == 4704 || $vm == 4705 || $vm == 4706 || $vm == 4707 || $vm == 4708 || $vm == 4709) {
						if ($vm == $wan) {
							$rand1_danxuan = 2 * $val['mom'] * $danxuan['odds'];
							$orderssc[$key]['danxuan_wan'] = $rand1_danxuan;	
						}
					}
					//千位
					if ($vm == 4710 || $vm == 4711 || $vm == 4712 || $vm == 4713 || $vm == 4714 || $vm == 4715 || $vm == 4716 || $vm == 4717 || $vm == 4718 || $vm == 4719) {
						if ($vm == $qian) {
							$rand2_danxuan = 2 * $val['mom'] * $danxuan['odds'];
							$orderssc[$key]['danxuan_qian'] = $rand2_danxuan;
						}
					}
					//百位
					if ($vm == 4720 || $vm == 4721 || $vm == 4722 || $vm == 4723 || $vm == 4724 || $vm == 4725 || $vm == 4726 || $vm == 4727 || $vm == 4728 || $vm == 4729) {
						if ($vm == $bai) {
							$rand3_danxuan = 2 * $val['mom'] * $danxuan['odds'];
							$orderssc[$key]['danxuan_bai'] = $rand3_danxuan;
						}
					}
					//十位
					if ($vm == 4730 || $vm == 4731 || $vm == 4732 || $vm == 4733 || $vm == 4734 || $vm == 4735 || $vm == 4736 || $vm == 4737 || $vm == 4738 || $vm == 4739) {
						if ($vm == $shi) {
							$rand4_danxuan = 2 * $val['mom'] * $danxuan['odds'];
							$orderssc[$key]['danxuan_shi'] = $rand4_danxuan;
						}
					}
					//个位
					if ($vm == 4740 || $vm == 4741 || $vm == 4742 || $vm == 4743 || $vm == 4744 || $vm == 4745 || $vm == 4746 || $vm == 4747 || $vm == 4748 || $vm == 4749) {
						if ($vm == $ge) {
							$rand5_danxuan = 2 * $val['mom'] * $danxuan['odds'];
							$orderssc[$key]['danxuan_ge'] = $rand5_danxuan;
						}
					}
				
				}
				$orderssc[$key]['danxuan_peilv'] = $orderssc[$key]['danxuan_wan'] + $orderssc[$key]['danxuan_qian'] + $orderssc[$key]['danxuan_bai'] +$orderssc[$key]['danxuan_shi'] +$orderssc[$key]['danxuan_ge'];

				$orderssc[$key]['danxuan_order_peilv'] = $orderssc[$key]['danxuan_wan'] + $orderssc[$key]['danxuan_qian'] + $orderssc[$key]['danxuan_bai'] +$orderssc[$key]['danxuan_shi'] +$orderssc[$key]['danxuan_ge'];
			
			}
			
		}
		// print_r($orderssc);exit;
		//计算是否这个随机数合适
		foreach ($orderssc as $k => $v) {
			$order_price += $v['money'];//中的订单金额
			$daixao_peilv += $v['daixao_peilv'];
			$danshuang_peilv += $v['danshuang_peilv'];
			$danxuan_peilv += $v['danxuan_peilv'];
			$longhu_peilv += $v['longhu_peilv'];
			$longhuhe_peilv += $v['longhuhe_peilv'];
		}
		$all_peilv = $daixao_peilv + $danshuang_peilv + $danxuan_peilv + $longhu_peilv + $longhuhe_peilv;
		$zhongjiancha = $order_price - $all_peilv;//总价-总的赔率

		// $file  = 'log4.txt';
  //       $content =date("Y/m/d h:i:sa").'  zhongjiancha=  '.$zhongjiancha."\r\n";
  //       if($f  = file_put_contents($file, $content,FILE_APPEND)){
  //           echo "写入成功4。<br />";
  //       }

		if ($zhongjiancha < 0) {
			//未中奖
			// print_r($zhongjiancha);echo "<br/>";
		}else{
			// print_r($orderssc);exit;

			//五位随机数修改到开奖记录表中
			$sj_sql="update {$this->prename}datassc set m1=".$rand1.",m2=".$rand2.",m3=".$rand3.",m4=".$rand4.",m5=".$rand5.",m_money=".$zhongjiancha." where qishu=".$qishu."";
			$this->update($sj_sql);

			//记录成功的订单做记号，计算所得到的中奖金额
			foreach ($orderssc as $kym => $vym) {
				//这四个类型中中奖的且不为0的
				if ($vym['daixao_peilv'] != 0 || $vym['danshuang_peilv'] != 0 || $vym['danxuan_peilv'] != 0 || $vym['longhu_peilv'] != 0) {
					
					if ($vym['type'] == 1) {//大小的
						$dx_wintime = time();
						$dx_sql="update {$this->prename}orderssc set jisuan_statu=1,winmoney=".$vym['daixao_order_peilv'].",wintime=".$dx_wintime." where id=".$vym['id'];
						$this->update($dx_sql);
					}
					if ($vym['type'] == 2) {//单双的
						$ds_wintime = time();
						$ds_sql="update {$this->prename}orderssc set jisuan_statu=1,winmoney=".$vym['danshuang_order_peilv'].",wintime=".$ds_wintime." where id=".$vym['id'];
						$this->update($ds_sql);
					}
					if ($vym['type'] == 3) {//龙虎的
						$longhu_prices = $vym['longhu_order_peilv'] + $vym['longhuhe_order_peilv'];
						$lh_wintime = time();
						$lh_sql="update {$this->prename}orderssc set jisuan_statu=1,winmoney=".$longhu_prices.",wintime=".$lh_wintime." where id=".$vym['id'];
						$this->update($lh_sql);
					}
					if ($vym['type'] == 4) {//直选的
						$zx_wintime = time();
						$zx_sql="update {$this->prename}orderssc set jisuan_statu=1,winmoney=".$vym['danxuan_order_peilv'].",wintime=".$zx_wintime." where id=".$vym['id'];
						$this->update($zx_sql);
					}
				}else{
						$mmtime = time();
						$mm_sql="update {$this->prename}orderssc set jisuan_statu=0,wintime=".$mmtime." where id=".$vym['id'];
						$this->update($mm_sql);
				}
				
			}

			/*如果是手动就直接写入账户*/
			if ($shoudong == 20) {
				//开奖数据
				$sd_sqls="update {$this->prename}datassc set ball1=".$rand1.",ball2=".$rand2.",ball3=".$rand3.",ball4=".$rand4.",ball5=".$rand5.",status=1,all_money=".$zhongjiancha." where qishu=".$qishu."";
				$this->update($sd_sqls);
				//订单数据修改
				$orderDatas=$this->getRows("select * from {$this->prename}orderssc where qishu=".$qishu."");
				foreach ($orderDatas as $key => $val) {
					//已中奖的
					if ($val['jisuan_statu'] == 1) {
						$shoudong_sql="update {$this->prename}orderssc set kai=1 where id=".$val['id'];
						$this->update($shoudong_sql);

						//修改用户
						$sd_coin = $this->getValue("select coin from {$this->prename}members where uid=".$val['uid']."");
						$all_sd_coin = $sd_coin + $val['winmoney'];//用户de钱
						$sd_sql="update {$this->prename}members set coin=".$all_sd_coin." where uid=".$val['uid'];
						$this->update($sd_sql);

					}else{
						$shoudong_sql="update {$this->prename}orderssc set kai=2 where id=".$val['id'];
						$this->update($shoudong_sql);
					}
				}
			}
			echo 200;exit;
			// print_r($rand1.$rand2.$rand3.$rand4.$rand5);exit;

			
			
		}
		
			// echo "111";exit;
		// print_r($daxiao_pei);exit;
	}
	

	//这是调用的函数
	public final function rand_data(){//传期数
		set_time_limit(0);//时间永不过期
		$get_qishu = $_GET['qishu'];
		$shoudong  = $_GET['shoudong'];
		if ($get_qishu) {
			//后台手动请求
			$status = $this->getValue("select status from {$this->prename}datassc where qishu=".$get_qishu."");
			if ($status == 0) {
				//一次生成1000个随机数
				for ($i=0; $i < 1000; $i++) {
					//设置一个5位随机数
					$rand1 = rand(0,9);
					$rand2 = rand(0,9);
					$rand3 = rand(0,9);
					$rand4 = rand(0,9);
					$rand5 = rand(0,9);
					$data = $rand1.','.$rand2.','.$rand3.','.$rand4.','.$rand5;
					$arr_data[] = $data;
				}
				$a = $this->rand_data_two($arr_data,$get_qishu,$shoudong);
			}

		}else{
			//前端自动调用
			$res = $this->getRows("select * from {$this->prename}datassc order by id desc limit 1,1");
			$qishu = $res[0]['qishu'];
			if ($res[0]['status'] == 0) {
				//一次生成1000个随机数
				for ($i=0; $i < 1000; $i++) {
					//设置一个5位随机数
					$rand1 = rand(0,9);
					$rand2 = rand(0,9);
					$rand3 = rand(0,9);
					$rand4 = rand(0,9);
					$rand5 = rand(0,9);
					$data = $rand1.','.$rand2.','.$rand3.','.$rand4.','.$rand5;
					$arr_data[] = $data;
				}
				$a = $this->rand_data_two($arr_data,$qishu,'');
			}
		}
		
		// print_r($arr_data);
	}
	public final function rand_data_two($arr_data,$qishu,$shoudong){
		foreach ($arr_data as $key => $value) {

			$this->orderproces($value,$qishu,$shoudong);
	         
			// echo $fan;echo "<br/>";
			
		}
	}

	public final function kaijiang(){
    	// echo 1;
    	$kaijiang = $this->change_order();//修改订单，开奖
    	if ($kaijiang) {
    		echo 200;
    	}
    }


	//下一期期数和时间插入  
	public final function sscplay(){
		date_default_timezone_set('PRC');
		$qishu = $_POST['qishu'];
		if(empty($qishu)){
			$qishu = 1;
		}
		$sql="select * from {$this->prename}datassc where qishu=?";
		$res=$this->getRow($sql, $qishu);
		$time = strtotime($res['time'])+300;
		if($res){
	        $da['time'] = date('Y-m-d H:i:s',$time);
			$da['qishu'] =$res['qishu']+1;
		}else{
		    $times = time()+300;
			$da['time'] = date('Y-m-d H:i:s',$times);
			$da['qishu'] =$qishu;
		}
		$qi = "select * from {$this->prename}datassc where qishu=?";
		$resqi=$this->getRow($sql, $da['qishu']);
		if(!$resqi && !empty($qishu)){
			  

			  $info = "insert into ssc_datassc (qishu,time) values ('{$da['qishu']}', '{$da['time']}')";
              $datassc = $this->insert($info);
              if($datassc){
			  	$this->rand_data();//进行预算订单
                echo 200;
              }else{
                echo 201;
              }
            }else{
               echo 201;
        }

	}
	public final function lius(){
		$auto = $this->getRow("select * from {$this->prename} auto_0 order by id desc");
		if($auto){
           $autos = $this->getRow("select * from {$this->prename}auto_0 order by id desc limit 1");

		}else{

		}
		echo "<pre>";
		var_dump($auto);
		echo "<pre>";
	}

	public final function hongplay(){
		date_default_timezone_set('PRC');
		$totime = time();
		$qishu = $_POST['qishu'];
		//判断数据库是否存在数据
		$insf = $this->getRow("select * from {$this->prename}auto_0 order by id desc");
		if($insf){
			 $ins = $this->getRow("select * from {$this->prename}auto_0 order by id desc limit 1");
			 $da['actionNo'] =$ins['actionNo']+1;
			 $sun = date("w");
			 $notime = strtotime($ins['actionTime']);
			 if($sun == 6 && $notime <= $totime){
			    $time = strtotime($ins['actionTime'])+72*3600;
	         }else{
	            $time = strtotime($ins['actionTime'])+48*3600;
	         }	
	      /*   if($sun == 6 && $notime <= $totime){
			    $time = strtotime($ins['actionTime'])+72*3600;
	         }else{
	            $time = strtotime($ins['actionTime'])+48*3600;
	         }*/	
             /*if(($sun == 1 || $sun == 6 || $sun == 0 || $sun == 2) && $notime <= $totime){
                $time = strtotime($ins['actionTime'])+72*3600;
             }else{
                $time = strtotime($ins['actionTime'])+48*3600;
             }*/
		     $da['actionTime'] = date('Y-m-d H:i:s',$time);
		     //存在修改最新一期数据
		     $action = $this->getRow("select * from {$this->prename}auto_0 where action=$qishu");
		     $msg = $this->liu();
		     //判断是否存在该期数，该期数是否修改
		     if($msg['actionNo'] == $qishu || $action['ok'] == 0){
		     	if($this->updateRows($this->prename.'auto_0', $msg, 'actionNo='.$msg['actionNo'])){
		     	   //六合彩中奖订单修改
	               $this->autos();
	               $msgdata['type'] = 30;
                   $msgdata['time'] = $msg['otime'];
                   $msgdata['number'] = $msg['actionNo'];
                   $msgdata['data'] = $msg['ball'];
                   //判断ssc_data是否存在最新一期数据
                   $actiondata = $this->getRow("select * from {$this->prename}data where number={$msgdata['number']}");
                   if(!$actiondata){
                    		if($this->insertRow($this->prename.'data', $msgdata)){
		                	 $datas = $this->lastInsertId();
		                }
	                }
                }
		     }
		}else{
			     //不存在数据进行数据插入
				 $msg = $this->liu();
				 if($this->insertRow($this->prename.'auto_0', $msg)){
				 	   $inss = $this->lastInsertId();
				 	   if($inss){
				 	    $sqls = "select * from {$this->prename}auto_0 where id=?";
		                $ress = $this->getRow($sqls, $inss);
		                $da['actionNo'] =$ress['actionNo']+1;
		                $sun = date("w");
		                $totime = strtotime($ress['actionTime']);
		                if(($sun == 1 || $sun == 2 || $sun == 6 || $sun == 0) && $notime <= $totime){
                           $time = strtotime($ress['actionTime'])+72*3600;
		                }else{
			               $time = strtotime($ress['actionTime'])+48*3600;
		                }
		                $da['actionTime'] = date('Y-m-d H:i:s',$time);
		                $msgdata['type'] = 30;
		                $msgdata['time'] = $msg['otime'];
		                $msgdata['number'] = $msg['actionNo'];
		                $msgdata['data'] = $msg['ball'];
		                //判断ssc_data是否存在最新一期数据
                        $actiondata = $this->getRow("select * from {$this->prename}data where number={$msgdata['number']}");
                        if(!$actiondata){
                        		if($this->insertRow($this->prename.'data', $msgdata)){
			                	 $datas = $this->lastInsertId();
			                }
		                }
			      }
			} 
		}
			 	
		$sql = "select * from {$this->prename}auto_0 where actionNo=?";
		$resqi=$this->getRow($sql, $da['actionNo']);
		if(!$resqi){
			  $info = "insert into ssc_auto_0 (actionNo,actionTime) values ('{$da['actionNo']}', '{$da['actionTime']}')";
              $datassc = $this->insert($info);
              if($datassc){
                echo 200;
              }else{
                echo 201;
              }
            }else{
               echo 201;
        }

	}

	public final function liu(){
		 date_default_timezone_set('PRC'); 
		$msg = file_get_contents("http://api.caipiaokong.cn/lottery/?name=xglhc&format=json&uid=781488&token=76e0587095e4fb073eb4464af8f6028271823950");
		$data = json_decode($msg,true);
		$datas = array();
        foreach ($data as $key => $value) {
            $datas[]['expect'] = $key;
            $datas[] = $value;
        }
         $datamsg['actionNo'] = $datas[0]['expect'];
        if(!empty($datas[1]['dateline']) && (!empty($datas[1]['number']))){
            $code = explode(",",$datas[1]['number']);
            $datamsg['ball1'] = $code[0];
            $datamsg['ball2'] = $code[1];
            $datamsg['ball3'] = $code[2];
            $datamsg['ball4'] = $code[3];
            $datamsg['ball5'] = $code[4];
            $datamsg['ball6'] = $code[5];
            $datamsg['ball7'] = $code[6];
            $datamsg['opentime'] = $datas[1]['dateline'];
            /*$datamsg['actionTime'] = $datas[1]['dateline'];*/
            $datamsg['ok'] = 1;
            $datamsg['otime'] = strtotime($datamsg['opentime']);
            $datamsg['ball'] = $datas[1]['number'];
            $auto = $this->getRow("select * from {$this->prename}auto_0 order by id desc");
            //不存在插入最新一期的数据
            if(!$auto){
            	$datamsg['actionTime'] = $datas[1]['dateline'];
            }
            return $datamsg;
        } 
	}

	public final function liuhe(){
		//当前时间
		$totime = time();
		date_default_timezone_set('PRC'); 
		$msg = file_get_contents("http://api.caipiaokong.cn/lottery/?name=xglhc&format=json&uid=781488&token=76e0587095e4fb073eb4464af8f6028271823950");
		$data = json_decode($msg,true);
		$datas = array();
        foreach ($data as $key => $value) {
            $datas[]['expect'] = $key;
            $datas[] = $value;
        }
           $datamsg['actionNo'] = $datas[0]['expect'];
           $actionNos = $datas[0]['expect']; 
        if(!empty($datas[1]['dateline']) && (!empty($datas[1]['number']))){
            $code = explode(",",$datas[1]['number']);
            $datamsg['ball1'] = $code[0];
            $datamsg['ball2'] = $code[1];
            $datamsg['ball3'] = $code[2];
            $datamsg['ball4'] = $code[3];
            $datamsg['ball5'] = $code[4];
            $datamsg['ball6'] = $code[5];
            $datamsg['ball7'] = $code[6];
            $datamsg['opentime'] = $datas[1]['dateline'];
            /*$datamsg['actionTime'] = $datas[1]['dateline'];*/
            $datamsg['ok'] = 1;
            $datamsg['otime'] = strtotime($datamsg['opentime']);
            $datamsg['ball'] = $datas[1]['number'];
            //查询数据库是否存在数据
            $auto = $this->getRow("select * from {$this->prename}auto_0 order by id desc");
			if($auto){
				date_default_timezone_set('PRC'); 
	            $autos = $this->getRow("select * from {$this->prename}auto_0 order by id desc limit 1");  
	            $da['actionNo'] =$autos['actionNo']+1;
	            $notime = strtotime($autos['actionTime']);
			    $sun = date("w");
			    if($sun == 6 && $notime <= $totime){
			       $time = strtotime($autos['actionTime'])+72*3600;
                }else{
	               $time = strtotime($autos['actionTime'])+48*3600;
                }	
                /*if(($sun == 1 || $sun == 2 || $sun == 6 || $sun == 0) && $notime <= $totime){
                   $time = strtotime($autos['actionTime'])+72*3600;
                }else{
	               $time = strtotime($autos['actionTime'])+48*3600;
                }*/
		        $da['actionTime'] = date('Y-m-d H:i:s',$time);
		        $sql = "select * from {$this->prename}auto_0 where actionNo=?";
		        $resqi=$this->getRow($sql, $da['actionNo']);
		        if(!$resqi && ($notime <= $totime)){
		        	 $info = "insert into ssc_auto_0 (actionNo,actionTime) values ('{$da['actionNo']}', '{$da['actionTime']}')";
		             $datassc = $this->insert($info);
		             if($datassc){
		             	echo "存在插入最新一期的数据"."<br>";
		             }
		        }else{
		        	echo "该期数已经存在或时间未到"."<br>";
		        }
		             //修改最新一期的开奖数据
		              $sqls = $this->getRow("select * from {$this->prename}auto_0 order by id desc limit 1,1");
		              if($sqls['actionNo'] == $datamsg['actionNo'] && $sqls['ok'] == 0){
                           if($this->updateRows($this->prename.'auto_0', $datamsg, 'actionNo='.$datamsg['actionNo'])){
                           	   //六合彩中奖订单修改
	                           $this->autos();
                           	   echo "修改成功"."<br>";
				               $msgdata['type'] = 30;
			                   $msgdata['time'] = $datamsg['otime'];
			                   $msgdata['number'] = $datamsg['actionNo'];
			                   $msgdata['data'] = $datamsg['ball'];
			                   if($this->insertRow($this->prename.'data', $msgdata)){
			                	   $datas = $this->lastInsertId();
			                	   if($datas){
			                	 	  echo "开奖结果数据插入ssc_data表";
			                	   }
			                    }
                           }else{
                           	   echo "修改失败"."<br>";
                           }
		              }else{
		              	echo "该期数已经存在或已经修改或改期还未开奖"."<br>";
		              }
			}else{
				 //不存在插入最新一期的数据
				 $datamsg['actionTime'] = $datas[1]['dateline'];
				 if($this->insertRow($this->prename.'auto_0', $datamsg)){
				 	   $inss = $this->lastInsertId();
				 	   if($inss){
				 	   	echo "不存在插入最新一期的数据"."<br>";
				 	   	//下一期的时间和期数插入
				 	    $sqls = "select * from {$this->prename}auto_0 where id=?";
		                $ress = $this->getRow($sqls, $inss);
		                $da['actionNo'] =$ress['actionNo']+1;
			            $notime = strtotime($ress['actionTime']);
					    $sun = date("w");
		                if(($sun == 1 || $sun == 2 || $sun == 6 || $sun == 0) && $notime <= $totime){
		                   $time = strtotime($ress['actionTime'])+72*3600;
		                }else{
			               $time = strtotime($ress['actionTime'])+48*3600;
		                }
		                $da['actionTime'] = date('Y-m-d H:i:s',$time);
		                $sql = "select * from {$this->prename}auto_0 where actionNo=?";
		                $resqi=$this->getRow($sql, $da['actionNo']);
				        if(!$resqi){
					       $info = "insert into ssc_auto_0 (actionNo,actionTime) values ('{$da['actionNo']}', '{$da['actionTime']}')";
		                   $datassc = $this->insert($info);
		                 if($datassc){
		                 	echo "下一期的时间和期数插入"."<br>";
		                 	//开奖结果数据插入ssc_data表
			                $msgdata['type'] = 30;
			                $msgdata['time'] = $datamsg['otime'];
			                $msgdata['number'] = $datamsg['actionNo'];
			                $msgdata['data'] = $datamsg['ball'];
			                if($this->insertRow($this->prename.'data', $msgdata)){
			                	 $datas = $this->lastInsertId();
			                	 if($datas){
			                	 	echo "开奖结果数据插入ssc_data表";
			                	 }
			                }
		                  }
		                }
			      }
			  } 

			}
        } 
	}

    public final function sscplays(){
    	 date_default_timezone_set('PRC');
    	 //最新一期
    	 $res = $this->getRows("select * from {$this->prename}datassc order by id desc limit 1");
    	 if(!$res){
    	 	echo "数据库内容不存在";
    	 }
    	 //上一期
    	/* $ress = $this->getRows("select * from {$this->prename}datassc order by id desc limit 1,1");*/
    	 $totime = time();
    	 $netime = strtotime($res[0]['time']);
    	 $time = strtotime($res[0]['time'])+300;
    	 //上一期的期数
    	 /*$qishun = $ress[0]['qishu'];*/
    	 //上一期开奖时间
    	 $timef = strtotime($res[0]['time'])-180;
    	 //上一期开奖结果
    	 if($res[0]['status'] == 1){
		     echo "已经开奖"."<br>";
    	 }else{
    	 	 echo "还未开奖"."<br>";
    	 }
    	 if($totime >= $timef){
    	    $this->change_order();//修改订单，开奖
    	 	echo "开奖"."<br>";
    	 }else{
    	 	echo "还未到开奖时间"."<br>";
    	 }
         $da['time'] = date('Y-m-d H:i:s',$time);
		 $da['qishu'] =$res[0]['qishu']+1;
		 if($netime <= $totime){
		 	  $info = "insert into ssc_datassc (qishu,time) values ('{$da['qishu']}', '{$da['time']}')";
              $datassc = $this->insert($info);
              if($datassc){
		 	  	
		 	  	$this->rand_data();//进行预算订单
                
                echo "最新一期数据插入成功"."<br>";
              }else{
                echo "最新一期数据插入失败"."<br>";
              }
          }else{
                echo "还未到最新开奖时间"."<br>";
          }
    }
	/*开奖后修改订单*/
	public final function change_order(){
		//查期数
		$res = $this->getRows("select * from {$this->prename}datassc order by id desc limit 1,1");
		$qishu = $res[0]['qishu'];
		//开奖数据
		$xg_sql="update {$this->prename}datassc set ball1=".$res[0]['m1'].",ball2=".$res[0]['m2'].",ball3=".$res[0]['m3'].",ball4=".$res[0]['m4'].",ball5=".$res[0]['m5'].",status=1,all_money=".$res[0]['m_money']." where qishu=".$res[0]['qishu']."";
		$this->update($xg_sql);

		$orderData=$this->getRows("select * from {$this->prename}orderssc where qishu=".$qishu."");
		foreach ($orderData as $key => $val) {
			//已中奖的
			if ($val['jisuan_statu'] == 1) {
				$jisuan_sql="update {$this->prename}orderssc set kai=1 where id=".$val['id'];
				$this->update($jisuan_sql);

				//修改用户
				$ds_coin = $this->getValue("select coin from {$this->prename}members where uid=".$val['uid']."");
				$all_ds_coin = $ds_coin + $val['winmoney'];//用户de钱
				$ds_sql="update {$this->prename}members set coin=".$all_ds_coin." where uid=".$val['uid'];
				$this->update($ds_sql);

			}else{
				$jisuan_sql="update {$this->prename}orderssc set kai=2 where id=".$val['id'];
				$this->update($jisuan_sql);
			}
		}
	}

	//六合彩开奖结果期数
	public final function autos(){
		$sql = $this->getRows("select * from {$this->prename}auto_0 where ok = 1 order by id desc");
		foreach ($sql as $key => $value) {
               $this->kai($value['actionNo']);
        }
	}
    
    //六合彩订单列表计算
	public final function kai($qishu){
		$sql = $this->getRows("select * from {$this->prename}auto_0 where actionNo = {$qishu} and ok = 1");
		$ba['actionNo'] = $sql[0]['actionNo'];
		$ba['ball1'] = $sql[0]['ball1'];
		$ba['ball2'] = $sql[0]['ball2'];
		$ba['ball3'] = $sql[0]['ball3'];
		$ba['ball4'] = $sql[0]['ball4'];
		$ba['ball5'] = $sql[0]['ball5'];
		$ba['ball6'] = $sql[0]['ball6'];
		$ba['ball7'] = $sql[0]['ball7'];
		$ac['lotteryNo'] = $sql[0]['ball'];
		$ball = explode(',', $ac['lotteryNo']);
        //开出的球号十位
        $ball11 = substr($ball[0],0,1);
        $ball22 = substr($ball[1],0,1);
        $ball33 = substr($ball[2],0,1);
        $ball44 = substr($ball[3],0,1);
        $ball55 = substr($ball[4],0,1);
        $ball66 = substr($ball[5],0,1);
        $ball77 = substr($ball[6],0,1);
        //开出的球号ge位
        $ball1 = substr($ball[0],1,1);
        $ball2 = substr($ball[1],1,1);
        $ball3 = substr($ball[2],1,1);
        $ball4 = substr($ball[3],1,1);
        $ball5 = substr($ball[4],1,1);
        $ball6 = substr($ball[5],1,1);
        $ball7 = substr($ball[6],1,1);
        //总和
        $ballall = $ba['ball1']+$ba['ball2']+$ba['ball3']+$ba['ball4']+$ba['ball5']+$ba['ball6']+$ba['ball7'];
		if($this->updateRows($this->prename.'bets', $ac, 'actionNo='.$ba['actionNo'])){
			$sqldata = $this->getRows("select * from {$this->prename}bets where type=30 and actionNo = {$ba['actionNo']} and isDelete=0 and flag=0 order by id desc");
			foreach ($sqldata as $key => $value) {
				//特别号玩法
				//单个球号下注
				//中奖状态
				$msg['zjCount'] =  1; 
				//中奖金额
				$msg['bonus'] = $value['actionAmount'] * $value['bonusProp'];
				$msg['qz_time'] =  time();
				if($value['playedId'] == 308){
					if($value['actionName'] == "SP"){
						if($ba['ball7'] == $value['actionData']){
							if($value['zjCount'] == 0){
								$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								/*if($this->updateRows($this->prename.'bets', $msg, 'id='.$value['id'])){
			                        $sqls = $this->getRows("select coin from {$this->prename}members where uid = {$value['uid']}");
			                        $user['coin'] =  $sqls[0]['coin']+$msg['bonus'];
			                        $this->updateRows($this->prename.'members', $user, 'uid='.$value['uid']);
								}*/
							}
						}
					}
                    //特大、特小、特单、特双
					if($value['actionName'] == "SPBSOE"){
						//特大
						if($value['actionData'] == "特大"){
							if($ba['ball7'] >= 25 && $ba['ball7'] != 49){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
							}
						}

						//特小
						if($value['actionData'] == "特小"){
							if($ba['ball7'] <= 24){
								if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
							}
						}

						//特单
						if($value['actionData'] == "特单" && $ba['ball7'] != 49){
							if($ba['ball7'] % 2 != 0){
								if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
							}
						}

						//特双
						if($value['actionData'] == "特双"){
							if($ba['ball7'] % 2 == 0){
								if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
							}
						}
					}

					//大单、大双、小单、小双
					if($value['actionName'] == "SPH2"){
						//大单
						if($value['actionData'] == "大单"){
                            if($ba['ball7'] >= 25 && $ba['ball7'] % 2 != 0 && $ba['ball7'] != 49){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
                            }
						}

						//小单
						if($value['actionData'] == "小单"){
							if($ba['ball7'] <= 24 && $ba['ball7'] % 2 != 0 && $ba['ball7'] != 49){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
                            }
						}

						//大双
						if($value['actionData'] == "大双"){
							if($ba['ball7'] >= 25 && $ba['ball7'] % 2 == 0 && $ba['ball7'] != 49){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
                            }
						}

						//小双
						if($value['actionData'] == "小双"){
                            if($ba['ball7'] <= 24 && $ba['ball7'] % 2 == 0){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
                            }
						}
					}

					//合大、合小、合单、合双
					if($value['actionName'] == "SPTBSOE"){
						$all = substr($ball[6], 0 , 1) + substr($ball[6], 1 , 1);
						//合大
						if($value['actionData'] == "合大"){
						    if($all > 6 && $ba['ball7'] != 49){
								if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
							}
						}

						//合小
						if($value['actionData'] == "合小"){
						    if($all < 7){
								if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
							}
						}

						//合单
						if($value['actionData'] == "合单"){
							if($all % 2 != 0 && $ba['ball7'] != 49){
								if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
							}
						}

						//合双
						if($value['actionData'] == "合双"){
						    if($all % 2 == 0){
								if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
							}
						}
					}

					//特尾大、特尾小
					if($value['actionName'] == "SPSBS"){
						$all = substr($ball[6], 1 , 1);
						//特尾大
						if($value['actionData'] == "特尾大"){
                            if($all >= 5 && $all <= 9 && $ba['ball7'] != 49){
                            	if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
                            }
						}

						//特尾小
						if($value['actionData'] == "特尾小"){
							 if($all >= 0 && $all <= 4){
							 	if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
                            }
						}
					}
				}

				//生肖头像
				if($value['playedId'] == 309){
					//鼠、牛、虎、兔、龙、蛇、马、羊、猴、鸡、狗、猪
					if($value['actionName'] == "SPANM"){
						//鼠
                        if($value['actionData'] == "鼠"){
	                        if($ball[6] == 11 || $ball[6] == 23 || $ball[6] == 35 || $ball[6] == 47){
	                        	if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
	                        } 
                        }
                        //牛
                        if($value['actionData'] == "牛"){
	                        if($ball[6] == 10 || $ball[6] == 22 || $ball[6] == 34 || $ball[6] == 46){
	                        	if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
	                        } 
                        }
                        
                        //虎
                        if($value['actionData'] == "虎"){
	                        if($ball[6] == 09 || $ball[6] == 21 || $ball[6] == 33 || $ball[6] == 45){
	                        	if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
	                        } 
                        }

                        //兔
                        if($value['actionData'] == "兔"){
	                        if($ball[6] == 08 || $ball[6] == 20 || $ball[6] == 32 || $ball[6] == 44){
	                        	if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
	                        } 
                        }

                        //龙
                        if($value['actionData'] == "龙"){
	                        if($ball[6] == 07 || $ball[6] == 19 || $ball[6] == 31 || $ball[6] == 43){
	                        	if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
	                        } 
                        }

                        //蛇
                        if($value['actionData'] == "蛇"){
	                        if($ball[6] == 06 || $ball[6] == 18 || $ball[6] == 30 || $ball[6] == 42){
	                        	if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
	                        } 
                        }

                        //马
                        if($value['actionData'] == "马"){
	                        if($ball[6] == 05 || $ball[6] == 17 || $ball[6] == 29 || $ball[6] == 41){
	                        	if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
	                        } 
                        }

                        //羊
                        if($value['actionData'] == "羊"){
	                        if($ball[6] == 04 || $ball[6] == 16 || $ball[6] == 28 || $ball[6] == 40){
	                        	if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
	                        } 
                        }

                        //猴
                        if($value['actionData'] == "猴"){
	                        if($ball[6] == 03 || $ball[6] == 15 || $ball[6] == 27 || $ball[6] == 39){
	                        	if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
	                        } 
                        }

                        //鸡
                        if($value['actionData'] == "鸡"){
	                        if($ball[6] == 02 || $ball[6] == 14 || $ball[6] == 26 || $ball[6] == 38){
	                        	if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
	                        } 
                        }

                        //狗
                        if($value['actionData'] == "狗"){
	                        if($ball[6] == 01 || $ball[6] == 13 || $ball[6] == 25 || $ball[6] == 37 || $ball[6] == 49){
	                        	if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
	                        } 
                        }

                        //猪
                        if($value['actionData'] == "猪"){
	                        if($ball[6] == 12 || $ball[6] == 24 || $ball[6] == 36 || $ball[6] == 48){
	                        	if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
	                        } 
                        }

                        //鼠
                        if($value['actionData'] == "鼠"){
	                        if($ball[6] == 11 || $ball[6] == 23 || $ball[6] == 35 || $ball[6] == 47){
	                        	if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
	                        } 
                        }
					}

                    //0头、1头、2头、3头、4头、
                    if($value['actionName'] == "SPTD"){
                    	//0头
                    	if($value['actionData'] == "0头"){
                    		if($ball77 == 0){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
                    		}
                    	}

                    	//1头
                    	if($value['actionData'] == "1头"){
                    		if($ball77 == 1){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
                    		}
                    	}

                    	//2头
                    	if($value['actionData'] == "2头"){
                    		if($ball77 == 2){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
                    		}
                    	}

                    	//3头
                    	if($value['actionData'] == "3头"){
                    		if($ball77 == 3){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
                    		}
                    	}

                    	//4头
                    	if($value['actionData'] == "4头"){
                    		if($ball77 == 4){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
                    		}
                    	}
                    }

                    //0尾、1尾、2尾、3尾、4尾、5尾、6尾、7尾、8尾、9尾、
                    if($value['actionName'] == "SPSD"){
                    	//0尾
                    	if($value['actionData'] == "0尾"){
                    		if($ball7 == 0){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
                    		}
                    	}

                    	//1尾
                    	if($value['actionData'] == "1尾"){
                    		if($ball7 == 1){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
                    		}
                    	}

                    	//2尾
                    	if($value['actionData'] == "2尾"){
                    		if($ball7 == 2){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
                    		}
                    	}

                    	//3尾
                    	if($value['actionData'] == "3尾"){
                    		if($ball7 == 3){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
                    		}
                    	}

                    	//4尾
                    	if($value['actionData'] == "4尾"){
                    		if($ball7 == 4){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
                    		}
                    	}

                    	//5尾
                    	if($value['actionData'] == "5尾"){
                    		if($ball7 == 5){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
                    		}
                    	}

                    	//6尾
                    	if($value['actionData'] == "6尾"){
                    		if($ball7 == 6){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
                    		}
                    	}

                    	//7尾
                    	if($value['actionData'] == "7尾"){
                    		if($ball7 == 7){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
                    		}
                    	}

                    	//8尾
                    	if($value['actionData'] == "8尾"){
                    		if($ball7 == 8){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
                    		}
                    	}

                    	//9尾
                    	if($value['actionData'] == "9尾"){
                    		if($ball7 == 9){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
                    		}
                    	}
                    }
				}
                
                //波色
				if($value['playedId'] == 310){
					//红大、蓝大、绿大、红小、蓝小、绿小、红单、蓝单、绿单、红双、蓝双、绿双
					if($value['actionName'] == "SPHC"){
						//红大
						if($value['actionData'] == "红大"){
							if(($ball[6] == 01 || $ball[6] == 02 || $ball[6] == 07 || $ball[6] == 08 || $ball[6] == 12 || $ball[6] == 13 || $ball[6] == 18 || $ball[6] == 19 || $ball[6] == 23 || $ball[6] == 24 || $ball[6] == 29 || $ball[6] == 30 || $ball[6] == 34 || $ball[6] == 35 || $ball[6] == 40 || $ball[6] == 45 || $ball[6] == 46) && $ball[6] >= 25 && $ball[6] != 49){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
							}
						}

						//蓝大
						if($value['actionData'] == "蓝大"){
							if(($ball[6] == 03 || $ball[6] == 04 || $ball[6] == 09 || $ball[6] == 10 || $ball[6] == 14 || $ball[6] == 15 || $ball[6] == 20 || $ball[6] == 25 || $ball[6] == 26 || $ball[6] == 31 || $ball[6] == 36 || $ball[6] == 37 || $ball[6] == 41 || $ball[6] == 42 || $ball[6] == 47 || $ball[6] == 48) && $ball[6] >= 25 && $ball[6] != 49){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
							}
						}

						//绿大
						if($value['actionData'] == "绿大"){
							if(($ball[6] == 05 || $ball[6] == 06 || $ball[6] == 11 || $ball[6] == 16 || $ball[6] == 17 || $ball[6] == 21 || $ball[6] == 22 || $ball[6] == 27 || $ball[6] == 28 || $ball[6] == 32 || $ball[6] == 33 || $ball[6] == 38 || $ball[6] == 39 || $ball[6] == 43 || $ball[6] == 44 || $ball[6] == 49) && $ball[6] >= 25 && $ball[6] != 49){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
							}
						}

						//红小
						if($value['actionData'] == "红小"){
							if(($ball[6] == 01 || $ball[6] == 02 || $ball[6] == 07 || $ball[6] == 08 || $ball[6] == 12 || $ball[6] == 13 || $ball[6] == 18 || $ball[6] == 19 || $ball[6] == 23 || $ball[6] == 24 || $ball[6] == 29 || $ball[6] == 30 || $ball[6] == 34 || $ball[6] == 35 || $ball[6] == 40 || $ball[6] == 45 || $ball[6] == 46) && $ball[6] <= 24){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
							}
						}

						//蓝小
						if($value['actionData'] == "蓝小"){
							if(($ball[6] == 03 || $ball[6] == 04 || $ball[6] == 09 || $ball[6] == 10 || $ball[6] == 14 || $ball[6] == 15 || $ball[6] == 20 || $ball[6] == 25 || $ball[6] == 26 || $ball[6] == 31 || $ball[6] == 36 || $ball[6] == 37 || $ball[6] == 41 || $ball[6] == 42 || $ball[6] == 47 || $ball[6] == 48) && $ball[6] <= 24){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
							}
						}

						//绿小
						if($value['actionData'] == "绿小"){
							if(($ball[6] == 05 || $ball[6] == 06 || $ball[6] == 11 || $ball[6] == 16 || $ball[6] == 17 || $ball[6] == 21 || $ball[6] == 22 || $ball[6] == 27 || $ball[6] == 28 || $ball[6] == 32 || $ball[6] == 33 || $ball[6] == 38 || $ball[6] == 39 || $ball[6] == 43 || $ball[6] == 44 || $ball[6] == 49) && $ball[6] <= 24){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
							}
						}

						//红单
						if($value['actionData'] == "红单"){
							if(($ball[6] == 01 || $ball[6] == 02 || $ball[6] == 07 || $ball[6] == 08 || $ball[6] == 12 || $ball[6] == 13 || $ball[6] == 18 || $ball[6] == 19 || $ball[6] == 23 || $ball[6] == 24 || $ball[6] == 29 || $ball[6] == 30 || $ball[6] == 34 || $ball[6] == 35 || $ball[6] == 40 || $ball[6] == 45 || $ball[6] == 46) && $ball[6] % 2 != 0 && $ball[6] != 49){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
							}
						}

						//蓝单
						if($value['actionData'] == "蓝单"){
							if(($ball[6] == 03 || $ball[6] == 04 || $ball[6] == 09 || $ball[6] == 10 || $ball[6] == 14 || $ball[6] == 15 || $ball[6] == 20 || $ball[6] == 25 || $ball[6] == 26 || $ball[6] == 31 || $ball[6] == 36 || $ball[6] == 37 || $ball[6] == 41 || $ball[6] == 42 || $ball[6] == 47 || $ball[6] == 48) && $ball[6] % 2 != 0 && $ball[6] != 49){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
							}
						}

						//绿单
						if($value['actionData'] == "绿单"){
							if(($ball[6] == 05 || $ball[6] == 06 || $ball[6] == 11 || $ball[6] == 16 || $ball[6] == 17 || $ball[6] == 21 || $ball[6] == 22 || $ball[6] == 27 || $ball[6] == 28 || $ball[6] == 32 || $ball[6] == 33 || $ball[6] == 38 || $ball[6] == 39 || $ball[6] == 43 || $ball[6] == 44 || $ball[6] == 49) && $ball[6] % 2 != 0 && $ball[6] != 49){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
							}
						}

						//红双
						if($value['actionData'] == "红双"){
							if(($ball[6] == 01 || $ball[6] == 02 || $ball[6] == 07 || $ball[6] == 08 || $ball[6] == 12 || $ball[6] == 13 || $ball[6] == 18 || $ball[6] == 19 || $ball[6] == 23 || $ball[6] == 24 || $ball[6] == 29 || $ball[6] == 30 || $ball[6] == 34 || $ball[6] == 35 || $ball[6] == 40 || $ball[6] == 45 || $ball[6] == 46) && $ball[6] % 2 == 0){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
							}
						}

						//蓝双
						if($value['actionData'] == "蓝双"){
							if(($ball[6] == 03 || $ball[6] == 04 || $ball[6] == 09 || $ball[6] == 10 || $ball[6] == 14 || $ball[6] == 15 || $ball[6] == 20 || $ball[6] == 25 || $ball[6] == 26 || $ball[6] == 31 || $ball[6] == 36 || $ball[6] == 37 || $ball[6] == 41 || $ball[6] == 42 || $ball[6] == 47 || $ball[6] == 48) && $ball[6] % 2 == 0){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
							}
						}

						//绿双
						if($value['actionData'] == "绿双"){
							if(($ball[6] == 05 || $ball[6] == 06 || $ball[6] == 11 || $ball[6] == 16 || $ball[6] == 17 || $ball[6] == 21 || $ball[6] == 22 || $ball[6] == 27 || $ball[6] == 28 || $ball[6] == 32 || $ball[6] == 33 || $ball[6] == 38 || $ball[6] == 39 || $ball[6] == 43 || $ball[6] == 44 || $ball[6] == 49) && $ball[6] % 2 == 0){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
							}
						}
					}

					//红大单、蓝大单、绿大单、红小单、蓝小单、绿小单、红大双、蓝大双、绿大双、红小双、蓝小双、绿小双
					if($value['actionName'] == "SPHHC"){
						//红大单
                        if($value['actionData'] == "红大单"){
                        	if(($ball[6] == 01 || $ball[6] == 02 || $ball[6] == 07 || $ball[6] == 08 || $ball[6] == 12 || $ball[6] == 13 || $ball[6] == 18 || $ball[6] == 19 || $ball[6] == 23 || $ball[6] == 24 || $ball[6] == 29 || $ball[6] == 30 || $ball[6] == 34 || $ball[6] == 35 || $ball[6] == 40 || $ball[6] == 45 || $ball[6] == 46) && $ball[6] >= 25 && $ball[6] % 2 != 0 && $ball[6] != 49){
                        		if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
                        	}
                        }

                        //蓝大单
						if($value['actionData'] == "蓝大单"){
							if(($ball[6] == 03 || $ball[6] == 04 || $ball[6] == 09 || $ball[6] == 10 || $ball[6] == 14 || $ball[6] == 15 || $ball[6] == 20 || $ball[6] == 25 || $ball[6] == 26 || $ball[6] == 31 || $ball[6] == 36 || $ball[6] == 37 || $ball[6] == 41 || $ball[6] == 42 || $ball[6] == 47 || $ball[6] == 48) && $ball[6] >= 25 && $ball[6] % 2 != 0 && $ball[6] != 49){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
							}
						}

						//绿大单
						if($value['actionData'] == "绿大单"){
							if(($ball[6] == 05 || $ball[6] == 06 || $ball[6] == 11 || $ball[6] == 16 || $ball[6] == 17 || $ball[6] == 21 || $ball[6] == 22 || $ball[6] == 27 || $ball[6] == 28 || $ball[6] == 32 || $ball[6] == 33 || $ball[6] == 38 || $ball[6] == 39 || $ball[6] == 43 || $ball[6] == 44 || $ball[6] == 49) && $ball[6] >= 25 && $ball[6] % 2 != 0){
                                if($value['zjCount'] == 0 && $ball[6] != 49){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
							}
						}

						//红小单
                        if($value['actionData'] == "红小单"){
                        	if(($ball[6] == 01 || $ball[6] == 02 || $ball[6] == 07 || $ball[6] == 08 || $ball[6] == 12 || $ball[6] == 13 || $ball[6] == 18 || $ball[6] == 19 || $ball[6] == 23 || $ball[6] == 24 || $ball[6] == 29 || $ball[6] == 30 || $ball[6] == 34 || $ball[6] == 35 || $ball[6] == 40 || $ball[6] == 45 || $ball[6] == 46) && $ball[6] <= 24 && $ball[6] % 2 != 0 && $ball[6] != 49){
                        		if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
                        	}
                        }

                        //蓝小单
						if($value['actionData'] == "蓝小单"){
							if(($ball[6] == 03 || $ball[6] == 04 || $ball[6] == 09 || $ball[6] == 10 || $ball[6] == 14 || $ball[6] == 15 || $ball[6] == 20 || $ball[6] == 25 || $ball[6] == 26 || $ball[6] == 31 || $ball[6] == 36 || $ball[6] == 37 || $ball[6] == 41 || $ball[6] == 42 || $ball[6] == 47 || $ball[6] == 48) && $ball[6] <= 24 && $ball[6] % 2 != 0){
                                if($value['zjCount'] == 0 && $ball[6] != 49){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
							}
						}

						//绿小单
						if($value['actionData'] == "绿小单"){
							if(($ball[6] == 05 || $ball[6] == 06 || $ball[6] == 11 || $ball[6] == 16 || $ball[6] == 17 || $ball[6] == 21 || $ball[6] == 22 || $ball[6] == 27 || $ball[6] == 28 || $ball[6] == 32 || $ball[6] == 33 || $ball[6] == 38 || $ball[6] == 39 || $ball[6] == 43 || $ball[6] == 44 || $ball[6] == 49) && $ball[6] <= 24 && $ball[6] % 2 != 0){
                                if($value['zjCount'] == 0 && $ball[6] != 49){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
							}
						}

						//红大双
                        if($value['actionData'] == "红大双"){
                        	if(($ball[6] == 01 || $ball[6] == 02 || $ball[6] == 07 || $ball[6] == 08 || $ball[6] == 12 || $ball[6] == 13 || $ball[6] == 18 || $ball[6] == 19 || $ball[6] == 23 || $ball[6] == 24 || $ball[6] == 29 || $ball[6] == 30 || $ball[6] == 34 || $ball[6] == 35 || $ball[6] == 40 || $ball[6] == 45 || $ball[6] == 46) && $ball[6] >= 25 && $ball[6] % 2 == 0 && $ball[6] != 49){
                        		if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
                        	}
                        }

                        //蓝大双
						if($value['actionData'] == "蓝大双"){
							if(($ball[6] == 03 || $ball[6] == 04 || $ball[6] == 09 || $ball[6] == 10 || $ball[6] == 14 || $ball[6] == 15 || $ball[6] == 20 || $ball[6] == 25 || $ball[6] == 26 || $ball[6] == 31 || $ball[6] == 36 || $ball[6] == 37 || $ball[6] == 41 || $ball[6] == 42 || $ball[6] == 47 || $ball[6] == 48) && $ball[6] >= 25 && $ball[6] % 2 == 0 && $ball[6] != 49){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
							}
						}

						//绿大双
						if($value['actionData'] == "绿大双"){
							if(($ball[6] == 05 || $ball[6] == 06 || $ball[6] == 11 || $ball[6] == 16 || $ball[6] == 17 || $ball[6] == 21 || $ball[6] == 22 || $ball[6] == 27 || $ball[6] == 28 || $ball[6] == 32 || $ball[6] == 33 || $ball[6] == 38 || $ball[6] == 39 || $ball[6] == 43 || $ball[6] == 44 || $ball[6] == 49) && $ball[6] >= 25 && $ball[6] % 2 == 0 && $ball[6] != 49){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
							}
						}

						//红小双
                        if($value['actionData'] == "红小双"){
                        	if(($ball[6] == 01 || $ball[6] == 02 || $ball[6] == 07 || $ball[6] == 08 || $ball[6] == 12 || $ball[6] == 13 || $ball[6] == 18 || $ball[6] == 19 || $ball[6] == 23 || $ball[6] == 24 || $ball[6] == 29 || $ball[6] == 30 || $ball[6] == 34 || $ball[6] == 35 || $ball[6] == 40 || $ball[6] == 45 || $ball[6] == 46) && $ball[6] <= 24 && $ball[6] % 2 == 0){
                        		if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
                        	}
                        }

                        //蓝小双
						if($value['actionData'] == "蓝小双"){
							if(($ball[6] == 03 || $ball[6] == 04 || $ball[6] == 09 || $ball[6] == 10 || $ball[6] == 14 || $ball[6] == 15 || $ball[6] == 20 || $ball[6] == 25 || $ball[6] == 26 || $ball[6] == 31 || $ball[6] == 36 || $ball[6] == 37 || $ball[6] == 41 || $ball[6] == 42 || $ball[6] == 47 || $ball[6] == 48) && $ball[6] <= 24 && $ball[6] % 2 == 0){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
							}
						}

						//绿小双
						if($value['actionData'] == "绿小双"){
							if(($ball[6] == 05 || $ball[6] == 06 || $ball[6] == 11 || $ball[6] == 16 || $ball[6] == 17 || $ball[6] == 21 || $ball[6] == 22 || $ball[6] == 27 || $ball[6] == 28 || $ball[6] == 32 || $ball[6] == 33 || $ball[6] == 38 || $ball[6] == 39 || $ball[6] == 43 || $ball[6] == 44 || $ball[6] == 49) && $ball[6] <= 24 && $ball[6] % 2 == 0){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
							}
						}
					}
				}
                
                //正码平码
				if($value['playedId'] == 311){
					//号码下注
					if($value['actionName'] == "LOTTO"){
						if($ba['ball1'] == $value['actionData'] || $ba['ball2'] == $value['actionData'] || $ba['ball3'] == $value['actionData'] || $ba['ball4'] == $value['actionData'] || $ba['ball5'] == $value['actionData'] || $ba['ball6'] == $value['actionData']){
							if($value['zjCount'] == 0){
								$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
							}
						}
					}

					//总大、总小、总单、总双
					if($value['actionName'] == "LTTBSOE"){
						//总大
						if($value['actionData'] == "总大"){
							if($ballall >= 175){
								if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
							}
						}

						//总小
						if($value['actionData'] == "总小"){
							if($ballall <= 174){
								if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
							}
					    }

						//总单
						if($value['actionData'] == "总单"){
							if($ballall % 2 != 0){
								if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
							}
					    }

					    //总双
						if($value['actionData'] == "总双"){
							if($ballall % 2 == 0){
								if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
							}
					    }	
					}
				}

				//平特肖尾
				if($value['playedId'] == 312){
					//鼠、牛、虎、兔、龙、蛇、马、羊、猴、鸡、狗、猪
					if($value['actionName'] == "LTTBP"){
                        //牛
					    if($value['actionData'] == "牛"){
	                        if($ball[1] == 10 || $ball[1] == 22 || $ball[1] == 34 || $ball[1] == 46 || $ball[2] == 10 || $ball[2] == 22 || $ball[2] == 34 || $ball[2] == 46 || $ball[3] == 10 || $ball[3] == 22 || $ball[3] == 34 || $ball[3] == 46 || $ball[4] == 10 || $ball[4] == 22 || $ball[4] == 34 || $ball[4] == 46 || $ball[5] == 10 || $ball[5] == 22 || $ball[5] == 34 || $ball[5] == 46 || $ball[6] == 10 || $ball[6] == 22 || $ball[6] == 34 || $ball[6] == 46){
	                        	if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
	                        } 
                        }

                        //虎
                        if($value['actionData'] == "虎"){
	                        if($ball[1] == 09 || $ball[1] == 21 || $ball[1] == 33 || $ball[1] == 45 || $ball[2] == 09 || $ball[2] == 21 || $ball[2] == 33 || $ball[2] == 45 || $ball[3] == 09 || $ball[3] == 21 || $ball[3] == 33 || $ball[3] == 45 || $ball[4] == 09 || $ball[4] == 21 || $ball[4] == 33 || $ball[4] == 45 || $ball[5] == 09 || $ball[5] == 21 || $ball[5] == 33 || $ball[5] == 45 || $ball[6] == 09 || $ball[6] == 21 || $ball[6] == 33 || $ball[6] == 45){
	                        	if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
	                        } 
                        }

                        //兔
                        if($value['actionData'] == "兔"){
	                        if($ball[1] == 08 || $ball[1] == 20 || $ball[1] == 32 || $ball[1] == 44 || $ball[2] == 08 || $ball[2] == 20 || $ball[2] == 32 || $ball[2] == 44 || $ball[3] == 08 || $ball[3] == 20 || $ball[3] == 32 || $ball[3] == 44 || $ball[4] == 08 || $ball[4] == 20 || $ball[4] == 32 || $ball[4] == 44 || $ball[5] == 08 || $ball[5] == 20 || $ball[5] == 32 || $ball[5] == 44 || $ball[6] == 08 || $ball[6] == 20 || $ball[6] == 32 || $ball[6] == 44){
	                        	if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
	                        } 
                        }

                        //龙
                        if($value['actionData'] == "龙"){
	                        if($ball[1] == 07 || $ball[1] == 19 || $ball[1] == 31 || $ball[1] == 43 || $ball[2] == 07 || $ball[2] == 19 || $ball[2] == 31 || $ball[2] == 43 || $ball[3] == 07 || $ball[3] == 19 || $ball[3] == 31 || $ball[3] == 43 || $ball[4] == 07 || $ball[4] == 19 || $ball[4] == 31 || $ball[4] == 43 || $ball[5] == 07 || $ball[5] == 19 || $ball[5] == 31 || $ball[5] == 43 || $ball[6] == 07 || $ball[6] == 19 || $ball[6] == 31 || $ball[6] == 43){
	                        	if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
	                        } 
                        }

                        //蛇
                        if($value['actionData'] == "蛇"){
	                        if($ball[1] == 06 || $ball[1] == 18 || $ball[1] == 30 || $ball[1] == 42 || $ball[2] == 06 || $ball[2] == 18 || $ball[2] == 30 || $ball[2] == 42 || $ball[3] == 06 || $ball[3] == 18 || $ball[3] == 30 || $ball[3] == 42 || $ball[4] == 06 || $ball[4] == 18 || $ball[4] == 30 || $ball[4] == 42 || $ball[5] == 06 || $ball[5] == 18 || $ball[5] == 30 || $ball[5] == 42 || $ball[6] == 06 || $ball[6] == 18 || $ball[6] == 30 || $ball[6] == 42){
	                        	if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
	                        } 
                        }

                        //马
                        if($value['actionData'] == "马"){
	                        if($ball[1] == 05 || $ball[1] == 17 || $ball[1] == 29 || $ball[1] == 41 || $ball[2] == 05 || $ball[2] == 17 || $ball[2] == 29 || $ball[2] == 41 || $ball[3] == 05 || $ball[3] == 17 || $ball[3] == 29 || $ball[3] == 41 || $ball[4] == 05 || $ball[4] == 17 || $ball[4] == 29 || $ball[4] == 41 || $ball[5] == 05 || $ball[5] == 17 || $ball[5] == 29 || $ball[5] == 41 || $ball[6] == 05 || $ball[6] == 17 || $ball[6] == 29 || $ball[6] == 41){
	                        	if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
	                        } 
                        }


                        //羊
                        if($value['actionData'] == "羊"){
	                        if($ball[1] == 04 || $ball[1] == 16 || $ball[1] == 28 || $ball[1] == 40 || $ball[2] == 04 || $ball[2] == 16 || $ball[2] == 28 || $ball[2] == 40 || $ball[3] == 04 || $ball[3] == 16 || $ball[3] == 28 || $ball[3] == 40 || $ball[4] == 04 || $ball[4] == 16 || $ball[4] == 28 || $ball[4] == 40 || $ball[5] == 04 || $ball[5] == 16 || $ball[5] == 28 || $ball[5] == 40 || $ball[6] == 04 || $ball[6] == 16 || $ball[6] == 28 || $ball[6] == 40){
	                        	if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
	                        } 
                        }

                        //猴
                        if($value['actionData'] == "猴"){
	                        if($ball[1] == 03 || $ball[1] == 15 || $ball[1] == 27 || $ball[1] == 39 || $ball[2] == 03 || $ball[2] == 15 || $ball[2] == 27 || $ball[2] == 39 || $ball[3] == 03 || $ball[3] == 15 || $ball[3] == 27 || $ball[3] == 39 || $ball[4] == 03 || $ball[4] == 15 || $ball[4] == 27 || $ball[4] == 39 || $ball[5] == 03 || $ball[5] == 15 || $ball[5] == 27 || $ball[5] == 39 || $ball[6] == 03 || $ball[6] == 15 || $ball[6] == 27 || $ball[6] == 39){
	                        	if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
	                        } 
                        }

                        //鸡
                        if($value['actionData'] == "鸡"){
	                        if($ball[1] == 02 || $ball[1] == 14 || $ball[1] == 26 || $ball[1] == 38 || $ball[2] == 02 || $ball[2] == 14 || $ball[2] == 26 || $ball[2] == 38 || $ball[3] == 02 || $ball[3] == 14 || $ball[3] == 26 || $ball[3] == 38 || $ball[4] == 02 || $ball[4] == 14 || $ball[4] == 26 || $ball[4] == 38 || $ball[5] == 02 || $ball[5] == 14 || $ball[5] == 26 || $ball[5] == 38 || $ball[6] == 02 || $ball[6] == 14 || $ball[6] == 26 || $ball[6] == 38){
	                        	if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
	                        } 
                        }

                        //狗
                        if($value['actionData'] == "狗"){
	                        if($ball[1] == 01 || $ball[1] == 13 || $ball[1] == 25 || $ball[1] == 37 || $ball[1] == 49 || $ball[2] == 01 || $ball[2] == 13 || $ball[2] == 25 || $ball[2] == 37 || $ball[2] == 49 || $ball[3] == 01 || $ball[3] == 13 || $ball[3] == 25 || $ball[3] == 37 || $ball[3] == 49 || $ball[4] == 01 || $ball[4] == 13 || $ball[4] == 25 || $ball[4] == 37 || $ball[4] == 49 || $ball[5] == 01 || $ball[5] == 13 || $ball[5] == 25 || $ball[5] == 37 || $ball[5] == 49 || $ball[6] == 01 || $ball[6] == 13 || $ball[6] == 25 || $ball[6] == 37 || $ball[6] == 49){
	                        	if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
	                        } 
                        }

                        //猪
                        if($value['actionData'] == "猪"){
	                        if($ball[1] == 12 || $ball[1] == 24 || $ball[1] == 36 || $ball[1] == 48 || $ball[2] == 12 || $ball[2] == 24 || $ball[2] == 36 || $ball[2] == 48 || $ball[3] == 12 || $ball[3] == 24 || $ball[3] == 36 || $ball[3] == 48 || $ball[4] == 12 || $ball[4] == 24 || $ball[4] == 36 || $ball[4] == 48 || $ball[5] == 12 || $ball[5] == 24 || $ball[5] == 36 || $ball[5] == 48 || $ball[6] == 12 || $ball[6] == 24 || $ball[6] == 36 || $ball[6] == 48){
	                        	if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
	                        } 
                        }

                          //鼠
                        if($value['actionData'] == "鼠"){
	                        if($ball[1] == 11 || $ball[1] == 23 || $ball[1] == 35 || $ball[1] == 47 || $ball[2] == 11 || $ball[2] == 23 || $ball[2] == 35 || $ball[2] == 47 || $ball[3] == 11 || $ball[3] == 23 || $ball[3] == 35 || $ball[3] == 47 || $ball[4] == 11 || $ball[4] == 23 || $ball[4] == 35 || $ball[4] == 47 || $ball[5] == 11 || $ball[5] == 23 || $ball[5] == 35 || $ball[5] == 47 || $ball[6] == 11 || $ball[6] == 23 || $ball[6] == 35 || $ball[6] == 47){
	                        	if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
	                        }
	                    }    
                    }

                    if($value['actionName'] == "LTTSD"){
                    	//0尾、1尾、2尾、3尾、4尾、5尾、6尾、7尾、8尾、9尾、
                    	//0尾
                    	if($value['actionData'] == "0尾"){
                    		if($ball1 == 0 || $ball2 == 0 || $ball3 == 0 || $ball4 == 0 || $ball5 == 0 || $ball6 == 0 || $ball7 == 0){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
                    		}
                    	}

                    	//1尾
                    	if($value['actionData'] == "1尾"){
                    		if($ball1 == 1 || $ball2 == 1 || $ball3 == 1 || $ball4 == 1 || $ball5 == 1 || $ball6 == 1 || $ball7 == 1){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
                    		}
                    	}

                    	//2尾
                    	if($value['actionData'] == "2尾"){
                    		if($ball1 == 2 || $ball2 == 2 || $ball3 == 2 || $ball4 == 2 || $ball5 == 2 || $ball6 == 2 || $ball7 == 2){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
                    		}
                    	}

                    	//3尾
                    	if($value['actionData'] == "3尾"){
                    		if($ball1 == 3 || $ball2 == 3 || $ball3 == 3 || $ball4 == 3 || $ball5 == 3 || $ball6 == 3 || $ball7 == 3){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
                    		}
                    	}

                    	//4尾
                    	if($value['actionData'] == "4尾"){
                    		if($ball1 == 4 || $ball2 == 4 || $ball3 == 4 || $ball4 == 4 || $ball5 == 4 || $ball6 == 4 || $ball7 == 4){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
                    		}
                    	}

                    	//5尾
                    	if($value['actionData'] == "5尾"){
                    		if($ball1 == 5 || $ball2 == 5 || $ball4 == 5 || $ball4 == 5 || $ball5 == 5 || $ball6 == 5 || $ball7 == 5){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
                    		}
                    	}

                    	//6尾
                    	if($value['actionData'] == "6尾"){
                    		if($ball1 == 6 || $ball2 == 6 || $ball4 == 6 || $ball4 == 6 || $ball5 == 6 || $ball6 == 6 || $ball7 == 6){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
                    		}
                    	}

                    	//7尾
                    	if($value['actionData'] == "7尾"){
                    		if($ball1 == 7 || $ball2 == 7 || $ball4 == 7 || $ball4 == 7 || $ball5 == 7 || $ball6 == 7 || $ball7 == 7){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
                    		}
                    	}

                    	//8尾
                    	if($value['actionData'] == "8尾"){
                    		if($ball1 == 8 || $ball2 == 8 || $ball4 == 8 || $ball4 == 8 || $ball5 == 8 || $ball6 == 8 || $ball7 == 8){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
                    		}
                    	}

                    	//9尾
                    	if($value['actionData'] == "9尾"){
                    		if($ball1 == 9 || $ball2 == 9 || $ball4 == 9 || $ball4 == 9 || $ball5 == 9 || $ball6 == 9 || $ball7 == 9){
                                if($value['zjCount'] == 0){
									$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
								}
                    		}
                    	}
                    }	
				}

				//连肖
				if($value['playedId'] == 313){
                    $sheng = explode(" " , $value['actionData']);
                    if($sheng){
	                    foreach ($sheng as $ks => $vs) {
						    //牛
	                    	if($vs == "牛"){
	                    		if($ball[1] == 10 || $ball[1] == 22 || $ball[1] == 34 || $ball[1] == 46 || $ball[2] == 10 || $ball[2] == 22 || $ball[2] == 34 || $ball[2] == 46 || $ball[3] == 10 || $ball[3] == 22 || $ball[3] == 34 || $ball[3] == 46 || $ball[4] == 10 || $ball[4] == 22 || $ball[4] == 34 || $ball[4] == 46 || $ball[5] == 10 || $ball[5] == 22 || $ball[5] == 34 || $ball[5] == 46 || $ball[6] == 10 || $ball[6] == 22 || $ball[6] == 34 || $ball[6] == 46){
	                    		 	$arr1 = 1;
		                        }else{
		                        	$arr1 = 0;
		                        }
	                    	}

	                    	//虎
	                    	if($vs == "虎"){
	                    		if($ball[1] == 09 || $ball[1] == 21 || $ball[1] == 33 || $ball[1] == 45 || $ball[2] == 09 || $ball[2] == 21 || $ball[2] == 33 || $ball[2] == 45 || $ball[3] == 09 || $ball[3] == 21 || $ball[3] == 33 || $ball[3] == 45 || $ball[4] == 09 || $ball[4] == 21 || $ball[4] == 33 || $ball[4] == 45 || $ball[5] == 09 || $ball[5] == 21 || $ball[5] == 33 || $ball[5] == 45 || $ball[6] == 09 || $ball[6] == 21 || $ball[6] == 33 || $ball[6] == 45){
	                    		 	$arr2 = 1;
		                        }else{
		                        	$arr2 = 0;
		                        }
	                    	}


	                    	//兔
	                    	if($vs == "兔"){
	                    		if($ball[1] == 08 || $ball[1] == 20 || $ball[1] == 32 || $ball[1] == 44 || $ball[2] == 08 || $ball[2] == 20 || $ball[2] == 32 || $ball[2] == 44 || $ball[3] == 08 || $ball[3] == 20 || $ball[3] == 32 || $ball[3] == 44 || $ball[4] == 08 || $ball[4] == 20 || $ball[4] == 32 || $ball[4] == 44 || $ball[5] == 08 || $ball[5] == 20 || $ball[5] == 32 || $ball[5] == 44 || $ball[6] == 08 || $ball[6] == 20 || $ball[6] == 32 || $ball[6] == 44){
	                    		 	$arr3 = 1;
		                        }else{
		                        	$arr3 = 0;
		                        }
	                    	}

	                    	//龙
	                    	if($vs == "龙"){
	                    		if($ball[1] == 07 || $ball[1] == 19 || $ball[1] == 31 || $ball[1] == 43 || $ball[2] == 07 || $ball[2] == 19 || $ball[2] == 31 || $ball[2] == 43 || $ball[3] == 07 || $ball[3] == 19 || $ball[3] == 31 || $ball[3] == 43 || $ball[4] == 07 || $ball[4] == 19 || $ball[4] == 31 || $ball[4] == 43 || $ball[5] == 07 || $ball[5] == 19 || $ball[5] == 31 || $ball[5] == 43 || $ball[6] == 07 || $ball[6] == 19 || $ball[6] == 31 || $ball[6] == 43){
	                    		 	$arr4 = 1;
		                        }else{
		                        	$arr4 = 0;
		                        }
	                    	}

	                    	//蛇
	                    	if($vs == "蛇"){
	                    		if($ball[1] == 06 || $ball[1] == 18 || $ball[1] == 30 || $ball[1] == 42 || $ball[2] == 06 || $ball[2] == 18 || $ball[2] == 30 || $ball[2] == 42 || $ball[3] == 06 || $ball[3] == 18 || $ball[3] == 30 || $ball[3] == 42 || $ball[4] == 06 || $ball[4] == 18 || $ball[4] == 30 || $ball[4] == 42 || $ball[5] == 06 || $ball[5] == 18 || $ball[5] == 30 || $ball[5] == 42 || $ball[6] == 06 || $ball[6] == 18 || $ball[6] == 30 || $ball[6] == 42){
	                    		 	$arr5 = 1;
		                        }else{
		                        	$arr5 = 0;
		                        }
	                    	}

	                    	//马
	                    	if($vs == "马"){
	                    		if($ball[1] == 05 || $ball[1] == 17 || $ball[1] == 29 || $ball[1] == 41 || $ball[2] == 05 || $ball[2] == 17 || $ball[2] == 29 || $ball[2] == 41 || $ball[3] == 05 || $ball[3] == 17 || $ball[3] == 29 || $ball[3] == 41 || $ball[4] == 05 || $ball[4] == 17 || $ball[4] == 29 || $ball[4] == 41 || $ball[5] == 05 || $ball[5] == 17 || $ball[5] == 29 || $ball[5] == 41 || $ball[6] == 05 || $ball[6] == 17 || $ball[6] == 29 || $ball[6] == 41){
	                    		 	$arr6 = 1;
		                        }else{
		                        	$arr6 = 0;
		                        }
	                    	}

	                    	//羊
	                    	if($vs == "羊"){
	                    		if($ball[1] == 04 || $ball[1] == 16 || $ball[1] == 28 || $ball[1] == 40 || $ball[2] == 04 || $ball[2] == 16 || $ball[2] == 28 || $ball[2] == 40 || $ball[3] == 04 || $ball[3] == 16 || $ball[3] == 28 || $ball[3] == 40 || $ball[4] == 04 || $ball[4] == 16 || $ball[4] == 28 || $ball[4] == 40 || $ball[5] == 04 || $ball[5] == 16 || $ball[5] == 28 || $ball[5] == 40 || $ball[6] == 04 || $ball[6] == 16 || $ball[6] == 28 || $ball[6] == 40){
	                    		 	$arr7 = 1;
		                        }else{
		                        	$arr7 = 0;
		                        }
	                    	}

	                    	//猴
	                    	if($vs == "猴"){
	                    		if($ball[1] == 03 || $ball[1] == 15 || $ball[1] == 27 || $ball[1] == 39 || $ball[2] == 03 || $ball[2] == 15 || $ball[2] == 27 || $ball[2] == 39 || $ball[3] == 03 || $ball[3] == 15 || $ball[3] == 27 || $ball[3] == 39 || $ball[4] == 03 || $ball[4] == 15 || $ball[4] == 27 || $ball[4] == 39 || $ball[5] == 03 || $ball[5] == 15 || $ball[5] == 27 || $ball[5] == 39 || $ball[6] == 03 || $ball[6] == 15 || $ball[6] == 27 || $ball[6] == 39){
	                    		 	$arr8 = 1;
		                        }else{
		                        	$arr8 = 0;
		                        }
	                    	}

	                    	//鸡
	                    	if($vs == "鸡"){
	                    		if($ball[1] == 02 || $ball[1] == 14 || $ball[1] == 26 || $ball[1] == 38 || $ball[2] == 02 || $ball[2] == 14 || $ball[2] == 26 || $ball[2] == 38 || $ball[3] == 02 || $ball[3] == 14 || $ball[3] == 26 || $ball[3] == 38 || $ball[4] == 02 || $ball[4] == 14 || $ball[4] == 26 || $ball[4] == 38 || $ball[5] == 02 || $ball[5] == 14 || $ball[5] == 26 || $ball[5] == 38 || $ball[6] == 02 || $ball[6] == 14 || $ball[6] == 26 || $ball[6] == 38){
	                    		 	$arr9 = 1;
		                        }else{
		                        	$arr9 = 0;
		                        }
	                    	}

	                    	//狗
	                    	if($vs == "狗"){
	                    		if($ball[1] == 01 || $ball[1] == 13 || $ball[1] == 25 || $ball[1] == 37 || $ball[1] == 49 || $ball[2] == 01 || $ball[2] == 13 || $ball[2] == 25 || $ball[2] == 37 || $ball[2] == 49 || $ball[3] == 01 || $ball[3] == 13 || $ball[3] == 25 || $ball[3] == 37 || $ball[3] == 49 || $ball[4] == 01 || $ball[4] == 13 || $ball[4] == 25 || $ball[4] == 37 || $ball[4] == 49 || $ball[5] == 01 || $ball[5] == 13 || $ball[5] == 25 || $ball[5] == 37 || $ball[5] == 49 || $ball[6] == 01 || $ball[6] == 13 || $ball[6] == 25 || $ball[6] == 37 || $ball[6] == 49){
	                    		 	$arr10 = 1;
		                        }else{
		                        	$arr10 = 0;
		                        }
	                    	}

	                    	//猪
	                    	if($vs == "猪"){
	                    		if($ball[1] == 12 || $ball[1] == 24 || $ball[1] == 36 || $ball[1] == 48 || $ball[2] == 12 || $ball[2] == 24 || $ball[2] == 36 || $ball[2] == 48 || $ball[3] == 12 || $ball[3] == 24 || $ball[3] == 36 || $ball[3] == 48 || $ball[4] == 12 || $ball[4] == 24 || $ball[4] == 36 || $ball[4] == 48 || $ball[5] == 12 || $ball[5] == 24 || $ball[5] == 36 || $ball[5] == 48 || $ball[6] == 12 || $ball[6] == 24 || $ball[6] == 36 || $ball[6] == 48){
	                    		 	$arr11 = 1;
		                        }else{
		                        	$arr11 = 0;
		                        }
	                    	}

	                    	//鼠
	                    	if($vs == "鼠"){
	                    		if($ball[1] == 11 || $ball[1] == 23 || $ball[1] == 35 || $ball[1] == 47 || $ball[2] == 11 || $ball[2] == 23 || $ball[2] == 35 || $ball[2] == 47 || $ball[3] == 11 || $ball[3] == 23 || $ball[3] == 35 || $ball[3] == 47 || $ball[4] == 11 || $ball[4] == 23 || $ball[4] == 35 || $ball[4] == 47 || $ball[5] == 11 || $ball[5] == 23 || $ball[5] == 35 || $ball[5] == 47 || $ball[6] == 11 || $ball[6] == 23 || $ball[6] == 35 || $ball[6] == 47){
	                    		 	$arr12 = 1;
		                        }else{
		                        	$arr12 = 0;
		                        }
	                    	}
	                    }
                    }

                    //牛	
		            $str = $value['actionData'];				
		            $need1 ='牛';
			        $array1 = explode($need1,$str);
			        if(count($array1)<=1){
			        	$arr1 = 0;
			        }

			        //虎	
		            $need2 ='虎';
			        $array2 = explode($need2,$str);
			        if(count($array2)<=1){
			        	$arr2 = 0;
			        }

			        //兔	
		            $need3 ='兔';
			        $array3 = explode($need3,$str);
			        if(count($array3)<=1){
			        	$arr3 = 0;
			        }

			        //龙	
		            $need4 ='龙';
			        $array4 = explode($need4,$str);
			        if(count($array4)<=1){
			        	$arr4 = 0;
			        }

			        //蛇	
		            $need5 ='蛇';
			        $array5 = explode($need5,$str);
			        if(count($array5)<=1){
			        	$arr5 = 0;
			        }

			        //马	
		            $need6 ='马';
			        $array6 = explode($need6,$str);
			        if(count($array6)<=1){
			        	$arr6 = 0;
			        }

			        //羊	
		            $need7 ='羊';
			        $array7 = explode($need7,$str);
			        if(count($array7)<=1){
			        	$arr7 = 0;
			        }

			        //猴	
		            $need8 ='猴';
			        $array8 = explode($need8,$str);
			        if(count($array8)<=1){
			        	$arr8 = 0;
			        }

			        //鸡	
		            $need9 ='鸡';
			        $array9 = explode($need9,$str);
			        if(count($array9)<=1){
			        	$arr9 = 0;
			        }

			        //狗	
		            $need10 ='狗';
			        $array10 = explode($need10,$str);
			        if(count($array10)<=1){
			        	$arr10 = 0;
			        }

			        //猪	
		            $need11 ='猪';
			        $array11 = explode($need11,$str);
			        if(count($array11)<=1){
			        	$arr11 = 0;
			        }

			        //鼠	
		            $need12 ='鼠';
			        $array12 = explode($need12,$str);
			        if(count($array12)<=1){
			        	$arr12 = 0;
			        }
			        $arrs = $arr1+$arr2+$arr3+$arr4+$arr5+$arr6+$arr7+$arr8+$arr9+$arr10+$arr11+$arr12;
				    //中奖金额
				    $count = count(explode(" ", $value['actionData']));
					//二肖碰
					if($value['actionName'] == "SNBP2"){		        
				        if($arrs >= 2){
				            $count = ($count)*(($count-1)/2);
			                $msg['bonus'] = ($value['actionAmount']/$count)*$value['bonusProp']*($arrs)*(($arrs-1)/2);
				        	if($value['zjCount'] == 0){
								$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
							}
				        }
					}

					//三肖碰
					if($value['actionName'] == "SNBP3"){		        
				        if($arrs >= 3){
				        	$count = ($count*($count-1)*($count-2)/6);
				        	$arrays = ($arrs*($arrs-1)*($arrs-2)/6);
			                $msg['bonus'] = ($value['actionAmount']/$count)*$value['bonusProp']*$arrays;
				        	if($value['zjCount'] == 0){
								$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
							}
				        }
					} 

					//四肖碰
					if($value['actionName'] == "SNBP4"){		        
				        if($arrs >= 4){
				        	$count = ($count*($count-1)*($count-2)*($count-3)/24);
				        	$arrays = ($arrs*($arrs-1)*($arrs-2)*($arrs-3)/24);
				        	$msg['bonus'] = ($value['actionAmount']/$count)*$value['bonusProp']*$arrays;
				        	if($value['zjCount'] == 0){
								$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
							}
				        }
					}

					//五肖碰
					if($value['actionName'] == "SNBP5"){		        
				        if($arrs >= 5){
				        	$count = ($count*($count-1)*($count-2)*($count-3)*($count-4)/120);
				        	$arrays = ($arrs*($arrs-1)*($arrs-2)*($arrs-3)*($arrs-4)/120);
				        	$msg['bonus'] = ($value['actionAmount']/$count)*$value['bonusProp']*$arrays;
				        	if($value['zjCount'] == 0){
								$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
							}
				        }
					}    
				}
                 
				//连尾
				if($value['playedId'] == 314){
					$wei = explode(" " , $value['actionData']);
					if($wei){	
					    //0尾、1尾、2尾、3尾、4尾、5尾、6尾、7尾、8尾、9尾、		
						foreach ($wei as $ks => $vs) {
                            //0尾
							if($vs == "0尾"){
								if($ball1 == 0 || $ball2 == 0 || $ball3 == 0 || $ball4 == 0 || $ball5 == 0 || $ball6 == 0 || $ball7 == 0){
									$arr1 = 1;
								}else{
									$arr1 = 0;
								}
							}

							//1尾
							if($vs == "1尾"){
								if($ball1 == 1 || $ball2 == 1 || $ball3 == 1 || $ball4 == 1 || $ball5 == 1 || $ball6 == 1 || $ball7 == 1){
									$arr2 = 1;
								}else{
									$arr2 = 0;
								}
							}

							//2尾
							if($vs == "2尾"){
								if($ball1 == 2 || $ball2 == 2 || $ball3 == 2 || $ball4 == 2 || $ball5 == 2 || $ball6 == 2 || $ball7 == 2){
									$arr3 = 1;
								}else{
									$arr3 = 0;
								}
							}

							//3尾
							if($vs == "3尾"){
								if($ball1 == 3 || $ball2 == 3 || $ball3 == 3 || $ball4 == 3 || $ball5 == 3 || $ball6 == 3 || $ball7 == 3){
									$arr4 = 1;
								}else{
									$arr4 = 0;
								}
							}

							//4尾
							if($vs == "4尾"){
								if($ball1 == 4 || $ball2 == 4 || $ball3 == 4 || $ball4 == 4 || $ball5 == 4 || $ball6 == 4 || $ball7 == 4){
									$arr5 = 1;
								}else{
									$arr5 = 0;
								}
							}

							//5尾
							if($vs == "5尾"){
								if($ball1 == 5 || $ball2 == 5 || $ball4 == 5 || $ball4 == 5 || $ball5 == 5 || $ball6 == 5 || $ball7 == 5){
									$arr6 = 1;
								}else{
									$arr6 = 0;
								}
							}

							//6尾
							if($vs == "6尾"){
								if($ball1 == 6 || $ball2 == 6 || $ball4 == 6 || $ball4 == 6 || $ball5 == 6 || $ball6 == 6 || $ball7 == 6){
									$arr7 = 1;
								}else{
									$arr7 = 0;
								}
							}

							//7尾
							if($vs == "7尾"){
								if($ball1 == 7 || $ball2 == 7 || $ball4 == 7 || $ball4 == 7 || $ball5 == 7 || $ball6 == 7 || $ball7 == 7){
									$arr8 = 1;
								}else{
									$arr8 = 0;
								}
							}

							//8尾
							if($vs == "8尾"){
								if($ball1 == 8 || $ball2 == 8 || $ball4 == 8 || $ball4 == 8 || $ball5 == 8 || $ball6 == 8 || $ball7 == 8){
									$arr9 = 1;
								}else{
									$arr9 = 0;
								}
							}

							//9尾
							if($vs == "9尾"){
								if($ball1 == 9 || $ball2 == 9 || $ball4 == 9 || $ball4 == 9 || $ball5 == 9 || $ball6 == 9 || $ball7 == 9){
									$arr10 = 1;
								}else{
									$arr10 = 0;
								}
							}	
						}
					}

					//0尾	
		            $str = $value['actionData'];				
		            $need1 ='0尾';
			        $array1 = explode($need1,$str);
			        if(count($array1)<=1){
			        	$arr1 = 0;
			        }

			        //1尾					
		            $need2 ='1尾';
			        $array2 = explode($need2,$str);
			        if(count($array2)<=1){
			        	$arr2 = 0;
			        }

			        //2尾					
		            $need3 ='2尾';
			        $array3 = explode($need3,$str);
			        if(count($array3)<=1){
			        	$arr3 = 0;
			        }

			        //3尾	
		            $need4 ='3尾';
			        $array4 = explode($need4,$str);
			        if(count($array4)<=1){
			        	$arr4 = 0;
			        }

			        //4尾	
		            $need5 ='4尾';
			        $array5 = explode($need5,$str);
			        if(count($array5)<=1){
			        	$arr5 = 0;
			        }

			        //5尾	
		            $need6 ='5尾';
			        $array6 = explode($need6,$str);
			        if(count($array6)<=1){
			        	$arr6 = 0;
			        }

			        //6尾	
		            $need7 ='6尾';
			        $array7 = explode($need7,$str);
			        if(count($array7)<=1){
			        	$arr7 = 0;
			        }

			        //7尾	
		            $need8 ='7尾';
			        $array8 = explode($need8,$str);
			        if(count($array8)<=1){
			        	$arr8 = 0;
			        }

			        //8尾	
		            $need9 ='8尾';
			        $array9 = explode($need9,$str);
			        if(count($array9)<=1){
			        	$arr9 = 0;
			        }

			        //9尾	
		            $need10 ='9尾';
			        $array10 = explode($need10,$str);
			        if(count($array10)<=1){
			        	$arr10 = 0;
			        }
                    
                    $arrs = $arr1+$arr2+$arr3+$arr4+$arr5+$arr6+$arr7+$arr8+$arr9+$arr10+$arr11+$arr12;
				    //中奖金额
				    $count = count(explode(" ", $value['actionData']));

				    //二尾碰
			        if($value['actionName'] == "SNSD2"){
			        	if($arrs >= 2){
			        		$count = ($count)*(($count-1)/2);
			                $msg['bonus'] = ($value['actionAmount']/$count)*$value['bonusProp']*($arrs)*(($arrs-1)/2);
			                if($value['zjCount'] == 0){
								$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
							}
			        	}
			        }

			        //三尾碰
			        if($value['actionName'] == "SNSD3"){
			        	if($arrs >= 3){
				        	$count = ($count*($count-1)*($count-2)/6);
				        	$arrays = ($arrs*($arrs-1)*($arrs-2)/6);
			                $msg['bonus'] = ($value['actionAmount']/$count)*$value['bonusProp']*$arrays;
				        	if($value['zjCount'] == 0){
								$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
							}
				        }
			        }

			        //四尾碰
			        if($value['actionName'] == "SNSD4"){
			        	if($arrs >= 4){
				        	$count = ($count*($count-1)*($count-2)*($count-3)/24);
				        	$arrays = ($arrs*($arrs-1)*($arrs-2)*($arrs-3)/24);
				        	$msg['bonus'] = ($value['actionAmount']/$count)*$value['bonusProp']*$arrays;
				        	if($value['zjCount'] == 0){
								$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
							}
				        }
			        }

			        //五尾碰
			        if($value['actionName'] == "SNSD5"){
			        	if($arrs >= 5){
				        	$count = ($count*($count-1)*($count-2)*($count-3)*($count-4)/120);
				        	$arrays = ($arrs*($arrs-1)*($arrs-2)*($arrs-3)*($arrs-4)/120);
				        	$msg['bonus'] = ($value['actionAmount']/$count)*$value['bonusProp']*$arrays;
				        	if($value['zjCount'] == 0){
								$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
							}
				        }
			        }
				}

				//连码
				if($value['playedId'] == 315){
					$balls = array($ball[0],$ball[1],$ball[2],$ball[3],$ball[4],$ball[5]);
					$action = explode(" ", $value['actionData']);
					$all = count(array_intersect($action,$balls));
					//四全中
					if($value['actionName'] == "LM4OF4"){
						if($all == 4){
							if($value['zjCount'] == 0){
								$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
							}
						}
					}
               
					//三全中
					if($value['actionName'] == "LM3OF3"){
                   	    if($all == 3){
							if($value['zjCount'] == 0){
								$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
							}
						}		
					}

					//三中二
					if($value['actionName'] == "LM2OF3"){
                   	    if($all == 2){
							if($value['zjCount'] == 0){
								$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
							}
						}			
					}

					//二中二
					if($value['actionName'] == "LM2OF2"){
                   	    if($all == 2){
							if($value['zjCount'] == 0){
								$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
							}
						}		
					}
              
					//二中特
					if($value['actionName'] == "LMSPOF2"){
						$all = count(array_intersect($action,$ball));	
						if($all == 2){
							if($value['zjCount'] == 0){
								$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
							}
						}	
					}

					//特串
					if($value['actionName'] == "LMSPOF"){
						$b6 = in_array($ball[6],$action);
						if($b6 && $all == 1){		
							if($value['zjCount'] == 0){
								$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
							}
						}			
					}
				}

				//自选不中
				if($value['playedId'] == 316){
					$action = explode(" ", $value['actionData']);
					$all = count(array_diff($action,$ball));
					//五不中
					if($value['actionName'] == "NOHIT5"){
						if($all == 5){
							if($value['zjCount'] == 0){
								$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
							}
						}
					}

					//六不中
					if($value['actionName'] == "NOHIT6"){
						if($all == 6){
							if($value['zjCount'] == 0){
								$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
							}
						}
					}

					//七不中
					if($value['actionName'] == "NOHIT7"){
						if($all == 7){
							if($value['zjCount'] == 0){
								$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
							}
						}
					}

					//八不中
					if($value['actionName'] == "NOHIT8"){
						if($all == 8){
							if($value['zjCount'] == 0){
								$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
							}
						}
					}

					//九不中
					if($value['actionName'] == "NOHIT9"){
						if($all == 9){
							if($value['zjCount'] == 0){
								$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
							}
						}
					}

					//十不中
					if($value['actionName'] == "NOHIT10"){
						if($all == 10){
							if($value['zjCount'] == 0){
								$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
							}
						}
					}

					//十一不中
					if($value['actionName'] == "NOHIT11"){
						if($all == 11){
							if($value['zjCount'] == 0){
								$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
							}
						}
					}

					//十二不中
					if($value['actionName'] == "NOHIT12"){
						if($all == 12){
							if($value['zjCount'] == 0){
								$this->infomsg($msg,$value['id'],$value['uid'],$msg['bonus']);
							}
						}
					}	
				}
			}
		}
	}

	public final function infomsg($msgs,$id,$uid,$coin){
	      if($this->updateRows($this->prename.'bets', $msgs, 'id='.$id)){
		    $sqls = $this->getRows("select coin from {$this->prename}members where uid = {$uid}");
		    $user['coin'] =  $sqls[0]['coin']+$coin;
			$this->updateRows($this->prename.'members', $user, 'uid='.$uid);
		}
	}	

}
