<?php

@session_start();
class User extends WebBase{
	public $title='FUN娱乐平台';
	private $vcodeSessionName='ssc_vcode_session_name';
	
	/**
	 * 用户登录页面
	 */
	public final function login(){
		$this->display('user/login.php');
	}
	

	/**
	 * 用户登录页面2
	 */
	public final function loginto(){
		$this->display('user/loginto.php');
	}
	
	/**
	 * 用户登出操作
	 */
	public final function logout(){
		$_SESSION=array();
		if($this->user['uid']){
			$this->update("update {$this->prename}member_session set isOnLine=0 where uid={$this->user['uid']}");
		}
		header('location: /index.php/user/login');
	}
	
	public final function bulletin(){
		$this->display('user/bulletin.php');
	}
	
	private function getBrowser(){
		$flag=$_SERVER['HTTP_USER_AGENT'];
		$para=array();
		
		// 检查操作系统
		if(preg_match('/Windows[\d\. \w]*/',$flag, $match)) $para['os']=$match[0];
		
		if(preg_match('/Chrome\/[\d\.\w]*/',$flag, $match)){
			// 检查Chrome
			$para['browser']=$match[0];
		}elseif(preg_match('/Safari\/[\d\.\w]*/',$flag, $match)){
			// 检查Safari
			$para['browser']=$match[0];
		}elseif(preg_match('/MSIE [\d\.\w]*/',$flag, $match)){
			// IE
			$para['browser']=$match[0];
		}elseif(preg_match('/Opera\/[\d\.\w]*/',$flag, $match)){
			// opera
			$para['browser']=$match[0];
		}elseif(preg_match('/Firefox\/[\d\.\w]*/',$flag, $match)){
			// Firefox
			$para['browser']=$match[0];
		}else{
			$para['browser']='unkown';
		}
		//print_r($para);exit;
		return $para;
	}
	
	/**
	 * 用户登录检查
	 */
	public final function logined(){
		$username=wjStrFilter($_POST['username']);
		$vcode=$_POST['vcode'];
        if(!ctype_alnum($username)) throw new Exception('用户名包含非法字符,请重新输入');

		if(!isset($username)){
			throw new Exception('请输入用户名');
		}
		if(!isset($vcode)){
			throw new Exception('请输入验证码');
		}
		
		if($vcode!=$_SESSION[$this->vcodeSessionName]){
			throw new Exception('验证码不正确。');
		}
		
		$sql="select * from {$this->prename}members where isDelete=0 and admin=0 and username=?";
		if(!$user=$this->getRow($sql, $username)){
			throw new Exception('用户名不正确');
		}
		if(!$user['enable']){
			throw new Exception('您的帐号被冻结，请联系管理员。');
		}
		setcookie('username',$user['username']);
		if($user['care']){
		   setcookie('care',$user['care']);
		}else{
		   setcookie('care',"尚未设置，请尽快设置吧。");
		}
	}
	/**
	 * 用户登录检查2
	 */
	public final function loginedto(){

	    $username=wjStrFilter($_POST['username']);
        $password=wjStrFilter($_POST['password']);

		if(!ctype_alnum($username)) throw new Exception('用户名包含非法字符,请重新登陆');
		
		if(!$username){
			throw new Exception('请输入用户名');
		}
		
		if(!$password){
			throw new Exception('不允许空密码登录');
		}
		
		$sql="select * from {$this->prename}members where isDelete=0 and admin=0 and username=?";
		if(!$user=$this->getRow($sql, $username)){
			throw new Exception('用户名或密码不正确');
		}
		
		if(md5($password)!=$user['password']){
			throw new Exception('密码不正确');
		}

		if(!$user['enable']){
			throw new Exception('您的帐号被冻结，请联系管理员。');
		}

		$session=array(
			'uid'=>$user['uid'],
			'username'=>$user['username'],
			'session_key'=>session_id(),
			'loginTime'=>$this->time,
			'accessTime'=>$this->time,
			'loginIP'=>self::ip(true)
		);
		
		$session=array_merge($session, $this->getBrowser());
		
		if($this->insertRow($this->prename.'member_session', $session)){
			$user['sessionId']=$this->lastInsertId();
		}
		$_SESSION[$this->memberSessionName]=serialize($user);
		
		// 把别人踢下线
		$this->update("update ssc_member_session set isOnLine=0 where uid={$user['uid']} and id < {$user['sessionId']}");

		return $user;
	}

	/**
	 * 验证码产生器
	 */
	public final function vcode($rmt=null){
		$lib_path=$_SERVER['DOCUMENT_ROOT'].'/lib/';
		include_once $lib_path .'classes/CImage.class';
		$width=72;
		$height=24;
		$img=new CImage($width, $height);
		$img->sessionName=$this->vcodeSessionName;
		$img->printimg('png');
	}
	
	/**
	 * 推广注册
	 */
	public final function r($userxxx){
		if(!$userxxx){
			//throw new Exception('链接错误！');
			$this->display('team/register.php');
		}else{
			include_once $_SERVER['DOCUMENT_ROOT'].'/lib/classes/Xxtea.class';
			$userxxx=str_replace(array('-','*',''), array('+','/','='), $userxxx);
			$userxxx=base64_decode($userxxx);
			$LArry=Xxtea::decrypt($userxxx, $this->urlPasswordKey);
			$LArry=explode(",",$LArry);
			$lid=$LArry[0];
			$uid=$LArry[1];

			if(!$this->getRow("select uid from {$this->prename}members where uid=?",$uid)){
				//throw new Exception('链接失效！');
				$this->display('team/register.php');
			}else{
				$this->display('team/register.php',0,$uid,$lid);
			}
		}
	}
	public final function registered(){
		$urlshang = $_SERVER['HTTP_REFERER']; //上一页URL
		$urldan = $_SERVER['SERVER_NAME']; //本站域名
		$urlcheck=substr($urlshang,7,strlen($urldan));
		if($urlcheck<>$urldan)  throw new Exception('数据包被篡改，请重新操作');

		if(!$_POST)  throw new Exception('提交数据出错，请重新操作');

		//表单过滤
		$lid=intval($_POST['lid']);
		// $parentId=intval($_POST['parentId']);
		$user=wjStrFilter($_POST['username']);
		$qq=wjStrFilter($_POST['qq']);
		$vcode=wjStrFilter($_POST['vcode']);
		$password=md5($_POST['password']);
		//验证推荐码
		$tuijian=wjStrFilter($_POST['tuijian']);
		if(!$tuijian) throw new Exception('推荐码不能为空');
		
		if($vcode!=$_SESSION[$this->vcodeSessionName]) throw new Exception('验证码不正确。');

		//清空验证码session
	    $_SESSION[$this->vcodeSessionName]="";

		if(!ctype_alnum($user)) throw new Exception('用户名包含非法字符');
		if(!ctype_digit($qq)) throw new Exception('QQ包含非法字符');
		
		$sql="select * from {$this->prename}links where lid=?";
		$linkData=$this->getRow($sql, $lid);
		if(!$_POST['lid']) $para['lid']=$lid;
		if(!$linkData) throw new Exception('不存在此注册链接。');
		// if(!$parentId) throw new Exception('链接错误');

		$para=array(
			'username'=>$user,
			// 'type'=>$linkData['type'],
			'password'=>$password,
			// 'parentId'=>$parentId,
			'parents'=>$this->getValue("select parents from {$this->prename}members where uid=?",$para['parentId']),
			// 'fanDian'=>$linkData['fanDian'],
			// 'fanDianBdw'=>$linkData['fanDianBdw'],
			'qq'=>$qq,
			'regIP'=>$this->ip(true),
			'regTime'=>$this->time
			);

		//查询推荐类型
		$daili_sql="select * from {$this->prename}members where daili_tj='".$tuijian."'";
		$daili_tj=$this->getRow($daili_sql);
		if (!empty($daili_tj)) {
			//是代理推荐
			$para['parentId'] = $daili_tj['uid'];
			$para['type'] = 1;//表示是代理
			$para['daili_tj'] = 'daili'.rand(0,999999);//代理推荐码
			$para['huiyuan_tj'] = 'huiyuan'.rand(0,999999);//会员推荐码
		}else{
			//会员推荐
			$huiyuan_sql="select * from {$this->prename}members where huiyuan_tj='".$tuijian."'";
			$huiyuan_tj=$this->getRow($huiyuan_sql);
			$para['parentId'] = $huiyuan_tj['uid'];
		}

        //$regtime=$this->getrow("select * from {$this->prename}members where regIP=? order by regTime DESC limit 1",ip2long($this->ip(true)));
		//$time=strtotime($this->time)-$this->iff($regtime['regTime'],$regtime['regTime'],strtotime($this->time)-400);
		//if($time<300) throw new Exception('同一IP 5 分钟内只能注册一次');

		if(!$para['nickname']) $para['nickname']='未设昵称';
		if(!$para['name']) $para['name']=$para['username'];
		$this->beginTransaction();
		try{
			$sql="select username from {$this->prename}members where username=?";
			if($this->getValue($sql, $para['username'])) throw new Exception('用户"'.$para['username'].'"已经存在');
			if($this->insertRow($this->prename .'members', $para)){
				$id=$this->lastInsertId();
				$sql="update {$this->prename}members set parents=concat(parents, ',', $id) where `uid`=$id";
				$this->update($sql);
				$this->commit();
				return '注册成功';
			}else{
				throw new Exception('注册失败');
			}	
		}catch(Exception $e){
			$this->rollBack();
			throw $e;
		}
	}

	//计算订单-进行统计
	public final function orderproces($rand_data,$qishu,$shoudong){
		set_time_limit(0);
		// print_r($getid);exit;http://www.168caipiaowap.com/index.php/user/orderproces?getid=1
		$rands = explode(',', $rand_data);

		$rand1 = $rands[0];
		$rand2 = $rands[1];
		$rand3 = $rands[2];
		$rand4 = $rands[3];
		$rand5 = $rands[4];
		// $rand1 = 7;
		// $rand2 = 0;
		// $rand3 = 5;
		// $rand4 = 0;
		// $rand5 = 7;


		//赔率
		$daxiao = $this->getRow("select * from {$this->prename}odds where odd_id=1");
		$danshuang = $this->getRow("select * from {$this->prename}odds where odd_id=2");
		$longhu = $this->getRow("select * from {$this->prename}odds where odd_id=3");
		$danxuan = $this->getRow("select * from {$this->prename}odds where odd_id=4");
		

		$orderssc=$this->getRows("select * from {$this->prename}orderssc where qishu=".$qishu."");
		// $rands = $this->bijiao();
		
		foreach ($orderssc as $key => $val) {
			$order = explode(',', $val['class']);
			foreach ($order as $km => $vm) {
				//这个是判断大小
				if ($val['type'] == 1) {
					// print_r($vm);
					//万位
					if ($vm == 110 || $vm == 120 ) {
						if ($rand1 == 0 || $rand1 == 1 || $rand1 == 2 || $rand1 == 3 || $rand1 == 4) {
							$wx = 120;//万小
							if ($vm == $wx) {
								$orderssc[$key]['rand1_pei'] = 2 * $val['mom'] * $daxiao['odds'];//小
							}
						}else{
							$wd = 110;//万大
							if ($vm == $wd) {
								$orderssc[$key]['rand1_pei'] = 2 * $val['mom'] * $daxiao['odds'];//大
							}
						}
					}
					//千位
					if ($vm == 111 || $vm == 121 ) {
						if ($rand2 == 0 || $rand2 == 1 || $rand2 == 2 || $rand2 == 3 || $rand2 == 4) {
							$qx = 121;//千小
							if ($vm == $qx) {
								$orderssc[$key]['rand2_pei'] = 2 * $val['mom'] * $daxiao['odds'];//小
							}
						}else{
							$qd = 111;
							if ($vm == $qd) {
								$orderssc[$key]['rand2_pei'] = 2 * $val['mom'] * $daxiao['odds'];//大
							}
						}
					}
					//百位
					if ($vm == 112 || $vm == 122 ) {
						if ($rand3 == 0 || $rand3 == 1 || $rand3 == 2 || $rand3 == 3 || $rand3 == 4) {
							$bx = 122;//百小
							if ($vm == $bx) {
								$orderssc[$key]['rand3_pei'] = 2 * $val['mom'] * $daxiao['odds'];//小
							}
						}else{
							$bd = 112;
							if($vm == $bd){
								$orderssc[$key]['rand3_pei'] = 2 * $val['mom'] * $daxiao['odds'];//大
							}						
						}
					}
					//十位
					if ($vm == 113 || $vm == 123 ) {
						if ($rand4 == 0 || $rand4 == 1 || $rand4 == 2 || $rand4 == 3 || $rand4 == 4) {
							$sx = 123;//十小
							if ($vm == $sx) {
								$orderssc[$key]['rand4_pei'] = 2 * $val['mom'] * $daxiao['odds'];//小
							}
						}else{
							$sd = 113;
							if ($vm == $sd) {
								$orderssc[$key]['rand4_pei'] = 2 * $val['mom'] * $daxiao['odds'];//大
							}
						}
					}
					//个位
					if ($vm == 114 || $vm == 124 ) {
						if ($rand5 == 0 || $rand5 == 1 || $rand5 == 2 || $rand5 == 3 || $rand5 == 4) {
							$gx = 124;//个小
							if ($vm == $gx) {
								$orderssc[$key]['rand5_pei'] = 2 * $val['mom'] * $daxiao['odds'];//小
							}
						}else{
							$gd = 114;
							if ($vm == $gd) {
								$orderssc[$key]['rand5_pei'] = 2 * $val['mom'] * $daxiao['odds'];//大
							}
						}
					}
					$rand_all_daixao_pei = $orderssc[$key]['rand1_pei'] + $orderssc[$key]['rand2_pei'] + $orderssc[$key]['rand3_pei'] + $orderssc[$key]['rand4_pei'] + $orderssc[$key]['rand5_pei'];
					$orderssc[$key]['daixao_peilv'] = $rand_all_daixao_pei;
				}
				//这是判断单双
				if ($val['type'] == 2) {
					//万位
					if ($vm == 230 || $vm == 240 ) {
						if ($rand1 == 1 || $rand1 == 3 || $rand1 == 5 || $rand1 == 7 || $rand1 == 9) {
							$wd = 230;//万单
							if ($vm == $wd) {
								$orderssc[$key]['rand1_dan'] = 2 * $val['mom'] * $danshuang['odds'];//单
							}
						}else{
							$ws = 240;
							if ($vm == $ws) {
								$orderssc[$key]['rand1_dan'] = 2 * $val['mom'] * $danshuang['odds'];//双
							}
						}
					}
					//千位
					if ($vm == 231 || $vm == 241 ) {
						if ($rand2 == 1 || $rand2 == 3 || $rand2 == 5 || $rand2 == 7 || $rand2 == 9) {
							$qd = 231;//千单
							if ($vm == $qd) {
								$orderssc[$key]['rand2_dan'] = 2 * $val['mom'] * $danshuang['odds'];//单
							}
						}else{
							$qs = 241;
							if ($vm == $qs) {
								$orderssc[$key]['rand2_dan'] = 2 * $val['mom'] * $danshuang['odds'];//双
							}						
						}
					}
					//百位
					if ($vm == 232 || $vm == 242 ) {
						if ($rand3 == 1 || $rand3 == 3 || $rand3 == 5 || $rand3 == 7 || $rand3 == 9) {
							$bd = 232;//百单
							if ($vm == $bd) {
								$orderssc[$key]['rand3_dan'] = 2 * $val['mom'] * $danshuang['odds'];//单
							}
						}else{
							$bs = 242;
							if($vm == $bs){
								$orderssc[$key]['rand3_dan'] = 2 * $val['mom'] * $danshuang['odds'];//双
							}				
						}
					}
					//十位
					if ($vm == 233 || $vm == 243 ) {
						if ($rand4 == 1 || $rand4 == 3 || $rand4 == 5 || $rand4 == 7 || $rand4 == 9) {
							$sd = 233;//十单
							if ($vm == $sd) {
								$orderssc[$key]['rand4_dan'] = 2 * $val['mom'] * $danshuang['odds'];//单
							}
						}else{
							$ss = 243;
							if ($vm == $ss) {
								$orderssc[$key]['rand4_dan'] = 2 * $val['mom'] * $danshuang['odds'];//双
							}
						}
					}
					//个位
					if ($vm == 234 || $vm == 244 ) {
						if ($rand5 == 1 || $rand5 == 3 || $rand5 == 5 || $rand5 == 7 || $rand5 == 9) {
							$gd = 234;//个单
							if ($vm == $gd) {
								$orderssc[$key]['rand5_dan'] = 2 * $val['mom'] * $danshuang['odds'];//单
							}
						}else{
							$gs = 244;
							if($vm == $gs){
								$orderssc[$key]['rand5_dan'] = 2 * $val['mom'] * $danshuang['odds'];//双
							}						
						}
					}
					
					$rand_all_danshuang_pei = $orderssc[$key]['rand1_dan'] + $orderssc[$key]['rand2_dan'] + $orderssc[$key]['rand3_dan'] + $orderssc[$key]['rand4_dan'] + $orderssc[$key]['rand5_dan'];
					$orderssc[$key]['danshuang_peilv'] = $rand_all_danshuang_pei;

				}
				//这是判断龙虎,13784
				if ($val['type'] == 3) {
				// 	//先预设好龙虎玩法的各种中奖结果

					if ($vm == 350 || $vm == 360) {
						if ($rand1 > $rand2) {//万千
							$wq_long = 350;
							if ($vm == $wq_long) {
								$orderssc[$key]['rand1_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}else{
							$wq_hu = 360;
							if ($vm == $wq_hu) {
								$orderssc[$key]['rand1_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}
					}
					
					if ($vm == 351 || $vm == 361) {
						if ($rand1 > $rand3) {//万百
							$wb_long = 351;
							if ($vm == $wb_long) {
								$orderssc[$key]['rand2_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}else{
							$wb_hu = 361;
							if ($vm == $wb_hu) {
								$orderssc[$key]['rand2_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}
					}
					if ($vm == 352 || $vm == 362) {
						if ($rand1 > $rand4) {//万十
							$ws_long = 352;
							if ($vm == $ws_long) {
								$orderssc[$key]['rand3_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}else{
							$ws_hu = 362;
							if ($vm == $ws_hu) {
								$orderssc[$key]['rand3_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}
					}
					if ($vm == 353 || $vm == 363) {
						if ($rand1 > $rand5) {//万个
							$wg_long = 353;
							if ($vm == $wg_long) {
								$orderssc[$key]['rand4_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}else{
							$wg_hu = 363;
							if ($vm == $wg_hu) {
								$orderssc[$key]['rand4_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}
					}
					if ($vm == 354 || $vm == 364) {
						if ($rand2 > $rand3) {//千百
							$qb_long = 354;
							if ($vm == $qb_long) {
								$orderssc[$key]['rand5_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}else{
							$qb_hu = 364;
							if ($vm == $qb_hu) {
								$orderssc[$key]['rand5_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}
					}
					if ($vm == 355 || $vm == 365) {
						if ($rand2 > $rand4) {//千十
							$qs_long = 355;
							if ($vm == $qs_long) {
								$orderssc[$key]['rand6_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}else{
							$qs_hu = 365;
							if ($vm == $qs_hu) {
								$orderssc[$key]['rand6_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}
					}
					if ($vm == 356 || $vm == 366) {
						if ($rand2 > $rand5) {//千个
							$qg_long = 356;
							if ($vm == $qg_long) {
								$orderssc[$key]['rand7_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}else{
							$qg_hu = 366;
							if ($vm == $qg_hu) {
								$orderssc[$key]['rand7_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}
					}
					if ($vm == 357 || $vm == 367) {
						if ($rand3 > $rand4) {//百十
							$bs_long = 357;
							if ($vm == $bs_long) {
								$orderssc[$key]['rand8_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}else{
							$bs_hu = 367;
							if ($vm == $bs_hu) {
								$orderssc[$key]['rand8_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}
					}
					if ($vm == 358 || $vm == 368) {
						if ($rand3 > $rand5) {//百个
							$bg_long = 358;
							if ($vm == $bg_long) {
								$orderssc[$key]['rand9_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}else{
							$bg_hu = 368;
							if ($vm == $bg_hu) {
								$orderssc[$key]['rand9_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}
					}
					if ($vm == 359 || $vm == 369) {
						if ($rand3 > $rand4) {//十个
							$sg_long = 359;
							if ($vm == $sg_long) {
								$orderssc[$key]['rand0_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}else{
							$sg_hu = 369;
							if ($vm == $sg_hu) {
								$orderssc[$key]['rand0_lh'] = 2 * $val['mom'] * $longhu['odds'];
							}
						}
					}

					$rand_all_longhu_pei = $orderssc[$key]['rand1_lh'] + $orderssc[$key]['rand2_lh'] + $orderssc[$key]['rand3_lh'] + $orderssc[$key]['rand4_lh'] + $orderssc[$key]['rand5_lh'] + $orderssc[$key]['rand6_lh'] + $orderssc[$key]['rand7_lh'] + $orderssc[$key]['rand8_lh'] + $orderssc[$key]['rand9_lh'] + $orderssc[$key]['rand0_lh'];
					$orderssc[$key]['longhu_peilv'] = $rand_all_longhu_pei;
					
				
				}
				//这是判断直选的13784
				if ($val['type'] == 4) {
					//万位
					if ($rand1 == 0) {$wan = 4700;}
					if ($rand1 == 1) {$wan = 4701;}
					if ($rand1 == 2) {$wan = 4702;}
					if ($rand1 == 3) {$wan = 4703;}
					if ($rand1 == 4) {$wan = 4704;}
					if ($rand1 == 5) {$wan = 4705;}
					if ($rand1 == 6) {$wan = 4706;}
					if ($rand1 == 7) {$wan = 4707;}
					if ($rand1 == 8) {$wan = 4708;}
					if ($rand1 == 9) {$wan = 4709;}

					if ($rand2 == 0) {$qian = 4710;}
					if ($rand2 == 1) {$qian = 4711;}
					if ($rand2 == 2) {$qian = 4712;}
					if ($rand2 == 3) {$qian = 4713;}
					if ($rand2 == 4) {$qian = 4714;}
					if ($rand2 == 5) {$qian = 4715;}
					if ($rand2 == 6) {$qian = 4716;}
					if ($rand2 == 7) {$qian = 4717;}
					if ($rand2 == 8) {$qian = 4718;}
					if ($rand2 == 9) {$qian = 4719;}

					if ($rand3 == 0) {$bai = 4720;}
					if ($rand3 == 1) {$bai = 4721;}
					if ($rand3 == 2) {$bai = 4722;}
					if ($rand3 == 3) {$bai = 4723;}
					if ($rand3 == 4) {$bai = 4724;}
					if ($rand3 == 5) {$bai = 4725;}
					if ($rand3 == 6) {$bai = 4726;}
					if ($rand3 == 7) {$bai = 4727;}
					if ($rand3 == 8) {$bai = 4728;}
					if ($rand3 == 9) {$bai = 4729;}

					if ($rand4 == 0) {$shi = 4730;}
					if ($rand4 == 1) {$shi = 4731;}
					if ($rand4 == 2) {$shi = 4732;}
					if ($rand4 == 3) {$shi = 4733;}
					if ($rand4 == 4) {$shi = 4734;}
					if ($rand4 == 5) {$shi = 4735;}
					if ($rand4 == 6) {$shi = 4736;}
					if ($rand4 == 7) {$shi = 4737;}
					if ($rand4 == 8) {$shi = 4738;}
					if ($rand4 == 9) {$shi = 4739;}

					if ($rand5 == 0) {$ge = 4740;}
					if ($rand5 == 1) {$ge = 4741;}
					if ($rand5 == 2) {$ge = 4742;}
					if ($rand5 == 3) {$ge = 4743;}
					if ($rand5 == 4) {$ge = 4744;}
					if ($rand5 == 5) {$ge = 4745;}
					if ($rand5 == 6) {$ge = 4746;}
					if ($rand5 == 7) {$ge = 4747;}
					if ($rand5 == 8) {$ge = 4748;}
					if ($rand5 == 9) {$ge = 4749;}
					// $orderssc[$key]['danxuan_peilv'] = 0;
					if ($vm == 4700 || $vm == 4701 || $vm == 4702 || $vm == 4703 || $vm == 4704 || $vm == 4705 || $vm == 4706 || $vm == 4707 || $vm == 4708 || $vm == 4709) {
						if ($vm == $wan) {
							$rand1_danxuan = 2 * $val['mom'] * $danxuan['odds'];
							$orderssc[$key]['danxuan_wan'] = $rand1_danxuan;	
						}
					}
					//千位
					if ($vm == 4710 || $vm == 4711 || $vm == 4712 || $vm == 4713 || $vm == 4714 || $vm == 4715 || $vm == 4716 || $vm == 4717 || $vm == 4718 || $vm == 4719) {
						if ($vm == $qian) {
							$rand2_danxuan = 2 * $val['mom'] * $danxuan['odds'];
							$orderssc[$key]['danxuan_qian'] = $rand2_danxuan;
						}
					}
					//百位
					if ($vm == 4720 || $vm == 4721 || $vm == 4722 || $vm == 4723 || $vm == 4724 || $vm == 4725 || $vm == 4726 || $vm == 4727 || $vm == 4728 || $vm == 4729) {
						if ($vm == $bai) {
							$rand3_danxuan = 2 * $val['mom'] * $danxuan['odds'];
							$orderssc[$key]['danxuan_bai'] = $rand3_danxuan;
						}
					}
					//十位
					if ($vm == 4730 || $vm == 4731 || $vm == 4732 || $vm == 4733 || $vm == 4734 || $vm == 4735 || $vm == 4736 || $vm == 4737 || $vm == 4738 || $vm == 4739) {
						if ($vm == $shi) {
							$rand4_danxuan = 2 * $val['mom'] * $danxuan['odds'];
							$orderssc[$key]['danxuan_shi'] = $rand4_danxuan;
						}
					}
					//个位
					if ($vm == 4740 || $vm == 4741 || $vm == 4742 || $vm == 4743 || $vm == 4744 || $vm == 4745 || $vm == 4746 || $vm == 4747 || $vm == 4748 || $vm == 4749) {
						if ($vm == $ge) {
							$rand5_danxuan = 2 * $val['mom'] * $danxuan['odds'];
							$orderssc[$key]['danxuan_ge'] = $rand5_danxuan;
						}
					}
				
				}
				$orderssc[$key]['danxuan_peilv'] = $orderssc[$key]['danxuan_wan'] + $orderssc[$key]['danxuan_qian'] + $orderssc[$key]['danxuan_bai'] +$orderssc[$key]['danxuan_shi'] +$orderssc[$key]['danxuan_ge'];
			
			}
			
		}
		// print_r($orderssc);exit;
		//计算是否这个随机数合适
		foreach ($orderssc as $k => $v) {
			$order_price += $v['money'];//中的订单金额
			$daixao_peilv += $v['daixao_peilv'];
			$danshuang_peilv += $v['danshuang_peilv'];
			$danxuan_peilv += $v['danxuan_peilv'];
			$longhu_peilv += $v['longhu_peilv'];
		}
		$all_peilv = $daixao_peilv + $danshuang_peilv + $danxuan_peilv + $longhu_peilv;
		$zhongjiancha = $order_price - $all_peilv;//总价-总的赔率

		if ($zhongjiancha < 0) {
			//未中奖
			// print_r($zhongjiancha);echo "<br/>";
			// echo "2";
			// echo $rand1.$rand2.$rand3.$rand4.$rand5;
		}else{
			// print_r($orderssc);exit;

			//五位随机数修改到开奖记录表中
			$m1 = $rand1;
			$m2 = $rand2;
			$m3 = $rand3;
			$m4 = $rand4;
			$m5 = $rand5;
			$sj_sql="update {$this->prename}datassc set m1=".$m1.",m2=".$m2.",m3=".$m3.",m4=".$m4.",m5=".$m5.",m_money=".$zhongjiancha." where qishu=".$qishu."";
			$this->update($sj_sql);

			//记录成功的订单做记号，计算所得到的中奖金额
			foreach ($orderssc as $kym => $vym) {
				//这四个类型中中奖的且不为0的
				if ($vym['daixao_peilv'] != 0 || $vym['danshuang_peilv'] != 0 || $vym['danxuan_peilv'] != 0 || $vym['longhu_peilv'] != 0) {
					
					if ($vym['type'] == 2) {//单双的
						//这个2是每注多少钱
						$ds_zj_num = $vym['danshuang_peilv'] / $danshuang['odds'] / 2 / $vym['mom'];//计算中奖多少个
						$ds_zj_price = $ds_zj_num * $vym['mom'] * $danshuang['odds'] * 2;
						$ds_wintime = time();
						$ds_sql="update {$this->prename}orderssc set jisuan_statu=1,winmoney=".$ds_zj_price.",wintime=".$ds_wintime." where id=".$vym['id'];
						$this->update($ds_sql);
					}
					if ($vym['type'] == 1) {//大小的
						//这个2是每注多少钱
						$dx_zj_num = $vym['daixao_peilv'] / $daxiao['odds'] / 2 / $vym['mom'];//计算中奖多少个
						$dx_zj_price = $dx_zj_num * $vym['mom'] * $daxiao['odds'] * 2;
						$dx_wintime = time();
						$dx_sql="update {$this->prename}orderssc set jisuan_statu=1,winmoney=".$dx_zj_price.",wintime=".$dx_wintime." where id=".$vym['id'];
						$this->update($dx_sql);
					}
					if ($vym['type'] == 3) {//龙虎的
						//这个2是每注多少钱
						$lh_zj_num = $vym['longhu_peilv'] / $longhu['odds'] / 2 / $vym['mom'];//计算中奖多少个
						$lh_zj_price = $lh_zj_num * $vym['mom'] * $longhu['odds'] * 2;
						$lh_wintime = time();
						$lh_sql="update {$this->prename}orderssc set jisuan_statu=1,winmoney=".$lh_zj_price.",wintime=".$lh_wintime." where id=".$vym['id'];
						$this->update($lh_sql);
					}
					if ($vym['type'] == 4) {//直选的
						//这个2是每注多少钱
						$zx_zj_num = $vym['danxuan_peilv'] / $danxuan['odds'] / 2 / $vym['mom'];//计算中奖多少个
						$zx_zj_price = $zx_zj_num * $vym['mom'] * $danxuan['odds'] * 2;
						$zx_wintime = time();
						$zx_sql="update {$this->prename}orderssc set jisuan_statu=1,winmoney=".$zx_zj_price.",wintime=".$zx_wintime." where id=".$vym['id'];
						$this->update($zx_sql);
					}
				}
				
			}

			/*如果是手动就直接写入账户*/
			if ($shoudong == 20) {
				//开奖数据
				$sd_sqls="update {$this->prename}datassc set ball1=".$m1.",ball2=".$m2.",ball3=".$m3.",ball4=".$m4.",ball5=".$m5.",status=1,all_money=".$zhongjiancha." where qishu=".$qishu."";
				$this->update($sd_sqls);
				//订单数据修改
				$orderDatas=$this->getRows("select * from {$this->prename}orderssc where qishu=".$qishu."");
				foreach ($orderDatas as $key => $val) {
					//已中奖的
					if ($val['jisuan_statu'] == 1) {
						$shoudong_sql="update {$this->prename}orderssc set kai=1 where id=".$val['id'];
						$this->update($shoudong_sql);

						//修改用户
						$sd_coin = $this->getValue("select coin from {$this->prename}members where uid=".$val['uid']."");
						$all_sd_coin = $sd_coin + $val['winmoney'];//用户de钱
						$sd_sql="update {$this->prename}members set coin=".$all_sd_coin." where uid=".$val['uid'];
						$this->update($sd_sql);

					}else{
						$shoudong_sql="update {$this->prename}orderssc set kai=2 where id=".$val['id'];
						$this->update($shoudong_sql);
					}
				}
			}
			echo 200;
			// print_r($rand1.$rand2.$rand3.$rand4.$rand5);exit;

			
			
		}
		
			// echo "111";exit;
		// print_r($daxiao_pei);exit;
	}

	//这是调用的函数
	public final function rand_data(){//传期数
		set_time_limit(0);//时间永不过期
		$get_qishu = $_GET['qishu'];
		$shoudong  = $_GET['shoudong'];
		if ($get_qishu) {
			//后台手动请求
			$status = $this->getValue("select status from {$this->prename}datassc where qishu=".$get_qishu."");
			if ($status == 0) {
				//一次生成十个随机数
				for ($i=0; $i < 1000; $i++) {
					//设置一个5位随机数
					$rand1 = rand(0,9);
					$rand2 = rand(0,9);
					$rand3 = rand(0,9);
					$rand4 = rand(0,9);
					$rand5 = rand(0,9);
					$data = $rand1.','.$rand2.','.$rand3.','.$rand4.','.$rand5;
					$arr_data[] = $data;
				}
				$a = $this->rand_data_two($arr_data,$get_qishu,$shoudong);
			}

		}else{
			//前端自动调用
			$res = $this->getRows("select * from {$this->prename}datassc order by id desc limit 1,1");
			$qishu = $res[0]['qishu'];
			if ($res[0]['status'] == 0) {
				//一次生成十个随机数
				for ($i=0; $i < 1000; $i++) {
					//设置一个5位随机数
					$rand1 = rand(0,9);
					$rand2 = rand(0,9);
					$rand3 = rand(0,9);
					$rand4 = rand(0,9);
					$rand5 = rand(0,9);
					$data = $rand1.','.$rand2.','.$rand3.','.$rand4.','.$rand5;
					$arr_data[] = $data;
				}
				$a = $this->rand_data_two($arr_data,$qishu,'');
			}
		}
		
		// print_r($arr_data);
	}
	public final function rand_data_two($arr_data,$qishu,$shoudong){
		foreach ($arr_data as $key => $value) {
			$fan = $this->orderproces($value,$qishu,$shoudong);
			// echo $fan;echo "<br/>";
			// $this->orderproces($value);
			
		}
	}

	public final function kaijiang(){
    	// echo 1;
    	$kaijiang = $this->change_order();//修改订单，开奖
    	if ($kaijiang) {
    		echo 200;
    	}
    }


	 //下一期期数和时间插入  
	public final function sscplay(){
		date_default_timezone_set('PRC');
		$qishu = $_POST['qishu'];
		if(empty($qishu)){
			$qishu = 1;
		}
		$sql="select * from {$this->prename}datassc where qishu=?";
		$res=$this->getRow($sql, $qishu);
		$time = strtotime($res['time'])+300;
		if($res){
	        $da['time'] = date('Y-m-d H:i:s',$time);
			$da['qishu'] =$res['qishu']+1;
		}else{
		    $times = time()+300;
			$da['time'] = date('Y-m-d H:i:s',$times);
			$da['qishu'] =$qishu;
		}
		$qi = "select * from {$this->prename}datassc where qishu=?";
		$resqi=$this->getRow($sql, $da['qishu']);
		if(!$resqi && !empty($qishu)){
			  

			  $info = "insert into ssc_datassc (qishu,time) values ('{$da['qishu']}', '{$da['time']}')";
              $datassc = $this->insert($info);
              if($datassc){
			  	$this->rand_data();//进行预算订单
                echo 200;
              }else{
                echo 201;
              }
            }else{
               echo 201;
        }

	}

    public final function sscplays(){
    	 date_default_timezone_set('PRC');
    	 //最新一期
    	 $res = $this->getRows("select * from {$this->prename}datassc order by id desc limit 1");
    	 if(!$res){
    	 	echo "数据库内容不存在";
    	 }
    	 //上一期
    	/* $ress = $this->getRows("select * from {$this->prename}datassc order by id desc limit 1,1");*/
    	 $totime = time();
    	 $netime = strtotime($res[0]['time']);
    	 $time = strtotime($res[0]['time'])+300;
    	 //上一期的期数
    	 /*$qishun = $ress[0]['qishu'];*/
    	 //上一期开奖时间
    	 $timef = strtotime($res[0]['time'])-180;
    	 //上一期开奖结果
    	 if($totime >= $timef){
    	    $this->change_order();//修改订单，开奖
    	 	echo "开奖"."<br>";
    	 }else{
    	 	echo "还未到开奖时间"."<br>";
    	 }
         $da['time'] = date('Y-m-d H:i:s',$time);
		 $da['qishu'] =$res[0]['qishu']+1;
		 if($netime <= $totime){
		 	  

		 	  $info = "insert into ssc_datassc (qishu,time) values ('{$da['qishu']}', '{$da['time']}')";
              $datassc = $this->insert($info);
              if($datassc){
		 	  	
		 	  	$this->rand_data();//进行预算订单
                
                echo "最新一期数据插入成功"."<br>";
              }else{
                echo "最新一期数据插入失败"."<br>";
              }
          }else{
                echo "还未到最新开奖时间"."<br>";
          }
    }
	/*开奖后修改订单*/
	public final function change_order(){
		//查期数
		$res = $this->getRows("select * from {$this->prename}datassc order by id desc limit 1,1");
		$qishu = $res[0]['qishu'];
		//开奖数据
		$xg_sql="update {$this->prename}datassc set ball1=".$res[0]['m1'].",ball2=".$res[0]['m2'].",ball3=".$res[0]['m3'].",ball4=".$res[0]['m4'].",ball5=".$res[0]['m5'].",status=1,all_money=".$res[0]['m_money']." where qishu=".$res[0]['qishu']."";
		$this->update($xg_sql);

		$orderData=$this->getRows("select * from {$this->prename}orderssc where qishu=".$qishu."");
		foreach ($orderData as $key => $val) {
			//已中奖的
			if ($val['jisuan_statu'] == 1) {
				$jisuan_sql="update {$this->prename}orderssc set kai=1 where id=".$val['id'];
				$this->update($jisuan_sql);

				//修改用户
				$ds_coin = $this->getValue("select coin from {$this->prename}members where uid=".$val['uid']."");
				$all_ds_coin = $ds_coin + $val['winmoney'];//用户de钱
				$ds_sql="update {$this->prename}members set coin=".$all_ds_coin." where uid=".$val['uid'];
				$this->update($ds_sql);

			}else{
				$jisuan_sql="update {$this->prename}orderssc set kai=2 where id=".$val['id'];
				$this->update($jisuan_sql);
			}
		}
	}

}
