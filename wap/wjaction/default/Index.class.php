<?php
class Index extends WebLoginBase{
	public $pageSize=10;
	
	public final function game($type=null, $groupId=null, $played=null){
		// $type = $_GET['type'];
		// $groupId = $_GET['groupId'];

		if($type) $this->type=intval($type);
		if($groupId){
			$this->groupId=intval($groupId);
		}else{
			// 默认进入三星玩法
			$this->groupId=6;
		}
		if($played) $this->played=intval($played);
		$this->getSystemSettings();
		$this->isGame = true;
		$this->display('main.php');
	}
	public final function gamess(){
		$type = $_GET['type'];
		$groupId = $_GET['groupId'];

		$sql="select groupName from {$this->prename}played_group where id=$groupId";
		$groupName=$this->getValue($sql, $groupId);

		$sql="select id, name, playedTpl, enable  from {$this->prename}played where enable=1 and groupId=$groupId order by sort";
		$playeds=$this->getRows($sql, $groupId);
		if(!$playeds) {
			echo json_encode(1);exit;
			// echo json_encode('<div style="height:150px;margin-top:50px;text-align:center;color:#f00">暂无玩法</div>');exit;
		} 
		if(!$this->played) $this->played=$playeds[0]['id'];
		foreach ($playeds as $key => $val) {
			$playeds[$key]['type'] = $type;
		}
		echo json_encode($playeds);exit;
		// if($playeds) foreach($playeds as $played){
		// 	if($played['id']==$this->played){
		// 		echo json_encode('<div style="margin-bottom:2em;"><div class="beet-tips-tit"><span id="play_title2">'.$played['name'].'</span></div></div>');exit;
		// 	}
		// }
	}
	
   //平台首页
	public final function main(){
		//把该会员的顶级id存session
		if ($this->user['parentId'] == '') {
			$dingji_id = $this->user['dingji_id'];
		}else{
			$dingji_id = $this->get_top_parentid($this->user['parentId']);
		}
		$_SESSION['dingji_id'] = $dingji_id;
		
		$this->display('index1.php');
	}

	//获取顶级id
	public final function get_top_parentid($parentId){ 
	    $Par_user=$this->getRows("select * from {$this->prename}members where uid=".$parentId."");
	    foreach ($Par_user as $key => $val) {
	    	$arr = $val;
	    }
	    if($arr['parentId'] != '') {
	    	return $this->get_top_parentid($arr['parentId']);
	    }
	    return $arr['dingji_id'];
	}
	public final function index1(){
		$this->display('index.php');
	}

	public final function goucai(){
		$this->display('goucai.php');
	}
	
	//走势
	public final function zoushi(){
		$this->display('index/zoushi.php');
	}	
//走势图
	public final function chartssc(){
		$this->display('chart/chart_ssc.php');
	}
	//幸运卡丁车走势图
	public final function chartlucky(){
		$this->display('chart/chart_lucky.php');
	}
	public final function chart115(){
		$this->display('chart/chart_115.php');
	}
	public final function chartk3(){
		$this->display('chart/chart_k3.php');
	}
		public final function chartlhc(){
		$this->display('chart/chart_lhc.php');
	}
		public final function chartpk(){
		$this->display('chart/chart_pk.php');
	}
		public final function chartklsf(){
		$this->display('chart/chart_klsf.php');
	}	
	public final function znz($type=null, $groupId=null, $played=null){
		if($type) $this->type=intval($type);
		if($groupId) $this->groupId=intval($groupId);
		if($played) $this->played=intval($played);
		
		$this->getTypes();
		$this->getPlayeds();
		$this->display('index/inc_game_znz.php');
	}
	
	public final function group($type, $groupId){
		$this->type=intval($type);
		$this->groupId=intval($groupId);
		$this->display('index/load_tab_group.php');
	}
	
	public final function played($type,$playedId){
		$playedId=intval($playedId);
		$sql="select type, groupId, playedTpl from {$this->prename}played where id=?";
		$data=$this->getRow($sql, $playedId);
		$this->type=intval($type);
		if($data['playedTpl']){
			$this->groupId=$data['groupId'];
			$this->played=$playedId;
			$this->display("index/game-played/{$data['playedTpl']}.php");
		}else{
			$this->display('index/game-played/un-open.php');
		}
	}
	
	// 加载玩法介绍信息
	public final function playTips($playedId){
		$this->display('index/inc_game_tips.php', 0, intval($playedId));
	}

	// 阿里分分彩玩法说明
	public final function alffc(){
		$this->display('explain/alffc.php');
	}

	// 重庆时时彩玩法说明
	public final function cqssc(){
		$this->display('explain/cqssc.php');
	}

	// 北京PK10玩法说明
	public final function bjpk(){
		$this->display('explain/bjpk10.php');
	}

	// 新疆时时彩玩法说明
	public final function xjssc(){
		$this->display('explain/xjssc.php');
	}

	// 江西时时彩玩法说明
	public final function jxssc(){
		$this->display('explain/jxssc.php');
	}

	// 广东11选5玩法说明
	public final function gd(){
		$this->display('explain/gd11.php');
	}

    // 山东11选5玩法说明
	public final function sd(){
		$this->display('explain/sd11.php');
	}

	// 江西11选5玩法说明
	public final function jx(){
		$this->display('explain/jx11.php');
	}

	// 香港六合彩玩法说明
	public final function xglhc(){
		$this->display('explain/xglhc.php');
	}
	
	public final function getQiHao($type){
		$thisNo=$this->getGameNo($type);
		return array(
			'lastNo'=>$this->getGameLastNo($type),
			'thisNo'=>$this->getGameNo($type),
			'diffTime'=>strtotime($thisNo['actionTime'])-$this->time,
			'validTime'=>$thisNo['actionTime'],
			'kjdTime'=>$this->getTypeFtime($type)
		);
	}
	
	// 弹出追号层HTML
	public final function zhuiHaoModal($type){
		$this->display('index/game-zhuihao-modal.php');
	}
	
	// 追号层加载期号
	public final function zhuiHaoQs($type, $mode, $count){
		$this->type=intval($type);
		$this->display('index/game-zhuihao-qs.php', 0, $mode, $count);
	}
	
	// 加载历史开奖数据
	public final function getHistoryData($type){
		$this->type=intval($type);
		$this->display('index/inc_data_history.php');
	}
	
	// 历史开奖HTML
	public final function historyList($type){
	    $this->type=intval($type);
		$this->display('index/history-list.php',$pageSize,$type);
	}
	
	public final function getLastKjData($type){
		$ykMoney=0;
		$czName='重庆时时彩';
		$this->type=intval($type);
		if(!$lastNo=$this->getGameLastNo($this->type)) throw new Exception('查找最后开奖期号出错');
		if(!$lastNo['data']=$this->getValue("select data from {$this->prename}data where type={$this->type} and number='{$lastNo['actionNo']}'"))
		throw new Exception('获取数据出错');
		
		$thisNo=$this->getGameNo($this->type);
		$lastNo['actionName']=$czName;
		$lastNo['thisNo']=$thisNo['actionNo'];
		$lastNo['diffTime']=strtotime($thisNo['actionTime'])-$this->time;
		$lastNo['kjdTime']=$this->getTypeFtime($type);
		return $lastNo;
	}
	
	// 加载人员信息框
	public final function userInfo(){
		$this->display('index/inc_user.php');
	}

	public final function ssc(){
		$this->display('index/inc_data_current _ssc.php');
	}

    //幸运卡丁车开奖结果
    public final function luckylist(){
    	$this->display("index/history-list-lucky.php");
    }

    public final function orders(){
    	$order['qishu'] = $_POST['qishu'];
    	$order['money'] = $_POST['money'];
    	$order['mom'] = $_POST['beishu'];
    	$order['ball'] = $_POST['fenl'];
    	$order['codeid'] = date('Ymd') . str_pad(mt_rand(1, 99999), 5, '0');
    	$order['uid'] = $this->user['uid'];
    	$order['username'] = $this->user['username'];
    	$order ['ordertime'] = time();
    	$class = $_POST['htarray'];
    	$cla = array();
    	foreach ($class as $key => $value) {
    		$order['type'] = substr($value,0,1);
    		$cla[] = $value;
    	}
    	$order['class'] = join(",",array_unique($cla));
    	$user = $this->getRow("select uid,username,coin from {$this->prename}members where uid=?",$order['uid']);
    	$money = $user['coin']-$order['money'];
    	if($order['money']>$user['coin']){
    	   /* echo json_encode(['code'=>201,'infos'=>'金额不足']);*/
    	    echo 201;
    	    exit;
    	}
    	if($this->insertRow($this->prename.'orderssc', $order)){
			$ssc = $this->lastInsertId();
			$sql="update {$this->prename}members set coin=? where uid={$order['uid']}";
		    $members = $this->update($sql,$money);
			if($ssc && $members){
				/* echo json_encode(['code'=>200,'info'=>'下注成功']);*/
				 echo 200;
				 exit;
			}else{
				 echo 203;
				 exit;
			}
		}
    } 

}