<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<?php $this->display('inc_skin.php', 0 , '会员中心 - 申请提现'); ?>
<style type="text/css">
	.bank_box{
        	width:80% !important;
        	padding: 10px;
        	background: linear-gradient(to right, #F88E4F,#FC6C54);
        	margin:10px auto;
        	border-radius: 6px;
        }
        .bank_box p{
        	color: #fff;
        	text-align: left;
        	width: 88%;
        	margin:0 auto;
        }
        .table_b{
        	border: none;
        }
        .fl{
        	float: left;
        }
        .fr{
        	float: right;
        	width: 76%;
        }
        .fr input{
        	border:none;
        	display: block;
        	width: 100%;
        	outline:none;
        }
        .backBox{
        	overflow: hidden;
        	width: 100%;
        	padding:6px 0;
        	border-bottom: 1px solid #eee;
        }
        .copyss{
        	width: 22%;
        	padding-left: 2%;
        	text-align: left;
        }
        .bankBigboxss{
        	width: 90%;
        	background: #fff;
        	padding:10px;
        	margin: 10px auto;
        }
        .pagemain{
        	background: #eee !important;
        }
        .file-box{
			position: relative;
			padding: 6px 5px;
			width: 120px;
			overflow: hidden;
			color:#fff;
			background-color: #FC6C54;
			border-radius: 6px;
			text-align: center;
			margin: 10px auto;
			}
		.file-btn{
			width: 60px;
			width: 100%;
			height: 100%;
			position: absolute;
			top: 0;
		    left: 0;
		    outline: none;
		    background-color: transparent;
		    filter:alpha(opacity=0);
			-moz-opacity:0;
			-khtml-opacity: 0; 
			opacity: 0;
		}
		#page-header{
			margin-top:50px;
		}
		.rechargeBox{
			width: 100%;
			height: 44px;
			background: #fe962f;
			position: fixed;
			top:0;
			left: 0;
			z-index: 10000
		}
		.reveal-left{
			opacity: 0;
		}
	.rechargeBox .left {
    background: url(/images/blank_01.png) no-repeat;
    background-size: 15px;
    height: 44px;
    width: 43px;
    display: block;
    margin-top: 8px;
    left: 5px;
    position: relative;
    z-index: 3;
}
.pagemain input.btn{
	font-size: 14px;
}
</style>
<script type="text/javascript">
function beforeToCash(){
	if(!this.amount.value) throw('请填写提现金额');
	// if(!this.bankname.value) throw('请填写银行卡名称');
	// if(!this.bank_hm.value) throw('请填写银行卡号');
	// if(!this.names.value) throw('请填写持卡人姓名');
	if(!this.amount.value.match(/^[0-9]*[1-9][0-9]*$/)) throw('提现金额错误');
	
	showPaymentFee()
}

function panduan(){
	var coins = $('#coins').val();
	var amount = $('#amount').val();
	if (amount > coins) {
		//大于余额
		alert('不能超出当前余额');
		$('#amount').val('');
	}
	if (amount > 100000) {
		//大于余额
		alert('不能超出最大额度');
		$('#amount').val('');
	}
}


function toCash(err, data){
	if(err){
		alert(err)
	}else{
		reloadMemberInfo();
		$(':password').val('');
		$('input[name=amount]').val('');
		window.location.href="/index.php/cash/toCashResult";
		//alert(data);
		//$.messager.lays(200, 100);
	    //$.messager.anim('fade', 1000);
	    //$.messager.show("<strong>系统提示</strong>", "提款成功！<br />将在10分钟内到账！",0);

	}
}
$(function(){
	$('input[name=amount]').keypress(function(event){
		event.keyCode=event.keyCode||event.charCode;
		
		return !!(
			// 数字键
			(event.keyCode>=48 && event.keyCode<=57)
			|| event.keyCode==13
			|| event.keyCode==8
			|| event.keyCode==46
			|| event.keyCode==9
		)
	});
	
	//var form=$('form')[0];
	//form.account.value='';
	//form.username.value='';
});
</script>
<script type="text/javascript">
function showPaymentFee() {
   $("#ContentPlaceHolder1_txtMoney").val($("#ContentPlaceHolder1_txtMoney").val().replace(/\D+/g, ''));
   jQuery("#chineseMoney").html(changeMoneyToChinese($("#ContentPlaceHolder1_txtMoney").val()));
        }
</script>
</head> 
 
<body>
<div id="mainbody"> 
	<div class="rechargeBox">
<a href="/index.php" style="display: block;position: absolute;top:0;left: 0"><span class="left"></span></a><p style="width: 100%;text-align: center;font-size: 16px;color: #fff;height: 44px;line-height: 44px;letter-spacing: 4px;font-weight: 600;">提现</p>
	</div>
<?php $this->display('inc_header.php'); ?>
<div class="pagetop"></div>
<div class="pagemain">
<div class="display biao-cont">
<?php
	//最新一条记录,默认输出
 	$tixian = $this->getRows("select * from {$this->prename}tixian where uid=".$this->user['uid']." order by tx_id desc");
 	$jilus = $tixian[0];
 	$cur_coin = $this->getValue("select coin from {$this->prename}members where uid=".$this->user['uid']."");

?>
<form action="/index.php/cash/tixianshenqing" method="post"  enctype="multipart/form-data">
<table width="100%" border="0" cellspacing="1" cellpadding="4" class='table_b'>
    <tr class='table_b_th'>
      <td align="left" style="font-weight:bold;padding-left:10px;">提款申请</td>
    </tr>
    
    <tr height=25 class='table_b_tr_b' >
      <td height="80" align="left" class="copys" ><p>每天限提&nbsp&nbsp<strong style="font-size:16px;color:red;"><?=$this->getValue("select maxToCashCount from {$this->prename}member_level where level=?", $this->user['grade'])?></strong>&nbsp&nbsp次,今天您已经成功发起了&nbsp&nbsp<strong style="font-size:16px;color:green"><?=$times?></strong>&nbsp&nbsp次提现申请</p>
        <p>每天的提现处理时间为：<strong style="font-size:16px;color:red;" >
          早上 <?=$this->settings['cashFromTime']?> 至 晚上
          <?=$this->settings['cashToTime']?></strong></p>
        <p>提现10分钟内到账。(如遇高峰期，可能需要延迟到三十分钟内到帐)</p>
        <p style="color:blue;">银行卡用户每日最小提现&nbsp&nbsp 
          <strong style="color:green;font-size:16px;"><?=$this->settings['cashMin']?></strong>&nbsp&nbsp元，最大提提现&nbsp&nbsp 
          <strong style="color:green;font-size:16px;"><?=$this->settings['cashMax']?></strong>&nbsp&nbsp元。财付通/支付宝用户,最小提现&nbsp&nbsp<strong style="color:green;font-size:16px;"><?=$this->settings['cashMin1']?></strong>&nbsp&nbsp元，最大提现&nbsp&nbsp<strong style="color:green;font-size:16px;"><?=$this->settings['cashMax1']?></strong>&nbsp&nbsp元。
        </td>
      </tr>
	

   
</table> 
<div class="bankBigboxss">
	<div class="backBox" >
	  <div  class="copyss fl">银行名称：</div>
	  <div class="fr" ><input name="bankname" id="bankname" value="<?=$jilus['bankname']?>" /></div>
	  </div>
	<div class="backBox" >
	  <div  class="copyss fl">银行卡号：</div>
      <div class="fr" ><input name="bank_hm" id="bank_hm" value="<?=$jilus['bank_hm']?>" /></div>
      </div>
     <div class="backBox">
       <div class="copyss fl">持卡人：</div>
      <div class="fr"><input name="names" id="names" value="<?=$jilus['names']?>" /></div>
      </div>
     <div class="backBox">
       <div class="copyss fl">提现金额：</div>
      <div class="fr"><input name="amount" id="amount" class="spn9"  value="" onblur="panduan()"/></div>
      <input type="hidden" value="<?=$cur_coin?>" id="coins" >
      </div>
	<div class="backBox">
      <div class="fr"><strong style="color:red;margin-left:10px" id="chineseMoney"></strong></div>
      </div>
     <div class="backBox">
      <div align="center" style="font-weight:bold;"><input type="button" id='put_button_pass' class="btn darwingbtn" value="提交申请"  onclick="$(this).closest('form').submit()"></div>
      </div> 
 </div>     
</form>


    </div>
<?php $this->display('inc_footer.php'); ?> 
</div>
<div class="pagebottom"></div>
</div>

</body>
</html>
  
   
 