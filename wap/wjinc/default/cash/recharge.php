<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<?php $this->display('inc_skin.php', 0 , '会员中心 - 在线充值'); ?>
<script type="text/javascript">
$(function(){
	$('form').trigger('reset');
	$(':radio').click(function(){
		var data=$(this).data('bank'),
		box=$('#display-dom');
		 
		$('#bank-type-icon', box).attr('src', '/'+data.logo);
		//$('#bank-link', box).attr('href', data.home);
		//$('#bank-account', box).val(data.account);
		//$('#bank-username', box).val(data.username);
		//$('.example2', box).attr('rel', data.rechargeDemo);
		
		if($.cookie('rechargeBank')!=data.id) $.cookie('rechargeBank', data.id, 360*24);
	});
	
	var bankId=$.cookie('rechargeBank')||$(':radio').attr('value');
	$(':radio[value='+bankId+']').click();
	
	$('.copy').click(function(){
		var text=document.getElementById($(this).attr('for')).value;
		if(!CopyToClipboard(text, function(){
			alert('复制成功');
		}));
	});
	
	$('.example2').click(function(){
		var src='/'+$(this).attr('rel');
		if(src) $('<div>').append($('<img>',{src:src,width:'640px',height:'480px'})).dialog({width:630,height:500,title:'充值演示'});
	});
});
function checkRechargess(){
	if(!this.moneys.value) throw('请填写充值金额');
	showPaymentFee();
	
	if(!this.bank_hm.value) throw('请输入银行卡号');
	if(!this.names.value) throw('请输入付款人');
	showPaymentFee();
}
function checkRecharge(){
	if(!this.amount.value) throw('请填写充值金额');
	showPaymentFee();
	//if(isNaN(amount)) throw('充值金额错误');
	//if(!this.amount.value.match(/^\d+(\.\d{0,2})?$/)) throw('充值金额错误');
	showPaymentFee();
	var amount=parseInt(this.amount.value),
	$this=$('input[name=amount]',this),
	min=parseInt($this.attr('min')),
	max=parseInt($this.attr('max'));
	min1=parseInt($this.attr('min1')),
	max1=parseInt($this.attr('max1'));
	
	if($('input[name=mBankId]').val()==2||$('input[name=mBankId]').val()==3){
		if(amount<min1) throw('支付宝/财付通充值金额最小为'+min1+'元');
		if(amount>max1) throw('支付宝/财付通充值金额最多限额为'+max1+'元');
		showPaymentFee();
	}else{
		if(amount<min) throw('充值金额最小为'+min+'元');
		if(amount>max) throw('充值金额最多限额为'+max+'元');
		showPaymentFee();

	}
	if(!this.vcode.value) throw('请输入验证码');
	if(this.vcode.value<4) throw('验证码至少4位');
	showPaymentFee();
}
function toCash(err, data){
	//console.log(err);
	if(err){
		alert(err)
		$("#vcode").trigger("click");
	}else{
		$(':password').val('');
		$('input[name=amount]').val('');
		$('.biao-cont').html(data);
	}
}
$(function(){
	$('input[name=amount]').keypress(function(event){
		//console.log(event);
		event.keyCode=event.keyCode || event.charCode;
		return !!(
			// 数字键
			(event.keyCode>=48 && event.keyCode<=57)
			|| event.keyCode==13
			|| event.keyCode==8
			|| event.keyCode==9
			|| event.keyCode==46
		)
	});
});
</script>
<script type="text/javascript">
$(function(){
	$('.example2').click(function(){
		var src='/'+$(this).attr('rel');
		if(src) $('<img>',{src:src}).css({width:'640px',height:'480px'}).dialog({width:660,height:500,title:'充值演示'});
	});
	
	//$('.copy').click(function(){
	//	var text=document.getElementById($(this).attr('for')).value;
	//	if(!CopyToClipboard(text, function(){
	//		alert('复制成功');
	//	}));
	//});
});
</script>

<!--//复制程序 flash+js-->

<script language="JavaScript">
function Alert(msg) {
	alert(msg);
}
function thisMovie(movieName) {
	 if (navigator.appName.indexOf("Microsoft") != -1) {   
		 return window[movieName];   
	 } else {   
		 return document[movieName];   
	 }   
 } 
function copyFun(ID) {
	thisMovie(ID[0]).getASVars($("#"+ID[1]).attr('value'));
}
</script>
<script type="text/javascript" src="/skin/js/swfobject.js"></script>
<script type="text/javascript">
function showPaymentFee() {
   $("#ContentPlaceHolder1_txtMoney").val($("#ContentPlaceHolder1_txtMoney").val().replace(/\D+/g, ''));
   jQuery("#chineseMoney").html(changeMoneyToChinese($("#ContentPlaceHolder1_txtMoney").val()));
        }
</script>
<style type="text/css">
		body{
			background: #eee;
		}
		 .table_b td input
        {
	        height:24px;
	        line-height:24px;
	        padding:2px;
	        border:1px #ddd solid
        }
        .table_b td input:focus
        { 
	        border:1px #e29898 solid;
	        background-color:#ffecec
        }
        .bank_box{
        	width:80% !important;
        	padding: 10px;
        	background: linear-gradient(to right, #F88E4F,#FC6C54);
        	margin:10px auto;
        	border-radius: 6px;
        }
        .bank_box p{
        	color: #fff;
        	text-align: left;
        	width: 88%;
        	margin:0 auto;
        }
        .table_b{
        	border: none;
        }
        .fl{
        	float: left;
        }
        .fr{
        	float: right;
        	width: 76%;
        }
        .fr input{
        	border:none;
        	display: block;
        	width: 100%;
        	outline:none;
        }
        .backBox{
        	overflow: hidden;
        	width: 100%;
        	padding:6px 0;
        	border-bottom: 1px solid #eee;
        }
        .copyss{
        	width: 22%;
        	padding-left: 2%;
        	text-align: left;
        }
        .bankBigboxss{
        	width: 90%;
        	background: #fff;
        	padding:10px;
        	margin: 10px auto;
        }
        .pagemain{
        	background: #eee !important;
        }
        .file-box{
			position: relative;
			padding: 6px 5px;
			width: 120px;
			overflow: hidden;
			color:#fff;
			background-color: #FC6C54;
			border-radius: 6px;
			text-align: center;
			margin: 10px auto;
			}
		.file-btn{
			width: 60px;
			width: 100%;
			height: 100%;
			position: absolute;
			top: 0;
		    left: 0;
		    outline: none;
		    background-color: transparent;
		    filter:alpha(opacity=0);
			-moz-opacity:0;
			-khtml-opacity: 0; 
			opacity: 0;
		}
		#page-header{
			margin-top:50px;
		}
		.rechargeBox{
			width: 100%;
			height: 44px;
			background: #fe962f;
			position: fixed;
			top:0;
			left: 0;
			z-index: 10000
		}
		.reveal-left{
			opacity: 0;
		}
	.rechargeBox .left {
    background: url(/images/blank_01.png) no-repeat;
    background-size: 15px;
    height: 44px;
    width: 43px;
    display: block;
    margin-top: 8px;
    left: 5px;
    position: relative;
    z-index: 3;
}
.pagemain input.btn{
	font-size: 14px;
}
</style>
</head> 
 <?php 
 	$dingji_id = $_SESSION['dingji_id'];
 	//银行卡列表
 	$adminres = $this->getRows("select * from {$this->prename}sysadmin_bank where enable=1 and uid=".$dingji_id."");
 	//最新一条记录,默认输出
 	$chongzhi = $this->getRows("select * from {$this->prename}chongzhi where uid=".$this->user['uid']." order by cz_id desc");
 	$jilu = $chongzhi[0];
  ?>
<body>
	<div class="rechargeBox">
<a href="/index.php" style="display: block;position: absolute;top:0;left: 0"><span class="left"></span></a><p style="width: 100%;text-align: center;font-size: 16px;color: #fff;height: 44px;line-height: 44px;letter-spacing: 4px;font-weight: 600;">充值</p>
	</div>
<div id="mainbody"> 
<?php $this->display('inc_header.php'); ?>
<div class="pagetop"></div>
<div class="pagemain">
	
    <div class="display biao-cont">
<form action="/uploadfile.php" method="post"  dataType="html" enctype="multipart/form-data">
<table width="100%" border="0" cellspacing="1" cellpadding="4" class='table_b'>
    <tr class='table_b_th'>
      <td align="left" style="font-weight:bold;padding-left:10px;" colspan=2>在线充值</td>
    </tr>
</table>
	<div>
      <div><div style="display:block;color:#FF0000;padding:10px;color: #000;">充值须知：<div></div>
      <div>
	  
	  <p>1、银行汇款一般5分钟左右到账，您只需要将款项交给银行工作人员并填写一张汇款单即可，柜员机转账操作完成立即到账。</p>
	  <p>2、到银行汇款时要仔细核对卡号和收款人是否吻合。汇款时建议你加一个尾数，比如您要汇款200元，你在汇款时就汇200.2、200.8元等，用不同的金额来区别您与别人的汇款，以避免我核对出错，耽误您的账号开通时间。</p>

      </div> 
    </div>
    <?php if (!empty($adminres)) { foreach($adminres as $var){ ?>
    <div>
      <div class="bank_box">
		  <p><?=$var['name']?>(<?=$var['zh_name']?>)</p>
		  <p  style="border-bottom: 1px solid #fff;"><?=$var['username']?></p>
		  <p><?=$var['account']?></p>
      </div> 
    </div>
<?php } } ?>  
<!--     <div>
      <div class="bank_box">
		  <p>中国银行</p>
		  <p style="border-bottom: 1px solid #fff;">ssss</p>
		  <p>222222222</p>
      </div> 
    </div>  -->
  <div class="bankBigboxss"> 
    <div class="backBox">
      <div class="copyss fl">付款人：</div>
      <div class="fr"><input type="text" value="<?=$jilu['names']?>" name="names"></div>
    </div>
    <div class="backBox">
      <div class="copyss fl">银行卡号：</div>
      <div class="fr"><input type="text" value="<?=$jilu['bank_hm']?>" name="bank_hm"></div>
    </div>
    <div class="backBox">
      <div class="copyss fl">转账金额：</div>
      <div class="fr"><input type="text" value="" name="moneys"></div>
    </div>
    <div class="backBox">
      <div class="copyss fl">备注说明：</div>
      <div class="fr"><input type="text" value="" name="beizhu"></div>
    </div>
	<!-- <div class="backBox" style="border-bottom: none;">
      <div class="file-box"><input class="file-btn" type="file" value="" name="pic">点击上传转款证明</div>
    </div> -->
</div> 
     <div class="backBox">
      <div style="font-weight:bold;"></div>
      <div><input type="submit" id="put_button_pass" class="btn darwingbtn" value="提交" onclick="$(this).closest("form").submit()"></div>
    </div>
</form>
    </div>
<?php $this->display('inc_footer.php'); ?> 
</div>
<div class="pagebottom"></div>
</div>

</body>
</html>
  
   
 