<!DOCTYPE html>
<html style="font-size: 13px;">
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>走势</title>
    <link rel="stylesheet" href="/skin/laofan/pure.min.css">
	<!--[if lte IE 8]>
		<link rel="stylesheet" href="/skin/laofan/grids-responsive-old-ie-min.css">
	<![endif]-->
	<!--[if gt IE 8]><!-->
		<link rel="stylesheet" href="/skin/laofan/grids-responsive-min.css">
	<!--<![endif]-->	
	<script src="/skin/js/jquery.min.js?ver=2.7"></script>
	<script src="http://apps.bdimg.com/libs/layer.m/1.5/layer.m.js"></script>
	<link rel="stylesheet" href="/skin/laofan/pclist.css">
	<link rel="stylesheet" href="/skin/laofan/zoushi.css">
  <style type="text/css">
    .right-content{
      display: none;
    }
  </style>
	<script type="text/javascript"> 
	var index ;
	$(document).ready(function(){
	
		//a 跳转
	   $('.z_a').click(function(){
		var u = $(this).attr('href');
	    $("#zoushimap").show();
	    $(".nei").hide();
		$("#zoushimap iframe").attr('src',u);
		return false;
	   })
	   //走势选择
	   $("#reveal-right").click(function(){
		  //信息框
		 index =  layer.open({
			content: $("#liebiao").html()
			,style: ' width:90%; height:60%; border: none; -webkit-animation-duration: .5s; animation-duration: .5s;'
		  });
		console.log(index);	
	   })
	});	
	function qu(id){
		var u = '/index.php/chartssc?type='+id+'&count=30';
	    $("#zoushimap").show();
	    $(".nei").hide();		
		$("#zoushimap iframe").attr('src',u);
		layer.close(index);
	}
 function qus(){
    var u = '/index.php/chartlucky?count=30';
      $("#zoushimap").show();
      $(".nei").hide();   
    $("#zoushimap iframe").attr('src',u);
    layer.close(index);
  }
	</script>
	<style type="text/css"> 
	.trend-box .trend-main .trend-lot-ul li{    width: 49%;}
	</style>
</head>
<body>
<div class="header">
    <div class="headerTop">
        <div class="ui-toolbar-left">
            <a id="reveal-left" href="/index.php">reveal</a>
        </div>
        <div class="ui-toolbar-right">
            <a id="reveal-right" href="javascript:void(0);">reveal</a>
        </div>		
        <h1 class="ui-toolbar-title"><span id="caizhogn"></span>走势图</h1>
    </div>
</div>
<div class="pure-g" id="zoushimap" style="display:none;margin-top: 40px;"> 
	<iframe src="" frameborder="0"  width="100%" height="600" scrolling="yes"></iframe>
</div>
<div  id="liebiao" style="display:none;"> 
	<?php 
		$sql="select id,type, title  from ssc_type  ";
		$all_types = $this->getRows($sql); 	
	?> 

	<div class="trend-tips"  >
	  <div class="trtip-tit"><i class="tr-icon"></i>选择彩种</div>
	  <ul>
      <li onclick="qus()">阿里分分彩</li>
   <!--   <a href="/index.php/chartlucky?count=30"><li>阿里分分彩</li></a> -->
       <?php
             $sql="select * from {$this->prename}type where enable=1 order by sort";
              $row=$this->getRows($sql);   
          foreach ($row as $key => $value) {
          ?>
              <li class="game_li_<?=$value['id']?>"  onclick="qu(<?=$value['id']?>);"><?=$value['title']?></li>
               <?php
          };
           ?>
	  </ul>
	</div>	
</div>
     <div class="pure-g wrap  nei" >
<div id="_iframe_divs" class="_iframe_divs_">
    <div id="_games_2">
      <div class="trend-box">
        <div class="trend-title">
          <h2>时时彩</h2>
        </div>
        <div class="trend-main">
          <ul class="trend-lot-ul">
            <li>
              <div class="lottery-logo lottery-logo-big"> <a class="z_a" href='/index.php/chartlucky?count=30'> <img src="/skin/main/img/51.png" style="width: 168px;" alt=""> </a>
                <div class="lottery-title">
                  <h3> 阿里分分彩 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a class="z_a" href='/index.php/chartlucky?count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
              <div class="lottery-logo lottery-logo-big"> <a class="z_a" href='/index.php/chartssc?type=1&count=30'> <img src="/skin/main/img/5.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 重庆时时彩 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a class="z_a" href='/index.php/chartssc?type=1&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
              <div class="lottery-logo lottery-logo-big"> <a class="z_a" href='/index.php/chartssc?type=3&count=30'> <img src="/skin/main/img/7.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 天津时时彩 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a class="z_a" href='/index.php/chartssc?type=3&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
             <li>
              <div class="lottery-logo lottery-logo-big"> <a class="z_a" href='/index.php/chartssc?type=12&count=30'> <img src="/images/index/c22.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 新疆时时彩 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a class="z_a" href='/index.php/chartssc?type=12&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
               </ul>
        </div>
      </div>
      <div class="trend-box">
        <div class="trend-title">
          <h2>十一选5</h2>
        </div>
        <div class="trend-main">
          <ul class="trend-lot-ul">
            <li>
              <div class="lottery-logo lottery-logo-big"> <a class="z_a" href='/index.php/chartk3?type=7&count=30'> <img src="/skin/laofan/images/12.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 山东11选5 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a class="z_a" href='/index.php/chartk3?type=7&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
              <div class="lottery-logo lottery-logo-big"> <a class="z_a" href='/index.php/chartk3?type=6&count=30'> <img src="/skin/laofan/images/15.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 广东11选5 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a class="z_a" href='/index.php/chartk3?type=6&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
              <div class="lottery-logo lottery-logo-big"> <a class="z_a" href='/index.php/chartk3?type=16&count=30'> <img src="/skin/laofan/images/15.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 江西11选5 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a class="z_a" href='/index.php/chartk3?type=16&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>     
          </ul>
        </div>
      </div> 
      <div class="trend-box">
        <div class="trend-title">
          <h2>低频彩</h2>
        </div>
        <div class="trend-main">
          <ul class="trend-lot-ul">
           <!--  <li>
              <div class="lottery-logo lottery-logo-big"> <a class="z_a" href='/index.php/chartk3?type=18&count=30'> <img src="/images/index/c1.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 重庆幸运农场 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a class="z_a" href='/index.php/chartk3?type=18&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li> -->
            <li>
              <div class="lottery-logo lottery-logo-big"> <a class="z_a" href='/index.php/chartk3?type=20&count=30'> <img src="/skin/main/img/9.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 北京PK拾 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a class="z_a" href='/index.php/chartk3?type=20&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li> 
            <li>
              <div class="lottery-logo lottery-logo-big"> <a class="z_a" href='/index.php/chartk3?type=30&count=30'> <img src="/skin/main/img/18.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 六合彩 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a class="z_a" href='/index.php/chartk3?type=30&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li> 
           <!--  <li>
              <div class="lottery-logo lottery-logo-big"> <a class="z_a" href='/index.php/chartssc?type=27&count=30'> <img src="/skin/laofan/images/51.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 三分时时彩 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a class="z_a" href='/index.php/chartssc?type=27&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
              <div class="lottery-logo lottery-logo-big"> <a class="z_a" href='/index.php/chartssc?type=1&count=30'> <img src="/skin/laofan/images/5.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 重庆时时彩 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a class="z_a" href='/index.php/chartssc?type=1&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
              <div class="lottery-logo lottery-logo-big"> <a class="z_a" href='/index.php/chartpk?type=20&count=30'> <img src="/skin/laofan/images/9.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 北京PK拾 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a class="z_a" href='/index.php/chartpk?type=20&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
              <div class="lottery-logo lottery-logo-big"> <a class="z_a" href='/index.php/chartpk?type=12&count=30'> <img src="/skin/laofan/images/7.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 新疆时时彩 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a class="z_a" href='/index.php/chartpk?type=12&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
              <div class="lottery-logo lottery-logo-big"> <a class="z_a" href='/index.php/chartpk?type=14&count=30'> <img src="/skin/laofan/images/7.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 五分时时彩 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a class="z_a" href='/index.php/chartpk?type=14&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
              <div class="lottery-logo lottery-logo-big"> <a class="z_a" href='/index.php/chartpk?type=26&count=30'> <img src="/skin/laofan/images/7.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 两分时时彩 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a class="z_a" href='/index.php/chartpk?type=26&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
              <div class="lottery-logo lottery-logo-big"> <a class="z_a" href='/index.php/chartpk?type=5&count=30'> <img src="/skin/laofan/images/7.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 急速分分彩 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a class="z_a" href='/index.php/chartpk?type=5&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
              <div class="lottery-logo lottery-logo-big"> <a class="z_a" href='/index.php/chartpk?type=18&count=30'> <img src="/images/index/10fens.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 重庆幸运农场 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a class="z_a" href='/index.php/chartpk?type=18&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>	
            <li>
              <div class="lottery-logo lottery-logo-big"> <a class="z_a" href='/index.php/chartpk?type=30&count=30'> <img src="/images/index/p3s.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 香港六合彩 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a class="z_a" href='/index.php/chartpk?type=30&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>			
          </ul>
        </div>
      </div>
      <div class="trend-box">
        <div class="trend-title">
          <h2>快三</h2>
        </div>
        <div class="trend-main">
          <ul class="trend-lot-ul">
            <li>
              <div class="lottery-logo lottery-logo-big"> <a class="z_a" href='/index.php/chartk3?type=25&count=30'> <img src="/skin/laofan/images/10.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 江苏快三 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a class="z_a" href='/index.php/chartk3?type=25&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
              <div class="lottery-logo lottery-logo-big"> <a class="z_a" href='/index.php/chartk3?type=52&count=30'> <img src="/skin/laofan/images/17.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 广西快三 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a class="z_a" href='/index.php/chartk3?type=52&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
              <div class="lottery-logo lottery-logo-big"> <a class="z_a" href='/index.php/chartk3?type=50&count=30'> <img src="/skin/laofan/images/11.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 湖北快三 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a class="z_a" href='/index.php/chartk3?type=50&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
          </ul>
        </div>
      </div>
      <div class="trend-box">
        <div class="trend-title">
          <h2>十一选5</h2>
        </div>
        <div class="trend-main">
          <ul class="trend-lot-ul">
            <li>
              <div class="lottery-logo lottery-logo-big"> <a class="z_a" href='/index.php/chartk3?type=7&count=30'> <img src="/skin/laofan/images/12.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 山东11选5 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a class="z_a" href='/index.php/chartk3?type=7&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
              <div class="lottery-logo lottery-logo-big"> <a class="z_a" href='/index.php/chartk3?type=6&count=30'> <img src="/skin/laofan/images/15.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 广东11选5 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a class="z_a" href='/index.php/chartk3?type=6&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
              <div class="lottery-logo lottery-logo-big"> <a class="z_a" href='/index.php/chartk3?type=16&count=30'> <img src="/skin/laofan/images/15.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 江西多乐彩 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a class="z_a" href='/index.php/chartk3?type=16&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>			
          </ul>
        </div>
      </div>
      <div class="trend-box">
        <div class="trend-title">
          <h2>低频彩</h2>
        </div>
        <div class="trend-main">
          <ul class="trend-lot-ul">
            <li>
              <div class="lottery-logo lottery-logo-big"> <a class="z_a" href='/index.php/chartk3?type=9&count=30'> <img src="/skin/laofan/images/1.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 福彩3D </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a class="z_a" href='/index.php/chartk3?type=9&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li>
            <li>
              <div class="lottery-logo lottery-logo-big"> <a class="z_a" href='/index.php/chartk3?type=10&count=30'> <img src="/skin/laofan/images/2.png" alt=""> </a>
                <div class="lottery-title">
                  <h3> 排列三 </h3>
                </div>
              </div>
              <div class="right-content right-content-big">
                <div class="row-content">
                  <div class="trend-content"> <span class="trend-btn"><a class="z_a" href='/index.php/chartk3?type=10&count=30'>基本走势</a></span> </div>
                  <div class="clear"></div>
                </div>
                <div> <a name="dltAnchor"> </a> </div>
              </div>
              <div class="clear"></div>
            </li> -->
          </ul>
        </div>
      </div>
    </div>
    <div class="div_iframe" name="iframe_div"></div>
  </div>		  	 	
	 </div>	 
 
 	 
	 
</body>
</html>
