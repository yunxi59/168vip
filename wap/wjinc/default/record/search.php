<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<?php $this->display('inc_skin.php', 0 , '游戏记录'); ?>
<script type="text/javascript">
$(function(){
	
	$('.search form input[name=betId]')
	.focus(function(){
		if(this.value=='输入单号') this.value='';
	})
	.blur(function(){
		if(this.value=='') this.value='输入单号';
	})
	.keypress(function(e){
		if(e.keyCode==13) $(this).closest('form').submit();
	});
	
	$('.chazhao').click(function(){
		$(this).closest('form').submit();
	});
	
	$('.bottompage a[href]').live('click', function(){
		$('.biao-cont').load($(this).attr('href'));
		return false;
	});

});
function recordSearch(err, data){
	if(err){
		alert(err);
	}else{
		$('.biao-cont').html(data);
	}
}
function recodeRefresh(){
	$('.biao-cont').load(
		$('.bottompage .pagecurrent').attr('href')
	);
}

function deleteBet(err, code){
	if(err){
		alert(err);
	}else{
		recodeRefresh();
	}
}
</script>
</head> 
 
<body>
	<style type="text/css">
		.rechargeBox {
		    width: 100%;
		    height: 44px;
		    background: #fe962f;
		    position: fixed;
		    top: 0;
		    left: 0;
		    z-index: 10000;
		}
		.rechargeBox .left {
    background: url(/images/blank_01.png) no-repeat;
    background-size: 15px;
    height: 44px;
    width: 43px;
    display: block;
    margin-top: 8px;
    left: 5px;
    position: relative;
    z-index: 3;
}
.select_all{
	width:29%;
	border:none;
     background: #fe962f;
    color: #fff;
    height: 40px;
    line-height: 40px;
    font-size: 16px;
    font-weight: 600;
    padding-left: 10px;
}
.select_all option{
	color: #000;
	background: #fff;
}
.betId{
	display: block;
	width: 92% !important;
	height: 30px !important;
	line-height: 30px !important;
	margin-top:20px;
	padding-left:10px;
	border-radius: 0 !important; 
}
.zhis{
	display: inline-block;
	width:30px;
	text-align: center;
	position: relative;
	left: -4px;
}
</style>
<div id="mainbody"> 
	<div class="rechargeBox">
<a href="/index.php" style="display: block;position: absolute;top:0;left: 0"><span class="left"></span></a><p style="width: 100%;text-align: center;font-size: 16px;color: #fff;height: 44px;line-height: 44px;letter-spacing: 4px;font-weight: 600;"></p>
	</div>
<?php $this->display('inc_header.php'); ?>
<div class="pagetop"></div>
<div class="pagemain">
	<div class="search">
    	<form action="/index.php/record/searchGameRecord/<?=$this->userType?>" dataType="html" call="recordSearch" target="ajax">
  		<select name="type" class="select_all">
        	<option value="0" <?=$this->iff($_REQUEST['type']=='', 'selected="selected"')?>>全部彩种</option>
           <?php
             $sql="select * from {$this->prename}type where enable=1 order by sort";
              $row=$this->getRows($sql);   
		      foreach ($row as $key => $value) {
		      ?>
                <option value="<?=$value['id']?>"><?=$this->iff($value['shortName'], $value['shortName'], $value['title'])?></option>
               <?php
		      };
           ?>
        </select>
       <select name="state"  class="select_all">
            <option value="0" selected>所有状态</option>
            <option value="1">已派奖</option>
            <option value="2">未中奖</option>
            <option value="3">未开奖</option>
            <option value="4">追号</option>
            <option value="5">撤单</option>
        </select>
     
       <select name="mode"  class="select_all">
            <option value="" selected>全部模式</option>
            <option value="2.00">元</option>
            <option value="0.20">角</option>
            <option value="0.02">分</option>
        </select>
       <input  height="20" value="输入单号" name="betId" class="betId" />
      <input type="text"  name="fromTime" class="datainput"  value="<?=$this->iff($_REQUEST['fromTime'],$_REQUEST['fromTime'],date('Y-m-d H:i:s',$GLOBALS['fromTime']))?>"/><span class="zhis">至</span><input style="margin-left: 4px" type="text" name="toTime"  class="datainput" value="<?=$this->iff($_REQUEST['toTime'],$_REQUEST['toTime'],date('Y-m-d H:i:s',$GLOBALS['toTime']))?>"/>
         
      <input type="button" value="查 询" class="btn chazhao">
  </form> 
    </div>
    <div class="display biao-cont">
    	<!--下注列表-->
        <?php $this->display('record/search-list.php'); ?>
        <!--下注列表 end -->
    </div>
<?php $this->display('inc_footer.php'); ?> 
</div>
<div class="pagebottom"></div>
</div>

</body>
</html>
  
   
 