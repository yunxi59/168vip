<html style="font-size: 12px;">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="format-detection" content="telephone=no">
        <title>玩法说明</title>
        <link rel="stylesheet" href="/skin/laofan/newlist.css">
        <link rel="stylesheet" type="text/css" href="/skin/css/play.css">
    </head>
    <body ontouchstart="">
	<div class="header">
    <div class="headerTop">
<!--     	<a href="/index.php" style="display: block;position: absolute;top:3px;left: 0"><span class="left"></span></a> -->
        <div class="ui-toolbar-left">
            <a id="reveal-left" href="/index.php">reveal</a>
        </div>
        <h1 class="ui-toolbar-title" style="font-weight:normal">玩法说明</h1>
    </div>
    </div>
    <div class="content">
    	<div class="headinfo">
    		<span class="ssc"><img src="/skin/main/img/9.png" width="40px;"></span>
    		<span class="sscword">北京PK拾</span>
    	</div>
    	<div class="word">
    		<div class="palys">
    		<div class="play1">
    			<span class="yuan">
    				一
    			</span>
    			<span class="word1">
    				玩法类型及承销
    			</span>
    		</div>
    		<div class="word2">
    		1.根据财政部颁发的《彩票发行与销售暂行管理规定》（财综 [2002]13号）及中国福利彩票发行中心颁发的《中国福利彩票发行规则》等管理规定，结合计算机网络技术，制定本细则。
    	    </div>
            <br>
            <div class="word2">
            2.中国福利彩票"北京PK拾" 游戏，简称"PK拾"，由中国福利彩票发行管理中心（以下简称“中福彩中心”）统一发行，由北京市福利彩票发行中心承销。
            </div>
            <br>
            <div class="word2">
            3."北京PK拾"采用计算机网络系统发行销售，定期开奖。
            </div>
            <br>
            <div class="word2">
            4."北京PK拾"实行自愿购买，凡购买者均被视为同意遵守本细则。
            </div>
    		<div class="space"></div>

    		<div class="palys">
    		<div class="play1">
    			<span class="yuan">
    				二
    			</span>
    			<span class="word1">
    				开奖与购买方式
    			</span>
    		</div>
    		<div class="word2">
    		1."北京PK拾"的发行权属中国福利彩票发行管理中心，主要包括：中国福利彩票名称及标识的专有权，发行额度的分配权和调控权、各项资金比例的确定和调整权，投注单和彩票专用纸的监制权等。
    	    </div>
            <br>
    		<div class="word2">
    		2.北京市福利彩票发行中心通过电脑彩票销售系统发行"PK拾" 游戏，接受中彩中心的监控。
    	    </div>
            <br>
    	    <div  class="word2">
    	    3.北京市福利彩票发行中心在承销区域内设置投注站，并核发销售许可证。投注站由北京市福利彩票发行中心管理，展示销售许可证。
    	    </div>
            <br>
    	    <div  class="word2">
    	    4.本站北京PK拾游戏每天进行179期，开奖时间为09:07至23:57，每隔5分钟开奖一次，销售期号以开奖日界定，按日历年度编排。	
    	    </div>
            <br>
            <div class="word2">
            5.购买者可在对其选定的投注号码选择返点投注，返点越高则投注赔率越低。购买者也可进行追号投注，追号是指将一注或一组号码进行两期或两期以上的投注。追号可分为连续追号和间隔追号，连续追号指追号的期数是连续的，间隔追号指追号的期数不连续。   
            </div>
            <br>
            <div class="word2">
            6.购买者可选择机选号码投注、自选号码投注。机选号码投注是指由投注机随机产生投注号码进行投注，自选号码投注是指将购买者选定的号码输入投注机进行投注。  
            </div>
            <br>
            <div class="word2">
            7.如果用户投注成功后，若因销售终端故障、通讯线路故障和投注站信用额度受限等原因造成当期无法开奖的，应退还购买者投注金额。  
            </div>
    		</div>
    		<div class="space"></div>

    		<div class="palys">
    		<div class="play1">
    			<span class="yuan">
    				三
    			</span>
    			<span class="word1">
    				兑奖
    			</span>
    		</div>
    		<div class="word2">
    		1.根据投注者所选玩法及投注号码与中奖号码按位置顺序和对应的数字相符的个数多少确定相应的中奖奖级。
    	    </div>
            <div class="word2">
            2.无论大小奖均返还至用户在本站的账户中，一旦用户中奖，系统将自动返还中奖金额。可继续投注或提款，永无弃奖。
            </div>
    		</div>
    		<div class="space"></div>

    		<div class="palys">
    		<div class="play1">
    			<span class="yuan">
    				四
    			</span>
    			<span class="word1">
    				玩法说明
    			</span>
    		</div>
    		<div class="word2">
    		1.北京PK拾采用按位置顺序选择投注号码、固定设奖的玩法。投注者首先根据不同玩法确定选择最少1个、最多10个位置进行投注，然后按照从左到右、从1号到10号投注位置的顺序，从1号位置开始，在每个位置上从编号为1至10的号码中任意选择1个号码，且不能与其它位置上已经选择的号码相同，按照从1 到10号位置的顺序排序组成一组号码，每一组选定的按位置排序的号码称为一注投注号码。
    	    </div>
            <br>
            <div class="word2">
            2.北京pk10冠亚和值玩法介绍：开奖号码第一个数+第二个数的总和，3-11为冠亚和小，12-19为冠亚和大，和值为单数叫冠亚和单，和值为双数叫冠亚和双。
            </div>
    	    <div class="balls" style="display: inline-block;">
    	    <div>
    	    	<span class="ball">10</span>
    	    	<span class="num">冠军</span>
    	    </div>
    	    <div>
    	    	<span class="ball">07</span>
    	    	<span class="num">亚军</span>
    	    </div>
    	    <div>
    	    	<span class="ball">05</span>
    	    	<span class="num">季军</span>
    	    </div>
    	    <div>
    	    	<span class="ball">02</span>
    	    	<span class="num">第四</span>
    	    </div>
            <div>
                <span class="ball">04</span>
                <span class="num">第五</span>
            </div>
    	    <div>
    	    	<span class="ball">06</span>
    	    	<span class="num">第六</span>
    	    </div>
            <div>
                <span class="ball">09</span>
                <span class="num">第七</span>
            </div>
            <div>
                <span class="ball">08</span>
                <span class="num">第八</span>
            </div>
            <div>
                <span class="ball">03</span>
                <span class="num">第九</span>
            </div>
            <div>
                <span class="ball">01</span>
                <span class="num">第十</span>
            </div>
    	    </div>
    		</div>
    		<div class="space" style="margin-top: 70px;"></div>

    		<div class="palys">
    		<div class="play1"> 
    			<span class="yuan">
    				五
    			</span>
    			<span class="word1">
    				设奖及中奖
    			</span>
    		</div>
            <br>
    		<div class="word2" style="margin-top: -15px;">
    		1."北京PK拾" 采用专用的摇奖计算机系统进行摇奖，中奖号码从数字1 到10 总共10 个数字中随机产生。每次摇奖时按照从左至右、从1 号位置到10 号位置的顺序进行，第一个摇出的数字对应1 号位置，第二个摇出的数字对应2 号位置，依次类推，直到摇出对应10 号位置的第十个数字为止，这10 个位置和对应的数字组成当期中奖号码
    	    </div>
            <br>
            <div class="word2">
            2."北京PK拾"按不同投注方式设奖，均为固定奖。奖金规定如下：
            </div>
    	    <div class="tables">
    	    	<table>
    	    		<tr>
    	    			<th colspan="2">玩法</th>
    	    			<th>开奖号码示例</th>
    	    			<th>投注号码示例</th>
    	    			<th>中奖规则</th>
    	    			<th>单注最高赔率</th>
    	    		</tr>
    	    		<tr>
                        <td>前一</td> 
                        <td>直选复式</td> 
                        <td>05 06...</td>  
                        <td>05</td>    
                        <td>选号与开奖号按位猜中1位即中奖</td> 
                        <td>9.8</td>
                    </tr>
                    <tr>
                        <td rowspan="2">前二</td>
                        <td>直选复式</td>
                        <td rowspan="2">05 06 07...</td>
                        <td rowspan="2">05 06</td>
                        <td rowspan="2">选号与开奖号按位猜中2位即中奖</td>
                        <td rowspan="2">88.2</td>
                    </tr>
                    <tr>
                        <td>直选单式</td>
                    </tr>
                    <tr>
                        <td rowspan="2">前三</td>
                        <td>直选复式</td>
                        <td rowspan="2">05 06 07 08...</td>
                        <td rowspan="2">05 06 07</td>
                        <td rowspan="2">选号与开奖号按位猜中3位即中奖</td>
                        <td rowspan="2">705.6</td>
                    </tr>
                    <tr>
                        <td>直选单式</td>
                    </tr>
                    <tr>
                        <td colspan="2">定位胆</td> 
                        <td>05 06...</td> 
                        <td>冠军：05</td>  
                        <td>选号与开奖号按位猜中1位或1位以上即中奖</td>    
                        <td>9.8</td> 
                    </tr>
    	    	</table>
    	    </div>
            <br>
            <div class="word2">
            3.北京PK拾实行自愿购买，凡购买者均被视为同意遵守本细则。
            </div>
    		</div>
    		<div class="space"></div>

    		<div class="palys">
    		<div class="play1">
    			<span class="yuan">
    				六
    			</span>
    			<span class="word1">
    				投注方式
    			</span>
    		</div>
    	    <div class="word2">
    		1.前一：从第一名中至少选择1个号码组成一注，选号和开奖号码猜对一位即中奖，单注奖金最高9.8元。
    	    </div>
            <div class="tables" style="margin-top: -10px;margin-left:40px;" >
                <table>
                    <tr>
                        <th rowspan="2">例：</th>
                        <td>开奖：01 02 03 04 05 06 07 08 09 10</td>
                    </tr>
                    <tr>
                        <th style="height: 30px;">选号：冠军01</th>
                    </tr>
                </table>
            </div>
            <br>
            <div class="word2">
            2.前二直选：从第一名、第二名中各至少选择1个号码组成一注，选号和开奖号码猜对2位即中奖，单注奖金最高88.2元。
            </div>
            <div class="tables" style="margin-top: -10px;margin-left:40px;" >
                <table>
                    <tr>
                        <th rowspan="2">例：</th>
                        <td>开奖：01 02 03 04 05 06 07 08 09 10</td>
                    </tr>
                    <tr>
                        <th style="height: 30px;">选号：冠军01，亚军02</th>
                    </tr>
                </table>
            </div>
    	    <br>
            <div class="word2">
            3.前三直选：从第一名、第二名、第三名中至少选择1个号码组成一注，选号和开奖号码猜对3位即中奖，单注奖金最高705.6元。
            </div>
            <div class="tables" style="margin-top: -10px;margin-left:40px;" >
                <table>
                    <tr>
                        <th rowspan="2">例：</th>
                        <td>开奖：01 02 03 04 05 06 07 08 09 10</td>
                    </tr>
                    <tr>
                        <th style="height: 30px;">选号：冠军01，亚军02，季军03</th>
                    </tr>
                </table>
            </div>
            <br>
            <div class="word2">
            4.定位胆：从第一名到第十名任意位置上选择1个或1个以上号码，所选号码与相同位置上的开奖号码一致，即位中奖，单注奖金最高9.8元。
            </div>
            <div class="tables" style="margin-top: -10px;margin-left:40px;" >
                <table>
                    <tr>
                        <th rowspan="2">例：</th>
                        <td>开奖：01 02 03 04 05 06 07 08 09 10</td>
                    </tr>
                    <tr>
                        <th style="height: 30px;">选号：冠军01</th>
                    </tr>
                </table>
            </div>
    	    </div>
    		<div class="space"></div>
    	</div>
    </div>

    </body>
</html> 
	  