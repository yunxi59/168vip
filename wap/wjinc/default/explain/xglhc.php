<html style="font-size: 12px;">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="format-detection" content="telephone=no">
        <title>玩法说明</title>
        <link rel="stylesheet" href="/skin/laofan/newlist.css">
        <link rel="stylesheet" type="text/css" href="/skin/css/play.css">
    </head>
    <body ontouchstart="">
	<div class="header">
    <div class="headerTop">
<!--     	<a href="/index.php" style="display: block;position: absolute;top:3px;left: 0"><span class="left"></span></a> -->
        <div class="ui-toolbar-left">
            <a id="reveal-left" href="/index.php">reveal</a>
        </div>
        <h1 class="ui-toolbar-title" style="font-weight:normal">玩法说明</h1>
    </div>
    </div>
    <div class="content">
    	<div class="headinfo">
    		<span class="ssc"><img src="/skin/main/img/18.png" width="40px;"></span>
    		<span class="sscword">香港六合彩</span>
    	</div>
    	<div class="word">
    		<div class="palys">
    		<div class="play1">
    			<span class="yuan">
    				一
    			</span>
    			<span class="word1">
    				重要声明
    			</span>
    		</div>
    		<div class="word2">
    		1.如果客户怀疑自己的资料被盗用，应立即通知本站，并更改详细数据，以前的使用者名称及密码将全部无效。
    	    </div>
            <br>
            <div class="word2">
            2.客户有责任确保自己的账户及登录资料的保密性。以使用者名称及密码进行的任何网上投注将被视为有效。
            </div>
            <br>
            <div class="word2">
            3.公布赔率时出现的任何打字错误或非故意人为失误，本公司保留改正错误和按正确赔率结算投注的权力。
            </div>
            <br>
            <div class="word2">
            4.每次登录时客户都应该核对自己的账户结余额。如对余额有任何疑问，请在第一时间内通知本站客服人员。
            </div>
            <br>
            <div class="word2">
            5.一旦投注被接受，则不得取消或修改。
            </div>
            <br>
            <div class="word2">
            6.所有号码赔率将不时浮动，派彩时的赔率将以确认投注时之赔率为准。
            </div>
            <br>
            <div class="word2">
            7.所有投注都必须在开奖前时间内进行否则投注无效。
            </div>
    		<div class="space"></div>

    		<div class="palys">
    		<div class="play1">
    			<span class="yuan">
    				二
    			</span>
    			<span class="word1">
    				开奖与购买方式
    			</span>
    		</div>
    		<div class="word2">
    		1.六合彩是从1至49个号码中选出七个为中奖号码的奖券，由香港赛马会的附属公司「香港马会奖券有限公司」经办 。六合彩每星期搅珠三次，通常于星期二、星期四及非赛马日之星期六或日晚上举行，并由电视台现场直播。
    	    </div>
            <br>
    		<div class="word2">
    		2.六合彩注项单位每注金额最低1元。选择投注项时，购买者可预设金额，然后对其选定的投注号码进行投注。
    	    </div>
            <br>
    	    <div  class="word2">
    	    3.如果用户投注成功后，若因销售终端故障、通讯线路故障和投注站信用额度受限等原因造成当期无法开奖的，应退还购买者投注金额。
    	    </div>
    		</div>
    		<div class="space"></div>

    		<div class="palys">
    		<div class="play1">
    			<span class="yuan">
    				三
    			</span>
    			<span class="word1">
    				兑奖
    			</span>
    		</div>
    		<div class="word2">
    		1.无论大小奖均返还至用户在本站的账户中，一旦用户中奖，系统将自动返还中奖金额。可继续投注或提款，永无弃奖。
    	    </div>
    		</div>
    		<div class="space"></div>

    		<div class="palys">
    		<div class="play1">
    			<span class="yuan">
    				四
    			</span>
    			<span class="word1">
    				六合彩玩法说明
    			</span>
    		</div>
    	    <div class="balls" style="width: 200px;">
    	    <div>
    	    	<span class="ball">30</span>
    	    	<span class="num">蛇</span>
    	    </div>
    	    <div>
    	    	<span class="ball">40</span>
    	    	<span class="num">羊</span>
    	    </div>
    	    <div>
    	    	<span class="ball">34</span>
    	    	<span class="num">牛</span>
    	    </div>
    	    <div>
    	    	<span class="ball">08</span>
    	    	<span class="num">兔</span>
    	    </div>
            <div>
                <span class="ball" style="background: #409EFF">25</span>
                <span class="num">狗</span>
            </div>
    	    <div>
    	    	<span class="ball" style="background: #409EFF">15</span>
    	    	<span class="num">猴</span>
    	    </div>
            <div>
                <span class="ball" style="background: none;color: black;font-size: 18px;font-weight: bolder;">+</span>
                <span class="num"></span>
            </div>
            <div>
                <span class="ball" style="background: #409EFF">42</span>
                <span class="num">蛇</span>
            </div>
    	    </div>
    		</div>
    		<div class="space" style="margin-top: 70px;"></div>

    		<div class="palys">
    		<div class="play1"> 
    			<span class="yuan">
    				五
    			</span>
    			<span class="word1">
    				特码
    			</span>
    		</div>
            <br>
            <div class="word2" style="margin-top: -15px;">
              <strong>1.特码</strong>  
            </div>
    		<div class="word2">
    		香港⑥合彩公司当期开出的最后一码为特码
    	    </div>
    	    <div class="tables" style="margin-top: -10px;margin-left:40px;" >
                <table>
                    <tr>
                        <th rowspan="2">例：</th>
                        <td>开奖号码：30 40 34 08 25 15 + 42</td>
                    </tr>
                    <tr>
                        <th style="height: 30px;">投注特码：42</th>
                    </tr>
                </table>
            </div>
            <br>
            <br>
            <div class="word2" style="margin-top: -15px;">
              <strong>2.特码大小</strong>  
            </div>
            <div class="tables" style="margin-top: -10px;margin-left:40px;" >
                <table>
                    <tr>
                        <th style="font-weight: normal;">特小：开出的特码，(01~24)小于或等于24<br>
                            <span class="color1">01</span> 
                            <span class="color1">02</span>
                            <span class="color2">03</span>
                            <span class="color2">04</span>
                            <span class="color3">05</span>
                            <span class="color3">06</span>
                            <span class="color1">07</span> 
                            <span class="color1">08</span>
                            <span class="color2">09</span>
                            <span class="color2">10</span>
                            <span class="color3">11</span>
                            <span class="color1">12</span> 
                            <span class="color1">13</span>
                            <span class="color2">14</span>
                            <span class="color2">15</span>
                            <span class="color3">16</span>
                            <span class="color3">17</span>
                            <span class="color1">18</span> 
                            <span class="color1">19</span>
                            <span class="color2">20</span>
                            <span class="color3">21</span>
                            <span class="color3">22</span>
                            <span class="color1">23</span> 
                            <span class="color1">24</span>
                        </th>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;">特大：开出的特码，(25~48)大于或等于25<br>
                            <span class="color2">25</span> 
                            <span class="color2">26</span>
                            <span class="color3">27</span>
                            <span class="color3">28</span>
                            <span class="color1">29</span>
                            <span class="color1">30</span>
                            <span class="color2">31</span> 
                            <span class="color3">32</span>
                            <span class="color3">33</span>
                            <span class="color1">34</span>
                            <span class="color1">35</span>
                            <span class="color2">36</span> 
                            <span class="color2">37</span>
                            <span class="color3">38</span>
                            <span class="color3">39</span>
                            <span class="color1">40</span>
                            <span class="color2">41</span>
                            <span class="color2">42</span> 
                            <span class="color3">43</span>
                            <span class="color3">44</span>
                            <span class="color1">45</span>
                            <span class="color1">46</span>
                            <span class="color2">47</span> 
                            <span class="color2">48</span>
                        </th>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;">和局：特码为49</th>
                    </tr>
                </table>
            </div>
            <br>
            <br>
            <div class="word2" style="margin-top: -15px;">
              <strong>3.特码单双</strong>  
            </div>
            <div class="tables" style="margin-top: -10px;margin-left:40px;" >
                <table>
                    <tr>
                        <th style="font-weight: normal;">特双：特码为双数<br>
                            <span class="color1">02</span> 
                            <span class="color2">04</span>
                            <span class="color3">06</span>
                            <span class="color1">08</span>
                            <span class="color2">10</span>
                            <span class="color1">12</span>
                            <span class="color2">14</span> 
                            <span class="color3">16</span>
                            <span class="color1">18</span>
                            <span class="color2">20</span>
                            <span class="color3">22</span>
                            <span class="color1">24</span> 
                            <span class="color2">26</span>
                            <span class="color3">28</span>
                            <span class="color1">30</span>
                            <span class="color3">32</span>
                            <span class="color1">34</span>
                            <span class="color2">36</span> 
                            <span class="color3">38</span>
                            <span class="color1">40</span>
                            <span class="color2">42</span>
                            <span class="color3">44</span>
                            <span class="color1">46</span> 
                            <span class="color2">48</span>
                        </th>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;">特单：特码为单数<br>
                            <span class="color1">01</span> 
                            <span class="color2">03</span>
                            <span class="color3">05</span>
                            <span class="color1">07</span>
                            <span class="color2">09</span>
                            <span class="color3">11</span>
                            <span class="color1">13</span> 
                            <span class="color2">15</span>
                            <span class="color3">17</span>
                            <span class="color1">19</span>
                            <span class="color3">21</span>
                            <span class="color1">23</span> 
                            <span class="color2">25</span>
                            <span class="color3">27</span>
                            <span class="color1">29</span>
                            <span class="color2">31</span>
                            <span class="color3">33</span>
                            <span class="color1">35</span> 
                            <span class="color2">37</span>
                            <span class="color3">39</span>
                            <span class="color2">41</span>
                            <span class="color3">43</span>
                            <span class="color1">45</span> 
                            <span class="color2">47</span>
                        </th>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;">和局：特码为49</th>
                    </tr>
                </table>
            </div>
            <br>
            <br>
            <div class="word2" style="margin-top: -15px;">
              <strong>4.特码合数单双</strong>  
            </div>
            <div class="tables" style="margin-top: -10px;margin-left:40px;" >
                <table>
                    <tr>
                        <th style="font-weight: normal;">特合双：指开出的特码，个位加上十位之和为‘双数’<br>
                            <span>0、2、4、6、8、10、12</span> 
                        </th>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;">特合单：指开出的特码，个位加上十位之和为‘单数’<br>
                            <span>1、3、5、7、9、11、13</span> 
                        </th>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;">和局：特码为49</th>
                    </tr>
                </table>
            </div>
            <br>
            <br>
            <div class="word2" style="margin-top: -15px;">
              <strong>5.特码尾数大小</strong>  
            </div>
            <div class="tables" style="margin-top: -10px;margin-left:40px;" >
                <table>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">特尾大</th>
                        <td>5尾~9尾为大，如：05、18、19</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">特尾小</th>
                        <td>0尾~4尾为小，如：01、32、44</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">和局</th>
                        <td>特码为49</td>
                    </tr>
                </table>
            </div>
            <br>
            <br>
            <div class="word2" style="margin-top: -15px;">
              <strong>6.特码半特</strong>  
            </div>
            <div class="word2">
            以特码大小与特码单双游戏为一个投注组合；当期特码开出符合投注组合，即视为中奖；若当期特码开出49号，其余情形视为不中奖。    
            </div>
            <div class="tables" style="margin-top: -10px;margin-left:40px;" >
                <table>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">大单</th>
                        <td>25、27、29、31、33、35、37、39，41、43、45、47</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">大双</th>
                        <td>26、28、30、32、34、36、38、40，42、44、46、48</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">小单</th>
                        <td>01、03、05、07、09、11、13、15，17、19、21、23</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">小双</th>
                        <td>02、04、06、08、10、12、14、16，18、20、22、24</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">和局</th>
                        <td>特码为49时。</td>
                    </tr>
                </table>
            </div>
            <br>
            <br>
            <div class="word2" style="margin-top: -15px;">
              <strong>7.特码合数大小</strong>  
            </div>
            <div class="word2">
            以特码个位和十位数字之和值来判断胜负，和值大于6为合数大，如07、17、26、36、44；和值小于7为合数小，如01、12、23、32、42；开出49则视为和局。    
            </div>
            <br>
            <br>
            <div class="word2" style="margin-top: -15px;">
              <strong>8.半波</strong>  
            </div>
            <div class="word2">
            以特码色波和特单，特双，特大，特小为一个投注组合，当期特码开出符合投注组合，即视为中奖； 若当期特码开出49号，则视为和局；其余情形视为不中奖。    
            </div>
            <br>
            <br>
            <div class="word2" style="margin-top: -15px;">
              <strong>9.半半波</strong>  
            </div>
            <div class="word2">
            以特码色波和特单双及特大小等游戏为一个投注组合，当期特码开出符合投注组合，即视为中奖； 若当期特码开出49号，则视为和局；其余情形视为不中奖
            </div>
            <br>
            <br>
            <div class="word2" style="margin-top: -15px;">
              <strong>10.特码色波</strong>  
            </div>
            <div class="word2">
            香港⑥合彩49个号码球分别有红、蓝、绿三种颜色，以特码开出的颜色和投注的颜色相同视为中奖，颜色代表如下:    
            </div>
            <div class="tables" style="margin-top: -10px;margin-left:40px;" >
                <table>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">红波</th>
                        <td>01 ,02 ,07 ,08 ,12 ,13 ,18 ,19 ,23 ,24 ,29 ,30 ,34 ,35 ,40 ,45 ,46</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">蓝波</th>
                        <td>03 ,04 ,09 ,10 ,14 ,15 ,20 ,25 ,26 ,31 ,36 ,37 ,41 ,42 ,47 ,48</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">绿波</th>
                        <td>05 ,06 ,11 ,16 ,17 ,21 ,22 ,27 ,28 ,32 ,33 ,38 ,39 ,43 ,44 ,49</td>
                    </tr>
                </table>
            </div>
            <br>
            <br>
            <div class="word2" style="margin-top: -15px;">
              <strong>11.特码头数</strong>  
            </div>
            <div class="word2">
            特码头数：是指特码属头数的号码：    
            </div>
            <div class="tables" style="margin-top: -10px;margin-left:40px;" >
                <table>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">"0"头</th>
                        <td>01、02、03、04、05、06、07、08、09</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">"1"头</th>
                        <td>10、11、12、13、14、15、16、17、18、19</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">"2"头</th>
                        <td>20、21、22、23、24、25、26、27、28、29</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">"3"头</th>
                        <td>30、31、32、33、34、35、36、37、38、39</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">"4"头</th>
                        <td>40、41、42、43、44、45、46、47、48、49</td>
                    </tr>
                </table>
            </div>
            <br>
            <div class="word2">
                <span class="example">例如</span> 开奖结果特别号码为21则2头为中奖，其他头数都不中奖。
            </div>
            <br>
            <br>
            <div class="word2" style="margin-top: -15px;">
              <strong>12.特码尾数</strong>  
            </div>
            <div class="word2">
            特码尾数：是指特码属尾数的号码。    
            </div>
            <div class="tables" style="margin-top: -10px;margin-left:40px;" >
                <table>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">"0"尾</th>
                        <td>10、20、30、40</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">"1"尾</th>
                        <td>01、11、21、31、41</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">"2"尾</th>
                        <td>02、12、22、32、42</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">"3"尾</th>
                        <td>03、13、23、33、43</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">"4"尾</th>
                        <td>04、14、24、34、44</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">"5"尾</th>
                        <td>05、15、25、35、45</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">"6"尾</th>
                        <td>06、16、26、36、46</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">"7"尾</th>
                        <td>07、17、27、37、47</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">"8"尾</th>
                        <td>08、18、28、38、48</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">"9"尾</th>
                        <td>09、19、29、39、49</td>
                    </tr>
                </table>
            </div>
            <br>
            <div class="word2">
                <span class="example">例如</span> 开奖结果特别号码为21则1尾数为中奖，其他尾数都不中奖。
            </div>
    		</div>
    		<div class="space"></div>

    		<div class="palys">
    		<div class="play1">
    			<span class="yuan">
    				六
    			</span>
    			<span class="word1">
    				正码
    			</span>
    		</div>
    	    <div class="word2">
    		香港⑥合彩公司每期开出的前面六个号码为正码，下注号码如在六个正码号码里中奖。
    	    </div>
            <div class="tables" style="margin-top: -10px;margin-left:40px;" >
                <table>
                    <tr>
                        <th rowspan="2">例：</th>
                        <td>开奖号码：30 40 34 08 25 15 + 42</td>
                    </tr>
                    <tr>
                        <th style="height: 30px;">投注正码：30 40 34 08 25 15</th>
                    </tr>
                </table>
            </div>
    	    <br>
            <div class="word2" style="margin-left: 50px;">
                <strong>1.总和大小</strong>
            </div>
            <div class="tables" style="margin-top: -10px;margin-left:50px;">
                <table>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">总和大</th>
                        <td>以七个开奖号码的分数总和大于或等于175。</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">总和小</th>
                        <td>以有七个开奖号码的分数总和小于或等于174。</td>
                    </tr>
                </table>
            </div>    
            <br>
            <div class="word2" style="margin-left: 50px;">
                <strong>2.总和单双</strong>
            </div>
            <div class="tables" style="margin-top: -10px;margin-left:50px;">
                <table>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">总和单</th>
                        <td>以七个开奖号码的分数总和是‘单数’，如分数总和是133、197。</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">总和双</th>
                        <td>以七个开奖号码的分数总和是‘双数’，如分数总和是120、188。</td>
                    </tr>
                </table>
            </div>    
            <br>
            <div class="word2" style="margin-left: 50px;">
                <strong>3.正码特</strong>
            </div>
            <div class="word2" style="margin-left: 50px;">
             当期开奖的前面6个号码，按开奖出来的先后顺序依次分为：正1特、正2特、正3特、正4特、正5特、正6特。其下注的正码特号与现场摇珠开出之正码其开奖顺序及开奖号码相同，视为中奖。
            </div>
            <div class="word2" style="margin-left: 50px;">
                <span class="example">例如</span> 19,47,22,12,40,36+X 投注，正1特-19、正2特-47、正3特-22、正4特-12、正5特-40、正6特-36 当中的任意一项都中奖，投注正码特，其它号码如，正1特-47、正4特-36 不中奖。
            </div>
            <br>
            <div class="word2" style="margin-left: 50px;">
                <strong>4.正码1-6</strong>
            </div>
            <div class="word2" style="margin-left: 50px;">
            香港⑥合彩公司当期开出之前6个号码叫正码。第一时间出来的叫正码1，依次为正码2、正码3┅┅ 正码6(并不以号码大小排序)。正码1、正码2、正码3、正码4、正码5、正码6的大小单双合单双和特别号码的大小单双规则相同，如正码1为31，则正码1为大，为单，为合双，为合小；正码2为08，则正码2为小，为双，为合双，为合大；号码为49则为和。假如投注组合符合中奖结果，视为中奖。正码1-6色波下注开奖之球色与下注之颜色相同时，视为中奖。其余情形视为不中奖。
            </div>
            <br>
            <div class="word2" style="margin-left: 50px;">
                <strong>5.尾数</strong>
            </div>
            <div class="word2" style="margin-left: 50px;">
            含有所属尾数的一个或多个号码，但派彩只派一次，即不论同尾数号码出现一个或多个号码都只派彩一次。
            </div>
            <div class="tables" style="margin-top: -10px;margin-left:50px;" >
                <table>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">"0"尾</th>
                        <td>10、20、30、40</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">"1"尾</th>
                        <td>01、11、21、31、41</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">"2"尾</th>
                        <td>02、12、22、32、42</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">"3"尾</th>
                        <td>03、13、23、33、43</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">"4"尾</th>
                        <td>04、14、24、34、44</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">"5"尾</th>
                        <td>05、15、25、35、45</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">"6"尾</th>
                        <td>06、16、26、36、46</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">"7"尾</th>
                        <td>07、17、27、37、47</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">"8"尾</th>
                        <td>08、18、28、38、48</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">"9"尾</th>
                        <td>09、19、29、39、49</td>
                    </tr>
                </table>
            </div>
            <br>
            <div class="word2" style="margin-left: 50px;">
                <span class="example">例如</span> 开奖结果正码号码为11、31、42、44、35、32特别号码为21则1尾2尾4尾5尾都为中奖，其他尾数都不中奖。（请注意:49亦算输赢，不为和）。
            </div>
    	    </div>
    		<div class="space"></div>

            <div class="palys">
            <div class="play1">
                <span class="yuan">
                    七
                </span>
                <span class="word1">
                    连码
                </span>
            </div>
            <div class="word2">
            1.三中二：所投注的每三个号码为一组合，若其中2个是开奖号码中的正码，即为三中二，视为中奖；若3个都是开奖号码中的正码，即为三中二之中三，其余情形视为不中奖，如06、07、08 为一组合，开奖号码中有06、07两个正码，没有08，即为三中二，按三中二赔付；如开奖号码中有06、07、08三个正码，即为三中二之中三，按中三赔付；如出现1个或没有，视为不中奖 。
            </div>
            <br>
            <div class="word2">
            2.四全中：选择投注号码每四个为一组（四个或四个以上），兑奖号为正码，如四个号码都在开奖号码的正码里面，视为中奖，其他情形都视为不中奖 。    
            </div>
            <br>
            <div class="word2">
            3.三全中：所投注的每三个号码为一组合，若三个号码都是开奖号码之正码，视为中奖，其余情形视为不中奖。如06、07、08三个都是开奖号码之正码，视为中奖，如两个正码加上一个特别号码视为不中奖 。    
            </div>
            <br>
            <div class="word2">
            4.二全中：所投注的每二个号码为一组合，二个号码都是开奖号码之正码，视为中奖，其余情形视为不中奖（含一个正码加一个特别号码之情形）。    
            </div>
            <br>
            <div class="word2">
            5.二中特：所投注的每二个号码为一组合，二个号码都是开奖号码之正码，叫二中特之中二；若其中一个是正码，一个是特别号码，叫二中特之中特；其余情形视为不中奖 。    
            </div>
            <br>
            <div class="word2">
            6.特串：所投注的每二个号码为一组合，其中一个是正码，一个是特别号码，视为中奖，其余情形视为不中奖（含二个号码都是正码之情形） 。    
            </div>
            </div>
            <div class="space"></div>

            <div class="palys">
            <div class="play1">
                <span class="yuan">
                    八
                </span>
                <span class="word1">
                    生肖
                </span>
            </div>
            <div class="word2" style="margin-left: 50px;">
                <strong>1.特码生肖（请注意：若该期特码开出49时，此类生肖玩法视为和局）</strong>
            </div>
            <div class="tables" style="margin-top: -10px;margin-left:50px;">
                <table>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">天肖</th>
                        <td>兔马猴猪牛龙</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">地肖</th>
                        <td>鼠虎蛇羊鸡狗</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">家肖</th>
                        <td>羊马牛猪狗鸡</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">野肖</th>
                        <td>猴蛇龙兔虎鼠</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">前肖</th>
                        <td>鼠牛虎兔龙蛇</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">后肖</th>
                        <td>马羊猴鸡狗猪</td>
                    </tr>
                </table>
            </div>    
            <br>
            <div class="word2" style="margin-left: 50px;">
                <strong>2.特码生肖</strong>
            </div>
            <div class="word2" style="margin-left: 50px;">
            生肖顺序为 鼠 >牛 >虎 >兔 >龙 >蛇 >马 >羊 >猴 >鸡 >狗 >猪 。如今年是狗年，就以狗为开始，依顺序将49个号码分为12个生肖 『如下』
            </div>
            <div class="tables" style="margin-top: -10px;margin-left:50px;">
                <table>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">狗</th>
                        <td>01、13、25、37、49</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">猪</th>
                        <td>12、24、36、48</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">鼠</th>
                        <td>11、23、35、47</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">牛</th>
                        <td>10、22、34、46</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">虎</th>
                        <td>09、21、33、45</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">兔</th>
                        <td>08、20、32、44</td>
                    </tr>
                     <tr>
                        <th style="font-weight: normal;height: 30px;">龙</th>
                        <td>07、19、31、43</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">蛇</th>
                        <td>06、18、30、42</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">马</th>
                        <td>05、17、29、41</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">羊</th>
                        <td>04、16、28、40</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">猴</th>
                        <td>03、15、27、39</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;height: 30px;">鸡</th>
                        <td>02、14、26、38</td>
                    </tr>
                </table>
            </div>    
            <br>
            <div class="word2" style="margin-left: 50px;">
               若当期特别号，落在下注生肖范围内，视为中奖
            </div>
            <div class="word2" style="margin-left: 50px;">
                <span class="example">例如</span> 19,47,22,12,40,36+X 开奖：XXXXXX+18 为特码-蛇，投注特码-蛇即中奖，投注特码其它生肖不中奖。
            </div>
            <div class="word2" style="margin-left: 50px;">
                <strong>3.正肖</strong>
            </div>
            <div class="word2" style="margin-left: 50px;">
            当期开奖的前6个号码(不含特码，不分先后顺序)，其中有一个号码在某一投注的生肖范围内即算中奖，中奖金额=投注金额*赔率*1。 如果有N个号码在某一投注的生肖范围内，中奖金额=投注金额*赔率*N。
            </div>
            <div class="word2" style="margin-left: 50px;">
                <strong>4.一肖</strong>
            </div>
            <div class="word2" style="margin-left: 50px;">
            当期开奖的全部号码(前6个号码和特码)，其中只要有一个球号在投注的生肖范围则中奖；没有一个球号在投注的生肖范围内，则不中奖；多个球号在投注生肖范围内，则中奖；但奖金不倍增，派彩只派一次，即不论同生肖号码出现一个或多个号码都只派一次。
            </div>
            </div>
            <div class="space"></div>

            <div class="palys">
            <div class="play1">
                <span class="yuan">
                    九
                </span>
                <span class="word1">
                    对碰
                </span>
            </div>
            <div class="word2">
            1.生肖（尾数）所对应的号码和特码生肖（尾数）项目的一样；一个生肖（尾数）对应多个号码，不论同生肖（尾数）的号码出现一个或多个，派彩只派一次。每个生肖（尾数）都有自己的赔率，下注组合的总赔率，取该组合生肖（尾数）的最低赔率为总赔率。
            </div>
            <br>
            <div class="word2">
            2.二连尾:选择二个尾数为一投注组合进行下注。该注的二个尾数必须在当期开出的7个开奖号码相对应的尾数中，视为中奖。
            </div>
            <br>
            <div class="word2">
            3.三连尾:选择三个尾数为一投注组合进行下注。该注的三个尾数必须在当期开出的7个开奖号码相对应的尾数中，视为中奖。
            </div>
            <br>
            <div class="word2">
            4.四连尾:选择四个尾数为一投注组合进行下注。该注的四个尾数必须在当期开出的7个开奖号码相对应的尾数中，视为中奖。
            </div>
            <br>
            <div class="word2">
            5.五连尾:选择五个尾数为一投注组合进行下注。该注的五个尾数必须在当期开出的7个开奖号码相对应的尾数中，视为中奖。
            </div>
            <br>
            <div class="word2">
            6.二连肖:选择二个生肖为一投注组合进行下注。该注的二个生肖必须在当期开出的7个开奖号码相对应的生肖中，视为中奖。
            </div>
            <br>
            <div class="word2">
            7.三连肖:选择三个生肖为一投注组合进行下注。该注的三个生肖必须在当期开出的7个开奖号码相对应的生肖中，视为中奖。
            </div>
            <br>
            <div class="word2">
            8.四连肖:选择四个生肖为一投注组合进行下注。该注的四个生肖必须在当期开出的7个开奖号码相对应的生肖中，视为中奖。
            </div>
            <br>
            <div class="word2">
            9.五连肖:选择五个生肖为一投注组合进行下注。该注的五个生肖必须在当期开出的7个开奖号码相对应的生肖中，视为中奖。
            </div>
            <div class="space"></div>

             <div class="palys">
            <div class="play1">
                <span class="yuan">
                    十
                </span>
                <span class="word1">
                    合肖
                </span>
            </div>
            <div class="word2">
            挑选2-11个生肖『排列如同生肖』为一个组合，并选择开奖号码的特码是否在此组合内『49号除外』，即视为中奖；若当期特码开出49号，则所有组合皆视为和局。
            </div>
            <br>
            <div class="word2" style="margin-left: 20px;">
            <span class="word1">自选不中</span>
            </div>
            <br>
            <br>
            <div class="word2">
            挑选最少6个号码为一投注组合进行下注。当期开出的7个开奖号码都没有在该下注组合中，即视为中奖。每个号码都有自己的赔率，下注组合的总赔率，取该组合号码的最低赔率为总赔率。如下注组合为1-2-3-4-5-13，开奖号码为6，7，8，9，10，11，12，即为中奖，如果开奖号码为5，6，7，8，9，10，11，则为不中奖。
            </div>
            <br>
            <div class="word2" style="margin-left: 20px;">
            <span class="word1">总肖</span>
            </div>
            <br>
            <br>
            <div class="word2">
            当期号码(所有正码与最后开出的特码)开出的不同生肖总数，与所投注之预计开出之生肖总和数(不用指定特定生肖)，则视为中奖，其余情形视为不中奖。例如：如果当期号码为19、24、12、34、40、39 特别号：49，总计六个生肖，若选总肖【6】则为中奖(请注意：49号亦算输赢，不为和）。
            </div>
            <br>
            <div class="word2" style="margin-left: 20px;">
            <span class="word1">总肖单双</span>
            </div>
            <br>
            <br>
            <div class="word2">
            当期号码（正码和特码）开出的不同生肖总数若为单数则为单，若为双数则为双。
            </div>
            <br>
            <div class="word2" style="margin-left: 20px;">
            <span class="word1">七色波</span>
            </div>
            <br>
            <br>
            <div class="word2">
            以开出的7个色波，那种颜色最多为中奖。 开出的6个正码各以1个色波计，特别号以1.5个色波计。而以下3种结果视为和局。
            </div>
            <br>
            <div class="word2" style="margin-left: 50px;">
            1. 6个正码开出3蓝3绿，而特别码是1.5红
            </div>
            <br>
            <div class="word2" style="margin-left: 50px;">
            2. 6个正码开出3蓝3红，而特别码是1.5绿
            </div>
            <br>
            <div class="word2" style="margin-left: 50px;">
            3. 6个正码开出3绿3红，而特别码是1.5蓝
            </div>
            <br>
            <div class="word2">
            如果出现和局，所有投注红，绿，蓝七色波的金额将全数退回，玩家也可投注和局
            </div>
            <br>
            <div class="word2" style="margin-left: 20px;">
            <span class="word1">五行</span>
            </div>
            <br>
            <br>
            <div class="word2">
            挑选一个五行选项为一个组合，若开奖号码的特码在此组合内，即视为中奖；若开奖号码的特码不在此组合内，即视为不中奖；
            </div>
            <div class="tables">
                <table>
                    <tr>
                        <th style="font-weight: normal;">金</th>
                        <td>04、05、18、19、26、27、34、35、48、49</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;">木</th>
                        <td>01、08、09、16、17、30、31、38、39、46、47</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;">水</th>
                        <td>06、07、14、15、22、23、36、37、44、45</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;">火</th>
                        <td>02、03、10、11、24、25、32、33、40、41</td>
                    </tr>
                    <tr>
                        <th style="font-weight: normal;">土</th>
                        <td>12、13、20、21、28、29、42、43</td>
                    </tr>
                </table>
            </div>
            <br>
            </div>
            <div class="space"></div>
    	</div>
    </div>

    </body>
</html> 
	  