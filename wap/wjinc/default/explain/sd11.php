<html style="font-size: 12px;">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="format-detection" content="telephone=no">
        <title>玩法说明</title>
        <link rel="stylesheet" href="/skin/laofan/newlist.css">
        <link rel="stylesheet" type="text/css" href="/skin/css/play.css">
    </head>
    <body ontouchstart="">
	<div class="header">
    <div class="headerTop">
<!--     	<a href="/index.php" style="display: block;position: absolute;top:3px;left: 0"><span class="left"></span></a> -->
        <div class="ui-toolbar-left">
            <a id="reveal-left" href="/index.php">reveal</a>
        </div>
        <h1 class="ui-toolbar-title" style="font-weight:normal">玩法说明</h1>
    </div>
    </div>
    <div class="content">
    	<div class="headinfo">
    		<span class="ssc"><img src="/skin/main/img/15.png" width="40px;"></span>
    		<span class="sscword">山东11选5</span>
    	</div>
    	<div class="word">
    		<div class="palys">
    		<div class="play1">
    			<span class="yuan">
    				一
    			</span>
    			<span class="word1">
    				玩法类型及承销
    			</span>
    		</div>
    		<div class="word2">
    		11选5是一种在线即开型彩票玩法，属于基诺型彩票范畴，由省体育彩票管理中心负责承销。
    	    </div>
    		<div class="space"></div>

    		<div class="palys">
    		<div class="play1">
    			<span class="yuan">
    				二
    			</span>
    			<span class="word1">
    				开奖与购买方式
    			</span>
    		</div>
    		<div class="word2">
    		1.本站山东11选5游戏每天进行87期，开奖时间为08:35至22:55，每隔 10分钟开奖一次。开奖号码通过电脑体育彩票每期开奖结果通过随机数码生成器，从01-11共11个号码中按顺序自动生成5个不同号码作为当期中奖号码。
    	    </div>
            <br>
    		<div class="word2">
    		2.购买者可透过本站进行投注，在对其选定的投注号码进行投注；或返点投注，返点越高投注赔率则低。每注金额人民币1元。
    	    </div>
            <br>
    	    <div  class="word2">
    	    3.购买者可选择机选号码投注、自选号码投注。机选号码投注是指由投注机随机产生投注号码进行投注，自选号码投注是指将购买者选定的号码输入投注机进行投注。
    	    </div>
            <br>
    	    <div  class="word2">
    	    4.购买者还可进行多倍投注、追号投注。多倍投注是指同样的投注号码进行翻倍数额购买的投注。追号投注指将一注或一组号码进行两期或两期以上的投注。追号可分为连续追号和间隔追号，连续追号指追号的期数是连续的，间隔追号指追号的期数不连续。	
    	    </div>
            <br>
            <div class="word2">
            5.如果用户投注成功后，若因销售终端故障、通讯线路故障和投注站信用额度受限等原因造成当期无法开奖的，应退还购买者投注金额。   
            </div>
    		</div>
    		<div class="space"></div>

    		<div class="palys">
    		<div class="play1">
    			<span class="yuan">
    				三
    			</span>
    			<span class="word1">
    				兑奖
    			</span>
    		</div>
    		<div class="word2">
    		返奖：无论大小奖均返还至用户在本站的账户中，一旦用户中奖，系统将自动返还中奖金额。可继续投注或提款，永无弃奖。
    	    </div>
    		</div>
    		<div class="space"></div>

    		<div class="palys">
    		<div class="play1">
    			<span class="yuan">
    				四
    			</span>
    			<span class="word1">
    				玩法说明
    			</span>
    		</div>
    		<div class="word2">
    		11选5投注区号码范围为01～11，每期开出5个号码作为中奖号码。11选5玩法即是竞猜5位开奖号码的全部或部分号码。
    	    </div>
    	    <div class="balls">
    	    <div>
    	    	<span class="ball">03</span>
    	    	<span class="num">万位</span>
    	    </div>
    	    <div>
    	    	<span class="ball">09</span>
    	    	<span class="num">千位</span>
    	    </div>
    	    <div>
    	    	<span class="ball">01</span>
    	    	<span class="num">百位</span>
    	    </div>
    	    <div>
    	    	<span class="ball">04</span>
    	    	<span class="num">十位</span>
    	    </div>
            <div>
                <span class="ball">10</span>
                <span class="num">个位</span>
            </div>
    	    </div>
            <div class="tables" style="margin-top: -20px;">
                <table>
                    <tr>
                        <th colspan="2">玩法</th>
                        <th>规则</th>
                    </tr>
                    <tr>
                        <td rowspan="8">任选</td>
                        <td>任选一</td>
                        <td>竞猜第1位开奖号码。</td>
                    </tr>
                    <tr>
                        <td>任选二</td>
                        <td>竞猜开奖号码中的任意2位。</td>
                    </tr>
                    <tr>
                        <td>任选三</td>
                        <td>竞猜开奖号码中的任意3位。</td>
                    </tr>
                    <tr>
                        <td>任选四</td>
                        <td>竞猜开奖号码中的任意4位。</td>
                    </tr>
                    <tr>
                        <td>任选五</td>
                        <td>选择5个号码，竞猜全部5位开奖号码。</td>
                    </tr>
                    <tr>
                        <td>任选六</td>
                        <td>选择6个号码，竞猜全部5位开奖号码。</td>
                    </tr>
                    <tr>
                        <td>任选七</td>
                        <td>选择7个号码，竞猜全部5位开奖号码。</td>
                    </tr>
                    <tr>
                        <td>任选八</td>
                        <td>选择8个号码，竞猜全部5位开奖号码。</td>
                    </tr>
                </table>
            </div>
            <div>
                <span class="yuans">
                    注
                </span>
            </div>
            <div class="space" style="margin-top: 20px;"></div>
            <div class="word3">
                • 直选：将投注号码以唯一的排列方式进行投注。
            </div>
            <div class="word3">
                • 组选：将投注号码的所有排列方式作为一注投注号码进行投注。
            </div>
            <div class="word3">
                <span class="example">示例</span> 前三组选01 02 03，排列方式有01 02 03、01 03 02、02 01 03、02 03 01、03 01 02、03 02 01，共计6种。
            </div>
    		</div>
    		<div class="space"></div>

    		<div class="palys">
    		<div class="play1"> 
    			<span class="yuan">
    				五
    			</span>
    			<span class="word1">
    				设奖及中奖
    			</span>
    		</div>
            <br>
    		<div class="word2" style="margin-top: -15px;">
    		奖金计算说明列表请浏览本站主页“开奖公告”页面，点击广东11选5查阅。
    	    </div>
    	    <div class="tables">
    	    	<table>
    	    		<tr>
    	    			<th colspan="2">奖级</th>
    	    			<th>中奖条件</th>
    	    			<th>中奖号码示例</th>
    	    			<th>单注最高赔率</th>
    	    		</tr>
    	    		<tr>
                        <td rowspan="8">任选</td>
                        <td>任选一</td>
                        <td>任意中1码</td>
                        <td>01</td>
                        <td>1:2.1</td>
                    </tr>
                    <tr>
                        <td>任选二</td>
                        <td>任意中2码</td>
                        <td>01 02，01 03</td>
                        <td>1:5.3</td>
                    </tr>
                    <tr>
                        <td>任选三</td>
                        <td>任意中3码</td>
                        <td>01 02 03，01 02 04</td>
                        <td>1:16</td>
                    </tr>
                    <tr>
                        <td>任选四</td>
                        <td>任意中4码</td>
                        <td>01 02 03 04，01 02 03 05</td>
                        <td>1:64</td>
                    </tr>
                    <tr>
                        <td>任选五</td>
                        <td>不定位中5码</td>
                        <td>01 02 03 04 05</td>
                        <td>1:438</td>
                    </tr>
                    <tr>
                        <td>任选六</td>
                        <td>不定位中5码</td>
                        <td>01 02 03 04 05 X</td>
                        <td>1:75</td>
                    </tr>
                    <tr>
                        <td>任选七</td>
                        <td>不定位中5码</td>
                        <td>01 02 03 04 05 X X</td>
                        <td>1:21</td>
                    </tr>
                    <tr>
                        <td>任选八</td>
                        <td>不定位中5码</td>
                        <td>01 02 03 04 05 X X X</td>
                        <td>1:8</td>
                    </tr>
    	    	</table>
    	    </div>
             <div>
                <span class="yuans">
                    注
                </span>
            </div>
            <div class="space" style="margin-top: 20px;"></div>
            <div class="word3">
                • 假设当期的开奖号码为01 02 03 04 05。
            </div>
            <div class="word3">
                • 2码和3码：2码指开奖号码的前/后2位号码，3码指开奖号码的前/中/后3位号码
            </div>
            <div class="word3">
                <span class="example">示例</span> 开奖号码为01 02 03 04 05，前1码为01，前2码为01 02，前3码为01 02 03。
            </div>
            <div class="word3">
                • 定位和不定位：定位指投注号码与开奖号码按位一致，不定位指投注号码与开奖号码一致，顺序不限。示例：开奖号码为01 02 03 04 05，01 02则定位中前2码，01 02或02 01则为不定位中前2码。
            </div>
    		</div>
    		<div class="space"></div>

    		<div class="palys">
    		<div class="play1">
    			<span class="yuan">
    				六
    			</span>
    			<span class="word1">
    				投注方式
    			</span>
    		</div>
    	    <div class="word2">
            <strong>1.任选一</strong>
            </div>
            <div class="word2">
            从01～11中任选1个或多个号码为一注，投注号码与开奖号码任意1位一致即为中奖，单注最高奖金4.2元
            </div>
            <div class="tables" style="margin-top: -10px;margin-left:40px;" >
                <table>
                    <tr>
                        <th rowspan="2">例：</th>
                        <td>开奖：04 05 02 09 07</td>
                    </tr>
                    <tr>
                        <th style="height: 30px;">选号：05</th>
                    </tr>
                </table>
            </div>
            <br>
            <div class="word2">
                <span class="example">命中</span> 任意一位号码，顺序不限，单注最高奖金4.2元
            </div>
            <br>
            <div class="word2">
            <strong>2.任选二</strong>
            </div>
            <div class="word2">
            从01～11中至少选择2个号码投注，选号与开奖号码任意2位。投注时可选择1个号码作为每注都有的胆码，再补充其它号码作为拖码投注。
            </div>
            <div class="tables" style="margin-top: -10px;margin-left:40px;" >
                <table>
                    <tr>
                        <th rowspan="2">例：</th>
                        <td>开奖：04 11 02 09 07</td>
                    </tr>
                    <tr>
                        <th style="height: 30px;">选号：11 04</th>
                    </tr>
                </table>
            </div>
            <br>
            <div class="word2">
                <span class="example">命中</span> 任意两位号码，顺序不限，单注最高奖金10.6元
            </div>
            <br>
            <div class="word2">
            <strong>3.任选三</strong>
            </div>
            <div class="word2">
            从01～11中任选3个号码为一注，选择3个以上号码为复式投注，投注号码与开奖号码中任意3个号码相同即为中奖。投注时可选择1～2个号码作为每注都有的胆码，再补充其它号码作为拖码投注。
            </div>
            <div class="tables" style="margin-top: -10px;margin-left:40px;" >
                <table>
                    <tr>
                        <th rowspan="2">例：</th>
                        <td>开奖：04 05 08 10 07</td>
                    </tr>
                    <tr>
                        <th style="height: 30px;">选号：04 08 07</th>
                    </tr>
                </table>
            </div>
            <br>
            <div class="word2">
                <span class="example">命中</span> 任意三位号码，顺序不限，单注最高奖金32元
            </div>
            <br>
            <div class="word2">
            <strong>4.任选四</strong>
            </div>
            <div class="word2">
            从01～11中任选4个号码为一注，选择4个以上号码为复式投注，投注号码与开奖号码中任意4个号码相同即为中奖。投注时可选择1～3个号码作为每注都有的胆码，再补充其它号码作为拖码投注。
            </div>
            <div class="tables" style="margin-top: -10px;margin-left:40px;" >
                <table>
                    <tr>
                        <th rowspan="2">例：</th>
                        <td>开奖：01 02 03 04 05</td>
                    </tr>
                    <tr>
                        <th style="height: 30px;">选号：01 03 04 05</th>
                    </tr>
                </table>
            </div>
            <br>
            <div class="word2">
                <span class="example">命中</span> 任意四位号码，顺序不限，单注最高奖金128元
            </div>
            <br>
            <div class="word2">
            <strong>5.任选五</strong>
            </div>
            <div class="word2">
            从01～11中任选5个号码为一注，选择5个以上号码为复式投注，投注号码与开奖号码全部相同即为中奖。投注时可选择1～4个号码作为每注都有的胆码，再补充其它号码作为拖码投注。
            </div>
            <div class="tables" style="margin-top: -10px;margin-left:40px;" >
                <table>
                    <tr>
                        <th rowspan="2">例：</th>
                        <td>开奖：01 03 08 10 07</td>
                    </tr>
                    <tr>
                        <th style="height: 30px;">选号：03 01 08 07 10</th>
                    </tr>
                </table>
            </div>
            <br>
            <div class="word2">
                <span class="example">命中</span> 任意五位号码，顺序不限，单注最高奖金876元
            </div>
            <br>
            <div class="word2">
            <strong>6.任选六</strong>
            </div>
            <div class="word2">
            从01～11中任选6个号码为一注，选择6个以上号码为复式投注，投注号码中任意5个号码与5位开奖号码相同即为中奖。投注时可选择1～5个号码作为每注都有的胆码，再补充其它号码作为拖码投注。
            </div>
            <div class="tables" style="margin-top: -10px;margin-left:40px;" >
                <table>
                    <tr>
                        <th rowspan="2">例：</th>
                        <td>开奖：01 03 08 10 07</td>
                    </tr>
                    <tr>
                        <th style="height: 30px;">选号：03 01 08 07 10 02</th>
                    </tr>
                </table>
            </div>
            <br>
            <div class="word2">
                <span class="example">命中</span> 任意六位号码，顺序不限，单注最高奖金150元
            </div>
            <br>
            <div class="word2">
            <strong>7.任选七</strong>
            </div>
            <div class="word2">
            从01～11中任选7个号码为一注，选择7个以上号码即为复式投注，投注号码中任意5个号码与5位开奖号码相同即为中奖。投注时可选择1～6个号码作为每注都有的胆码，再补充其它号码作为拖码进行投注。
            </div>
            <div class="tables" style="margin-top: -10px;margin-left:40px;" >
                <table>
                    <tr>
                        <th rowspan="2">例：</th>
                        <td>开奖：01 03 08 10 07</td>
                    </tr>
                    <tr>
                        <th style="height: 30px;">选号：03 01 08 07 10 02 04</th>
                    </tr>
                </table>
            </div>
            <br>
            <div class="word2">
                <span class="example">命中</span> 任意七位号码，顺序不限，单注最高奖金42元
            </div>
            <br>
            <div class="word2">
            <strong>8.任选八</strong>
            </div>
            <div class="word2">
            从01～11中任选8个号码为一注，选择8个以上号码为复式投注，投注号码中任意5个号码与5位开奖号码相同即为中奖。投注时可选择1～7个号码作为每注都有的胆码，再补充其它号码作为拖码投注。
            </div>
            <div class="tables" style="margin-top: -10px;margin-left:40px;" >
                <table>
                    <tr>
                        <th rowspan="2">例：</th>
                        <td>开奖：01 03 08 10 07</td>
                    </tr>
                    <tr>
                        <th style="height: 30px;">选号：03 01 08 07 10 02 04 05</th>
                    </tr>
                </table>
            </div>
            <br>
            <div class="word2">
                <span class="example">命中</span> 任意八位号码，顺序不限，单注最高奖金16元 9、二码直选 对前/后2位各选1个号码为一注，某一位或两位选择2个以上号码为复式投注，投注号码与开奖号码前/后2位按位一致即为中奖
            </div>
            <br>
            <div class="word2">
            <strong>9.二码直选</strong>
            </div>
            <div class="word2">
            选择两个号码（选择前两个号码是前二直选，选择后两个号码是后二直选），且选择的两个号码不能重复
            </div>
            <div class="tables" style="margin-top: -10px;margin-left:40px;" >
                <table>
                    <tr>
                        <th rowspan="2">例：</th>
                        <td>开奖：07 04 11 09 02</td>
                    </tr>
                    <tr>
                        <th style="height: 30px;">选号：07 04 或者09 02</th>
                    </tr>
                </table>
            </div>
            <br>
            <div class="word2">
                <span class="example">命中</span> 开奖号码的前两位号码，且顺序一致，单注最高奖金214元
            </div>
            <br>
            <div class="word2">
            <strong>10.二码组选</strong>
            </div>
            <div class="word2">
            从01～11中任选2个号码为一注，选择2个以上号码为复式投注，投注号码与开奖号码前/后2位一致，顺序不限即为中奖。
            </div>
            <div class="tables" style="margin-top: -10px;margin-left:40px;" >
                <table>
                    <tr>
                        <th rowspan="2">例：</th>
                        <td>开奖：01 03 08 10 07</td>
                    </tr>
                    <tr>
                        <th style="height: 30px;">选号：01 03</th>
                    </tr>
                </table>
            </div>
            <br>
            <div class="word2">
                <span class="example">命中</span> 开奖号码的前两位，顺序不限，单注最高奖金214元
            </div>
            <br>
            <div class="word2">
            <strong>11.三码直选</strong>
            </div>
            <div class="word2">
            对前/中/后3位各选1个号码为一注，某一位或几位选择2个以上号码为复式投注，投注号码与开奖号码前/中/后3位按位一致即为中奖，单注最高奖金1940元。
            </div>
            <div class="tables" style="margin-top: -10px;margin-left:40px;" >
                <table>
                    <tr>
                        <th rowspan="2">例：</th>
                        <td>开奖：01 03 08 10 07</td>
                    </tr>
                    <tr>
                        <th style="height: 30px;">选号：万位01 千位03 百位08</th>
                    </tr>
                </table>
            </div>
            <br>
            <div class="word2">
                <span class="example">命中</span> 开奖号码的前三位号码，且顺序一致，单注最高奖金214元
            </div>
            <br>
            <div class="word2">
            <strong>12.三码组选</strong>
            </div>
            <div class="word2">
            从01～11中任选3个号码为一注，选择3个以上号码为复式投注，投注号码与开奖号码前/中/后3位一致，顺序不限，即为中奖。
            </div>
            <div class="tables" style="margin-top: -10px;margin-left:40px;" >
                <table>
                    <tr>
                        <th rowspan="2">例：</th>
                        <td>开奖：01 03 08 10 07</td>
                    </tr>
                    <tr>
                        <th style="height: 30px;">选号：01 03 08</th>
                    </tr>
                </table>
            </div>
            <br>
            <div class="word2">
                <span class="example">命中</span> 开奖号码的前三位，顺序不限，单注最高奖金322元
            </div>
            <br>
    	    </div>
    		<div class="space"></div>
    	</div>
    </div>

    </body>
</html> 
	  