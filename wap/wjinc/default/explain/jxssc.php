<html style="font-size: 12px;">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="format-detection" content="telephone=no">
        <title>玩法说明</title>
        <link rel="stylesheet" href="/skin/laofan/newlist.css">
        <link rel="stylesheet" type="text/css" href="/skin/css/play.css">
    </head>
    <body ontouchstart="">
	<div class="header">
    <div class="headerTop">
<!--     	<a href="/index.php" style="display: block;position: absolute;top:3px;left: 0"><span class="left"></span></a> -->
        <div class="ui-toolbar-left">
            <a id="reveal-left" href="/index.php">reveal</a>
        </div>
        <h1 class="ui-toolbar-title" style="font-weight:normal">玩法说明</h1>
    </div>
    </div>
    <div class="content">
    	<div class="headinfo">
    		<span class="ssc"><img src="/skin/main/img/7.png" width="40px;"></span>
    		<span class="sscword">天津时时彩</span>
    	</div>
    	<div class="word">
    		<div class="palys">
    		<div class="play1">
    			<span class="yuan">
    				一
    			</span>
    			<span class="word1">
    				玩法类型及承销
    			</span>
    		</div>
    		<div class="word2">
    		时时彩是一种在线即开型彩票玩法，属于基诺型彩票，由市福利彩票发行管理中心负责承销。
    	    </div>
    		</div>
    		<div class="space"></div>

    		<div class="palys">
    		<div class="play1">
    			<span class="yuan">
    				二
    			</span>
    			<span class="word1">
    				开奖与购买方式
    			</span>
    		</div>
    		<div class="word2">
    		1.本站江西时时彩游戏每天进行84期，开奖时间为09:10至23:00，每10分钟开奖一次。购买者可在对其选定的投注号码进行投注，返点越高则投注赔率越低。
    	    </div>
            <br>
    		<div class="word2">
    		2.购买者可选择机选号码投注、自选号码投注。机选号码投注是指由投注机随机产生投注号码进行投注，自选号码投注是指将购买者选定的号码输入投注机进行投注。。
    	    </div>
            <br>
    	    <div  class="word2">
    	    3.购买者还可进行追号投注。追号投注是指将一注或一组号码进行两期或两期以上的投注。追号可分为连续追号和间隔追号，连续追号指追号的期数是连续的，间隔追号指追号的期数不连续。
    	    </div>
            <br>
    	    <div  class="word2">
    	    4.如果用户投注成功后，若因销售终端故障、通讯线路故障和投注站信用额度受限等原因造成当期无法开奖的，应退还购买者投注金额。	
    	    </div>
    		</div>
    		<div class="space"></div>

    		<div class="palys">
    		<div class="play1">
    			<span class="yuan">
    				三
    			</span>
    			<span class="word1">
    				兑奖
    			</span>
    		</div>
    		<div class="word2">
    		返奖：无论大小奖均返还至用户在本站的账户中，一旦用户中奖，系统将自动返还中奖金额。可继续投注或提款，永无弃奖。
    	    </div>
    		</div>
    		<div class="space"></div>

    		<div class="palys">
    		<div class="play1">
    			<span class="yuan">
    				四
    			</span>
    			<span class="word1">
    				玩法说明
    			</span>
    		</div>
    		<div class="word2">
    		1.时时彩投注区分为万位、千位、百位、十位和个位，各位号码范围为0～9。每期从各位上开出1个号码作为中奖号码，即开奖号码为5位数。时时彩玩法即是竞猜5位开奖号码的全部号码、部分号码或部分号码特征。
    	    </div>
    	    <div class="balls">
    	    <div>
    	    	<span class="ball">6</span>
    	    	<span class="num">万位</span>
    	    </div>
    	    <div>
    	    	<span class="ball">8</span>
    	    	<span class="num">千位</span>
    	    </div>
    	    <div>
    	    	<span class="ball">5</span>
    	    	<span class="num">百位</span>
    	    </div>
    	    <div>
    	    	<span class="ball">5</span>
    	    	<span class="num">十位</span>
    	    </div>
    	    <div>
    	    	<span class="ball">6</span>
    	    	<span class="num">个位</span>
    	    </div>
    	    </div>
    	    <div class="word2">
    	    	2.时时彩包括星彩玩、定位胆、不定位、大小单双、龙虎、任选等玩法。星彩玩法分为二星、前后三星、四星、五星四种玩法如下：
    	    </div>
    	    <div class="tables">
    	    	<table>
    	    		<tr>
    	    			<th colspan="2">玩法</th>
    	    			<th colspan="9">规则</th>
    	    		</tr>
    	    		<tr>
    	    			<td>五星</td>
    	    			<td>直选</td>
    	    			<td>竞猜全部5位号码，即万位、千位、百位、十位和个位，且顺序一致。</td>
    	    		</tr>
    	    		<tr>
    	    			<td>四星</td>
    	    			<td>直选</td>
    	    			<td>竞猜全部4位号码，即千位、百位、十位和个位，且顺序一致。</td>
    	    		</tr>
    	    		<tr>
    	    			<td rowspan="3">前三/后三</td>
    	    			<td>直选</td>
    	    			<td>竟猜前/后三码，即(万、千、百)位；或(百、十、个)位，且顺序一致。</td>
    	    		</tr>
    	    		<tr>
    	    			<td>组选三</td>
    	    			<td>竟猜前/后三码，即(万、千、百)位；或(百、十、个)位，顺序不限，且投注时三位号码有两位相同。</td>
    	    		</tr>
    	    		<tr>
    	    			<td>组选六</td>
    	    			<td>竟猜前/后三码，即(万、千、百)位；或(百、十、个)位，顺序不限，且投注时三位号码各不相同。</td>
    	    		</tr>
    	    		<tr>
    	    			<td rowspan="2">前二</td>
    	    			<td>直选</td>
    	    			<td>竟猜前两码，即万位和千位，且顺序一致。</td>
    	    		</tr>
    	    		<tr>
    	    			<td>组选</td>
    	    			<td>竟猜前两码，即万位和千位，顺序不限。</td>
    	    		</tr>
    	    		<tr>
    	    			<td colspan="2">定位胆</td>
    	    			<td>竟猜前两码，即万位和千位，顺序不限。</td>
    	    		</tr>
    	    		<tr>
    	    			<td colspan="2">不定位</td>
    	    			<td>竞猜对应三星、四星、五星玩法位置上任意1个或1个以上号码，且顺序不限。</td>
    	    		</tr>
    	    		<tr>
    	    			<td colspan="2">大小单双</td>
    	    			<td>竞猜对应三星、四星、五星玩法位置上任意1个或1个以上号码，且顺序不限。</td>
    	    		</tr>
    	    		<tr>
    	    			<td colspan="2">龙虎</td>
    	    			<td>竟猜万位、千位、百位、十位、个位任意位置上两两号码比对的大小。</td>
    	    		</tr>
    	    		<tr>
    	    			<td rowspan="3">任选</td>
    	    			<td>任选二</td>
    	    			<td>竞猜万位、千位、百位、十位、个位任意位置上任意2个号码或者2个号码和值。</td>
    	    		</tr>
    	    		<tr>
    	    			<td>任选三</td>
    	    			<td>竞猜万位、千位、百位、十位、个位任意位置上任意3个号码或者3个号码和值。</td>
    	    		</tr>
    	    		<tr>
    	    			<td>任选四</td>
    	    			<td>竞猜万位、千位、百位、十位、个位任意位置上任意4个号码或者4个号码和值。</td>
    	    		</tr>
    	    	</table>
    	    </div>
    	    <div>
    	    	<span class="yuans">
    				注
    			</span>
    	    </div>
    	    <div class="space" style="margin-top: 20px;"></div>
    	    <div class="word3">
    	    	• <strong>直选：</strong>将投注号码以惟一的排列方式进行投注。
    	    </div>
    	    <div class="word3">
    	    	• <strong>组选：</strong>将投注号码的所有排列方式作为一注投注号码进行投注。
    	    </div>
    	    <div class="word3">
    	    	<span class="example">示例</span> 123，排列方式有123、132、213、231、312、321，共计6种。
    	    </div>
    	    <div class="word3">
    	    	• <strong>组选三：</strong>在组选中，如果一注组选号码的3个数字有两个数字相同，则有3种不同的排列方式，因而就有3个中奖机会，这种组选投注方式简称组选三。
    	    </div>
    	    <div class="word3">
    	    	<span class="example">示例</span> 112，排列方式有112、121、211。
    	    </div>
    	    <div class="word3">
    	    	• <strong>组选六：</strong>在组选中，如果一注组选号码的3个数字各不相同，则有6种不同的排列方式，因而就有6个中奖机会，这种组选投注方式简称组选六。
    	    </div>
    	    <div class="word3">
    	    	<span class="example">示例</span> 123，排列方式有123、132、213、231、312、321，共计6种。
    	    </div>
    	    <div class="word3">
    	    	• <strong>大小单双：</strong>即把10个自然数按“大”“小”，或者“单”，“双”性质分为两组，0-4为小号，5-9为大号。0，2，4，6，8 为双号。1，3，5，7，9 为单号。
    	    </div>
    		</div>
    		<div class="space"></div>

    		<div class="palys">
    		<div class="play1">
    			<span class="yuan">
    				五
    			</span>
    			<span class="word1">
    				设奖及中奖
    			</span>
    		</div>
    		<div class="word2">
    		奖金计算说明列表请浏览本站主页“开奖公告”页面，点击重庆时时彩查阅。
    	    </div>
    	    <div class="tables">
    	    	<table>
    	    		<tr>
    	    			<th colspan="2">玩法</th>
    	    			<th>开奖号码</th>
    	    			<th>投注号码示例</th>
    	    			<th width="30px;"></th>
    	    			<th>中奖概率</th>
    	    			<th>单注最高赔率</th>
    	    		</tr>
    	    		<tr>
    	    			<td rowspan="7">三星</td>
    	    			<td>直选(复式)</td>
    	    			<td rowspan="2">678</td>
    	    			<td>678</td>
    	    			<td rowspan="3">定位中三码</td>
    	    			<td rowspan="3">1/1000</td>
    	    			<td rowspan="3">1:950</td>
    	    		</tr>
    	    		<tr>
    	    			<td>直选(单式)</td>
    	    			<td>678</td>
    	    		</tr>
    	    		<tr>
    	    			<td>直选和值</td>
    	    			<td>13</td>
    	    			<td>13</td>
    	    		</tr>
    	    		<tr>
    	    			<td>组选(组三)</td>
    	    			<td>113</td>
    	    			<td>113</td>
    	    			<td rowspan="4">不定位中三码</td>
    	    			<td>1/90</td>
    	    			<td>1:316.666</td>
    	    		</tr>
    	    		<tr>
    	    			<td>组选(组六)</td>
    	    			<td>123</td>
    	    			<td>123</td>
    	    			<td>1/120</td>
    	    			<td>1:158.333</td>
    	    		</tr>
    	    		<tr>
    	    			<td>组选和值(组三)</td>
    	    			<td>5(113)</td>
    	    			<td>5(113)</td>
    	    			<td>1/90</td>
    	    			<td>1:316.666</td>
    	    		</tr>
    	    		<tr>
    	    			<td>组选和值(组六)</td>
    	    			<td>6(123)</td>
    	    			<td>6(123)</td>
    	    			<td>1/120</td>
    	    			<td>1:158.333</td>
    	    		</tr>
    	    		<tr>
    	    			<td rowspan="2">二星</td>
    	    			<td>直选(前/后二)</td>
    	    			<td>78</td>
    	    			<td>78</td>
    	    			<td>定位中二码</td>
    	    			<td>1/100</td>
    	    			<td>1:95</td>
    	    		</tr>
    	    		<tr>
    	    			<td>组选(前/后二)</td>
    	    			<td>78</td>
    	    			<td>78</td>
    	    			<td>不定位中二码</td>
    	    			<td>1/45</td>
    	    			<td>1:47.5</td>
    	    		</tr>
    	    		<tr>
    	    			<td colspan="2">大小单双</td>
    	    			<td>678</td>
    	    			<td>大单</td>
    	    			<td>大小</td>
    	    			<td>1/16</td>
    	    			<td>1:3.8</td>
    	    		</tr>
    	    		<tr>
    	    			<td colspan="2">龙虎</td>
    	    			<td>78</td>
    	    			<td>个位</td>
    	    			<td>大小</td>
    	    			<td>1/2.2</td>
    	    			<td>1:1.96</td>
    	    		</tr>
    	    	</table>
    	    </div>
    	    <div>
    	    	<span class="yuans">
    				注
    			</span>
    	    </div>
    	    <div class="space" style="margin-top: 20px;"></div>
    	    <div class="word3">
    	    	• 假设当期的开奖号码为45678则组选三不中奖（后三组选适用开奖号码为45668）。
    	    </div>
    	    <div class="word3">
    	    	 • 前三码和后三码：前三码指开奖号码的前三位号码，后三码指开奖号码的后三位号码。
    	    </div>
    	    <div class="word3">
    	    	<span class="example">示例</span> 开奖号码为45678，前三码为456，后三码为678。
    	    </div>
    	    <div class="word3">
    	    	 • 前两码和后两码：前两码指开奖号码的前两位号码，后两码指开奖号码的后两位号码。
    	    </div>
    	    <div class="word3">
    	    	<span class="example">示例</span> 开奖号码为45678，前两码为45，后两码为78。
    	    </div>
    	    <div class="word3">
    	    	 • 定位胆：所选号码与相同位置上和开奖号码一致。
    	    </div>
    	    <div class="word3">
    	    	<span class="example">示例</span> 开奖号码为45678，万位4即中定位胆万位。
    	    </div>
    	    <div class="word3">
    	    	 • 龙虎：所选任意两两位置上数字的大小对比与开奖号码一致。
    	    </div>
    	    <div class="word3">
    	    	<span class="example">示例</span> 开奖号码为45678，位置选个位即中龙虎。
    	    </div>
    	    <div class="word3">
    	    	 • 定位和不定位：定位指投注号码与开奖号码按位一致，不定位指投注号码与开奖号码一致，顺序不限。
    	    </div>
    	    <div class="word3">
    	    	<span class="example">示例</span> 开奖号码为45678，78则定位中后两码，78或87则为不定位中后两码。
    	    </div>
    	    <div class="word3">
    	    	 • 任选和值：所选号码与开奖号码的和值相同。
    	    </div>
    	    <div class="word3">
    	    	<span class="example">示例</span> 开奖号码为45678，位置选万位、百位，和值号码即10。
    	    </div>
    		</div>
    		<div class="space"></div>

    		<div class="palys">
    		<div class="play1">
    			<span class="yuan">
    				六
    			</span>
    			<span class="word1">
    				投注方式
    			</span>
    		</div>
    		<div class="word2">
    		<strong>1.五星直选</strong>
    	    </div>
    	    <div class="word2"  style="margin-left: 50px;">
    		对万位、千位、百位、十位和个位各选1个号码为一注，每位号码可从0～9全选，投注号码与开奖号码按位一致，即为中奖。
    	    </div>
    	    <div  class="word2" style="margin-left: 50px;">
    	    	<img src="/skin/main/img/readImage.png" width="100%">
    	    </div>
    	    <div class="word2">
    		<strong>2.四星直选</strong>
    	    </div>
    	    <div class="word2"  style="margin-left: 50px;">
    		对千位、百位、十位和个位各选1个号码为一注，每位号码可从0～9全选，投注号码与开奖号码按位一致，即为中奖。
    	    </div>
    	    <div class="word2">
    		<strong>3.三星直选</strong>
    	    </div>
    	    <div class="word2"  style="margin-left: 50px;">
    		对百位、十位和个位各选1个号码为一注，每位号码最多可0～9全选，投注号码与开奖号码后三位按位一致即为中奖。
    	    </div>
    	    <div class="word2">
    		<strong>4.三星和值</strong>
    	    </div>
    	    <div class="word2"  style="margin-left: 50px;">
    		和值指号码各位数相加之和，如号码001，和值为1。三星和值投注指用某一个三星和值对应的所有号码进行投注，所选和值与开奖号码后三位和值一致即为中奖，单注最高奖金980元。示例：选择三星和值1投注，即用和值1所对应的号码（001、010、100）投注，如开奖号码后三位和值为1即中奖。
    	    </div>
    	    <div  class="word2" style="margin-left: 50px;">
    	    	<img src="/skin/main/img/readImage2.png" width="100%">
    	    </div>
    	    <div class="word2">
    		<strong>5.组三组选</strong>
    	    </div>
    		<div class="word2"  style="margin-left: 50px;">
    		组三包号指用所选号码的所有组三排列方式进行组选三投注，如开奖号码为组三号且包含在所选号码中即为中奖，单注最高奖金326.666元元。示例：组三包号12，共2注（112、122），如开奖号码后三位为112、121、211、122、212、221皆为中奖。 包号速算表如下：
    	    </div>
    	    <div class="tables">
    	    	<table>
    	    		<tr>
    	    			<th>包号个数（10选n）</th>
    	    			<th>投注金额（2元）</th>
    	    			<th>单注最高赔率</th>
    	    		</tr>
    	    		<tr>
    	    		    <td>2</td>
    	    		    <td>4</td>
    	    			<td>653.332</td>
    	    		</tr>
    	    		<tr>
    	    		    <td>6</td>
    	    		    <td>12</td>
    	    			<td>653.332</td>
    	    		</tr>
    	    		<tr>
    	    		    <td>4</td>
    	    		    <td>24</td>
    	    			<td>653.332</td>
    	    		</tr>
    	    		<tr>
    	    		    <td>5</td>
    	    		    <td>40</td>
    	    			<td>653.332</td>
    	    		</tr>
    	    		<tr>
    	    		    <td>6</td>
    	    		    <td>60</td>
    	    			<td>653.332</td>
    	    		</tr>
    	    		<tr>
    	    		    <td>8</td>
    	    		    <td>84</td>
    	    			<td>653.332</td>
    	    		</tr>
    	    		<tr>
    	    		    <td>9</td>
    	    		    <td>144</td>
    	    			<td>653.332</td>
    	    		</tr>
    	    		<tr>
    	    		    <td>10</td>
    	    		    <td>180</td>
    	    			<td>653.332</td>
    	    		</tr>
    	    	</table>
    	    </div>
    	    <br>
    	    <div class="word2">
    		<strong>6.组六组选</strong>
    	    </div>
    		<div class="word2"  style="margin-left: 50px;">
    		组六包号指用所选号码的所有组六排列方式进行组选六投注，如开奖号码为组六号且包含在所选号码中即为中奖，单注最高奖金163.333元。示例：组六包号123，共1注，如开奖号码后三位为123、132、213、231、312、321皆为中奖。 包号速算表如下：
    	    </div>
    	    <div class="tables">
    	    	<table>
    	    		<tr>
    	    			<th>包号个数（10选n）</th>
    	    			<th>投注金额（2元）</th>
    	    			<th>单注最高赔率</th>
    	    		</tr>
    	    		<tr>
    	    		    <td>4</td>
    	    		    <td>8</td>
    	    			<td>326.666</td>
    	    		</tr>
    	    		<tr>
    	    		    <td>5</td>
    	    		    <td>20</td>
    	    			<td>326.666</td>
    	    		</tr>
    	    		<tr>
    	    		    <td>6</td>
    	    		    <td>40</td>
    	    			<td>326.666</td>
    	    		</tr>
    	    		<tr>
    	    		    <td>7</td>
    	    		    <td>74</td>
    	    			<td>326.666</td>
    	    		</tr>
    	    		<tr>
    	    		    <td>8</td>
    	    		    <td>112</td>
    	    			<td>326.666</td>
    	    		</tr>
    	    		<tr>
    	    		    <td>9</td>
    	    		    <td>168</td>
    	    			<td>326.666</td>
    	    		</tr>
    	    		<tr>
    	    		    <td>10</td>
    	    		    <td>240</td>
    	    			<td>326.666</td>
    	    		</tr>
    	    	</table>
    	    </div>
    	    <br>
    	    <div class="word2">
    		<strong>7.二星直选</strong>
    	    </div>
    		<div class="word2"  style="margin-left: 50px;">
    		对万位和千位各选1个号码为一注，每位号码最多可0～9全选，投注号码与开奖号码后两位按位一致即为中奖。
    	    </div>
    	    <div class="word2">
    		<strong>8.二星和值</strong>
    	    </div>
    		<div class="word2"  style="margin-left: 50px;">
    		和值指号码各位数相加之和，如号码01，和值为1。二星和值投注指用某一个二星和值对应的所有号码进行投注，所选和值与开奖号码前两位和值一致即为中奖，单注最高奖金98元。示例：选择二星和值1投注，即用和值1所对应的号码（01、10）投注，如开奖号码前两位和值为1即中奖。
    	    </div>
    	    <div class="word2">
    		<strong>9.二星组选</strong>
    	    </div>
    		<div class="word2"  style="margin-left: 50px;">
    		从号码0—9中任选两个数字对万位和千位进行投注，最多可0～9全选。所选号码与开奖号码前两位一致，顺序不限，即为中奖。示例：选择12，共1注，如开奖号码前两位为12或21即中奖。
    	    </div>
    	    <div class="word2">
    		<strong>10.大小单双</strong>
    	    </div>
    	    <div class="word2"  style="margin-left: 50px;">
    		对万位和千位的大小单双4种特征中各选一种特征为一注，最多可4种特征全选，所选特征与开奖号码后两位号码特征一致即中奖。示例：开奖号码后两位为78，则大大、大双、单双、单大为中奖。
    	    </div>
    	    <div  class="word2" style="margin-left: 50px;">
    	    	<img src="/skin/main/img/readImage3.png" width="100%">
    	    </div>
    	    <div class="word2">
    		<strong>11.龙虎</strong>
    	    </div>
    	    <div class="word2"  style="margin-left: 50px;">
    		对任意两两位置的大小进行投注，投注的位置若为大数，即为中奖；两数相等为平局，平局视为未中奖。
    	    </div>
    		</div>
    		<div class="space"></div>

    	</div>
    </div>

    </body>
</html> 
	  