<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<link href="/skin/main/bl_ococ.css" rel="stylesheet" type="text/css">
<?php $this->display('inc_skin.php', 0 , '会员中心－密码管理'); ?>

</head> 
<?php 
  $daili=$this->getRows("select uid,parentId,username,nickname,name,type from {$this->prename}members where parentId=".$this->user['uid']." and type=1");
  $huiyuan=$this->getRows("select uid,parentId,username,nickname,name,type from {$this->prename}members where parentId=".$this->user['uid']." and type=0");
?>

<body>
<div id="mainbody"> 
<div id="page-header">
<div class="fm userInfo"> <div class="bodytop">
    <div id="money">
    昵称：<em><?=$this->user['nickname']?></em>
    余额：
    <strong>￥<?=$this->user['coin']?><a href="#" onclick="reloadMemberInfo()"><img src="/images/common/ref.png" alt="刷新余额"></a></strong>
    <a href="/index.php/cash/recharge">充值</a>
    <a href="/index.php/cash/toCash">提款</a>
    <a href="http://www.chinacqcp.com/kf.html target=" _blank"="">客服</a>
    <a href="/index.php/user/logout">退出</a>
  </div>
</div></div>
</div>
<style type="text/css">
  .pagemain input.btnss {
    background-color: #c32828;
    line-height: 20px;
    height: 30px;
    width: 80px;
    padding: 5px;
    padding-left: 20px;
    padding-right: 20px;
    border: 0;
    margin: 10px 5px;
    color: #fff;
    border-radius: 5px;
    background-image: url(/images/common/light.png);
    background-position: left top;
    background-repeat: repeat-x;
    border: 1px #dc4545 solid;
    cursor: pointer;
    margin-left: 20%;
}
.mmss{
  border: 1px solid #CCCCCC;
    border-radius: 5px;
    height: 24px;
    margin-right: 10px;
    width: 100px;
    padding-left: 5px;
    vertical-align: middle;
}
</style>

<div class="pagetop"></div>
<div class="pagemain">
     <input type="button" value="下级代理" onclick="daili()" class="btnss"> 
     <input type="button" value="下级会员" onclick="huiyuan()" class="btnss" style="background:#CCCCCC;border:1px solid #CCCCCC;" />
    

  <div id="dailis">  
    <p style="color: #c32828;font-size: 18px;font-weight: bold;">代理订单</p>
    <table width="100%" border="0" cellspacing="1" cellpadding="4" class='table_b'>
      <tr height=25 class='table_b_th'>
        <td align="left" style="font-weight:bold;padding-left:10px;" colspan=2>下级代理</td> 
      </tr>
      <tr height=25 class='table_b_tr_b'>
            <td align="left" width="100%">
            <?php if($daili) foreach($daili as $var){ ?>
            <div style="padding: 5px 5px;border-bottom:1px solid #CCCCCC; ">
            <input style="display: inline-block;width: 80%;border: none;" type="text" readonly="readonly" value="<?=$var['username']?>" /><a>查看订单</a>
            </div>
            <?php } ?>
            </td>
      </tr>
    </table>
  </div>

  
  <div id="huiyuans">
  <p style="color: #c32828;font-size: 18px;font-weight: bold;">会员订单</p>
  <table width="100%" border="0" cellspacing="1" cellpadding="4" class='table_b'>
      <tr height=25 class='table_b_th'>
        <td align="left" style="font-weight:bold;padding-left:10px;" colspan=2>下级会员</td>
      </tr>
      <tr height=25 class='table_b_tr_b'>
            <td align="left" width="75%">
            <?php if($huiyuan) foreach($huiyuan as $var){ ?>
            <div style="padding: 5px 5px;border-bottom:1px solid #CCCCCC; ">
            <input style="display: inline-block;width: 80%;border: none;" type="text" readonly="readonly" value="<?=$var['username']?>" /><a>查看订单</a>
            </div>
            <?php } ?>
            </td>
      </tr>
    </table>
  </div>

</div>
<div class="pagebottom"></div>
</div>

</body>
</html>
<script type="text/javascript">
  $(function(){
    $('#dailis').show();
    $('#huiyuans').hide();
  })
  //代理
  function daili() {
    $('#dailis').show();
    $('#huiyuans').hide();
    $('.btnss').eq(0).css({'background':'#c32828','border':'1px solid #c32828'});//代理
    $('.btnss').eq(1).css({'background':'#CCCCCC','border':'1px solid #CCCCCC'});//会员
  }
  //会员
  function huiyuan(){
    $('#dailis').hide();
    $('#huiyuans').show();
    $('.btnss').eq(1).css({'background':'#c32828','border':'1px solid #c32828'});//会员
    $('.btnss').eq(0).css({'background':'#CCCCCC','border':'1px solid #CCCCCC'});//代理
  }
  //ajax代理划分
  function setdailis(my_uid){
    var daili_money = $('#daili_money').val();
    var daili_uid = $('#daili_uid').val();
    $.ajax({
        url:"/index.php/safe/setdaili",
        type:'post',
        cache:false,
        dataType:'JSON',
        timeout: 10000,
        data:{"daili_money":daili_money,"my_uid":my_uid,"daili_uid":daili_uid},
        success:function(dedd){
            if (dedd == 1) {
              alert('金额不能为空');
            }else if (dedd == 2) {
              alert('余额不足');
            }else if (dedd == 3) {
              alert('代理划分成功');
              window.location.reaload();
            }else{
              alert('代理划分失败');
            }
            
        }
    })
  }

  //ajax代理划分
  function sethuiyuans(my_uid){
    var huiyuan_money = $('#huiyuan_money').val();
    var huiyuan_uid = $('#huiyuan_uid').val();
    $.ajax({
        url:"/index.php/safe/sethuiyuan",
        type:'post',
        cache:false,
        dataType:'JSON',
        timeout: 10000,
        data:{"huiyuan_money":huiyuan_money,"my_uid":my_uid,"huiyuan_uid":huiyuan_uid},
        success:function(dedd){
            if (dedd == 1) {
              alert('金额不能为空');
            }else if (dedd == 2) {
              alert('余额不足');
            }else if (dedd == 3) {
              alert('会员划分成功');
              window.location.reaload();
            }else{
              alert('会员划分失败');
            }
            
        }
    })
  }
  
</script>
  
   
