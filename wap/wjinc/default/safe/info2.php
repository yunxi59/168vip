<!DOCTYPE html>
<html lang="en">
<head>
    
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
    <meta name="format-detection" content="telephone=no">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
<link rel="stylesheet" href="/skin/main/wap.css?ver=2.7" type="text/css">
<script src="/skin/js/jquery.min.js?ver=2.7"></script>
<script src="/skin/js/iscro-lot.js?ver=2.7"></script>
<script src="/skin/js/index.js?ver=2.7"></script>

    <script>
        isLogin = true;
    </script>
    <script>
    	$(function(){
    		var _padding = function()
    		{
    			try{
	    			var l = $("body>.header").height();
		    		if($("body>.lott-menu").length>0)
		    		{
		    			l += $("body>.lott-menu").height();
		    		}
		    		$("#wrapper_1").css("paddingTop",l+"px");
	    		}catch(e){}
	    		try{
		    		if($("body>.menu").length>0)
		    		{
		    			var l = $("body>.menu").height();
		    		}
		    		$("#wrapper_1").css("paddingBottom",l+"px");
	    		}catch(e){}
    		};
    		(function(){
    			_padding();
    		})();
    		$(window).bind("load",_padding);
    		
    	});
    </script>
    <!-- hide address bar -->
    <title>个人中心</title>
</head>
<body class="login-bg" onload="loaded()">
<div class="header">
    <div class="headerTop">
        <div class="ui-toolbar-left">
            <button id="reveal-left">reveal</button>
        </div>
        <h1 class="ui-toolbar-title">个人中心</h1>
        <div class=" header-icon">
            <a class="header-logout" href="/index.php/user/logout" id="indexlog_out"></a>
        </div>
    </div>
</div>
<div id="wrapper_1" class="scorllmain-content nobottom_bar" style="padding-top: 44px; padding-bottom: 61px;">
    <div class="sub_ScorllCont">
        <div class="mine-top">
            <div class="mine-head">
                <div class="mine-img"><img src="/images/message/geren_tou.png" alt=""></div>
                <div class="mine-name"><?=$this->user['username']?> [<?=$this->iff($this->user['type'], '代理', '会员')?>]</div>
                
            </div>
            <div class="mine-info">
                <ul>
                    <li>
                        <div class="mine-tit">￥<span id="balance"><?=$this->user['coin']?></span></div>
                        <p><a href="javascript:;">余额</a></p>
                    </li>
                    <li>
                        <div class="mine-tit"><span id="latewithdraw"><?=$this->user['score']?></span></div>
                        <p><a href="javascript:;">积分</a></p>
                    </li>
                    <li>
                        <a class="mine-refresh1" href="/index.php/safe/info" style="color: rgb(255, 255, 255);">刷新金额</a>
                    </li>
                    <!--<li>
                        <div class="mine-tit">￥<span id="wintotal">0</span></div>
                        <p><a href="/mine/betList.html?onlyWin=1">最近中奖金额</a></p>
                    </li>-->
                </ul>
            </div>
        </div>

                    <div class="mine-but">
                <a href="/index.php/cash/recharge" class="recharge"><img src="/images/message/geren_cz_01.png"></a>
                <a href="/index.php/cash/toCash" class="withdraw"><img src="/images/message/geren_tixian_01.png" alt=""></a>
            </div>
        
        <div class="mine-list">
            <ul>
                <!--<li>
                    <a href="/luckymoney/myList.html">
                        <img src="/images/message/geren_tubiao_58.png" alt="">我的红包
                    </a>
                </li>-->
                <li>
                    <a href="/index.php/record/search">
                        <img src="/images/message/geren_tubiao_13.png" alt="">游戏记录
                    </a>
                </li>
                <li>
                    <a href="/index.php/report/count">
                        <img src="/images/message/geren_tubiao_24.png" alt="">盈亏报表
                    </a>
                </li>
                <li>
                    <a href="/index.php/report/coin">
                        <img src="/images/message/geren_tubiao_26.png" alt="">账变明细
                    </a>
                </li>
                <li>
                    <a href="/index.php/cash/rechargeLog">
                        <img src="/images/message/geren_tubiao_04.png" alt="">充值记录
                    </a>
                </li>
                <li>
                    <a href="/index.php/cash/toCashLog">
                        <img src="/images/message/geren_tubiao_14.png" alt="">提款记录
                    </a>
                </li>
                <li>
                    <a href="/index.php/notice/info">
                        <img src="/images/message/geren_tubiao_06.png" alt="">个人消息<span id="count_unread"></span> <i class="red-icon" id="flag_unread" style="display:none;"></i>
                    </a>
                </li>
                <li>
                    <a href="/index.php/safe/passwd">
                        <img src="/images/message/geren_tubiao_30.png" alt="">修改密码
                    </a>
                </li>
                <?php if($this->user['type'] == 1){ ?>
                <li>
                    <a href="/index.php/safe/tuijian">
                        <img src="/images/message/tuijian.png" alt="">推荐码
                    </a>
                </li>
                <li>
                    <a href="/index.php/safe/huafen">
                        <img src="/images/message/huafen.png" alt="">划分
                    </a>
                </li>
                <li>
                    <a href="/index.php/safe/xiaji_order">
                        <img src="/images/message/order_jilu.png" alt="">下级订单记录
                    </a>
                </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>

<div class="menu" id="fixnav">
    <ul>
        <li><a class="menu-home" href="/"><img src="/skin/main/img/nav_1.png"></a></li>
        <li><a class="menu-color" href="/index.php/index/goucai"><img src="/skin/main/img/nav_2.png"></a></li>
        <li><a class="menu-lot" href="/index.php/index/historyList-1/0"><img src="/skin/main/img/nav_3.png"></a></li>
        <li><a class="menu-news" href="/index.php/index/zoushi"><img src="/skin/main/img/nav_4.png"></a></li>
        <li><a class="menu-my" href="/index.php/safe/info"><img src="/skin/main/img/nav_05.png"></a></li>
    </ul>
</div>

<input type="hidden" id="refresh_unread" value="0">


<style>
    .center {text-align: center}
</style>

<div class="beet-odds-tips round" id="tip_pop" style="display: none; height:130px;">
    <div class="beet-odds-info f100">
        <div class="beet-money" id="tip_pop_content" style="font-size: 120%; margin-top: 15px; color:#666;">
            号码选择有误
        </div>
    </div>
    <div class="beet-odds-info text-center">
        <button class="btn-que" style="width: 100%;" onclick="tipOk()"><span>确定</span></button>
    </div>
</div>

<div id="tip_bg" class="tips-bg" style="display: none;"></div>

<script>
    var func;
    function tipOk() {
        $('#tip_pop').hide();
        $('#tip_bg').hide();
        if (typeof(func) == "function"){
            func();
            func = "";
        }else{
            if (typeof(doTipOk) == "function") {
                doTipOk();
            }
        }
    }
    function msgAlert (msg,funcParm) {
        $('div#tip_pop_content').html(msg);
        $('div#tip_pop').show();
        $('div#tip_bg').show();
        func = (funcParm == ""||typeof(funcParm) != "function")?'':funcParm;
    }
</script>

<div class="beet-odds-tips round" id="confirm_pop" style="display: none; height:130px;">
    <div class="beet-odds-info f100">
        <div class="beet-money" id="confirm_pop_content" style="font-size: 120%; margin-top: 15px; color:#666;">
            号码选择有误
        </div>
    </div>
    <div class="beet-odds-info text-center">
        <button class="btn-que btn-que-no" onclick="confirmCancel()"><span id="confirm_pop_cancel">取消</span></button>
        <button class="btn-que" style="width: 50%;" onclick="confirmOk()"><span id="confirm_pop_ok">确定</span></button>
    </div>
</div>

<div id="confirm_bg" class="tips-bg" style="display: none;"></div>

<script>
$(function(){
	$("#reveal-left").click(function(){
		window.location.href="/index.php"
	});
});
</script>

<div id="_BD_chrome" channel="" version="" date=""></div></body></html>