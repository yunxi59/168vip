<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="format-detection" content="telephone=no" />
<?php $this->display('inc_skin_lr.php',0,'新用户注册'); ?>
</head>

<body style="overflow-x: hidden;width: 100%;">

<!-- <div id="header">
    <div id="header-inner">
        <div class="logo">用户注册</div>
    </div>
    
</div> -->
<style type="text/css">
  .rechargeBox {
    width: 100%;
    height: 44px;
    background: #fe962f;
    position: fixed;
    top: 0;
    left: 0;
    z-index: 10000;
}
.rechargeBox .left {
    background: url(/images/blank_01.png) no-repeat;
    background-size: 15px;
    height: 44px;
    width: 43px;
    display: block;
    margin-top: 8px;
    left: 5px;
    position: relative;
    z-index: 3;
}
</style>
<div class="rechargeBox">
<a href="/index.php" style="display: block;position: absolute;top:0;left: 0"><span class="left"></span></a><p style="width: 100%;text-align: center;font-size: 16px;color: #fff;height: 44px;line-height: 44px;letter-spacing: 4px;font-weight: 600;">用户注册</p>
  </div>
<div id="contentt" class="clearfix" style="margin-top: 50px;">
    <div class="pic"></div>
    <div class="form" style="text-align: center;">
        <div class="form-inner">
         <?php if($args[0] && $args[1]){
        
		$sql="select * from {$this->prename}links where lid=?";
		$linkData=$this->getRow($sql, $args[1]);
		$sql="select * from {$this->prename}members where uid=?";
		$userData=$this->getRow($sql, $args[0]);
	
		?>

		<form action="/index.php/user/registered" method="post" onajax="registerBeforSubmit" enter="true" call="registerSubmit" target="ajax">
        	<!-- <input type="hidden" name="parentId" value="<?=$args[0]?>" /> -->
            <input type="hidden" name="lid" value="<?=$linkData['lid']?>"  />
          	<dl>
            	<dt>用户名：</dt>
                <dd><input name="username" type="text" id="username" class="login-text" onkeyup="value=value.replace(/[^\w\.\/]/ig,'')"/></dd>
            </dl>
            <dl>
            	<dt>密  码：</dt>
                <dd><input name="password" type="password" id="password" class="login-text" /></dd>
            </dl>
             <dl>
            	<dt>确认密码：</dt>
                <dd><input name="cpasswd" type="password" id="cpasswd" class="login-text" /></dd>
            </dl>
             <dl>
            	<dt>Q  Q：</dt>
                <dd><input name="qq" type="test" id="qq" class="login-text" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"/></dd>
            </dl>
            <dl>
            	<dt>验证码：</dt>
                <dd style="position:relative;"><input name="vcode" type="text" class="login-text" /><div class="yzmNum"><img width="72" height="24" border="0" id="vcode" style="cursor:pointer;" align="absmiddle" src="/index.php/user/vcode/<?=$this->time?>" title="看不清楚，换一张图片" onclick="this.src='/index.php/user/vcode/'+(new Date()).getTime()"/></div></dd>
            </dl>
            <dl>
              <dt>推荐码：</dt>
                <dd style="position:relative;"><input name="tuijian" type="text" class="login-text" /></dd>
            </dl>
             <dl>
            	<dt class="hide"><input type="submit" value=""/></dt>
                <dd><button class="login-btn" tabindex="5" type="button" onclick="$(this).closest('form').submit()">注　册</button></dd>
            </dl>
          </form>
           <?php }else{?>
           <div style="text-align:center; line-height:50px; color:#FF0; font-size:20px; font-weight:bold;">链接失效！</div>
           <?php }?>
        </div>
    </div>
    <div class="clear"></div>
</div>
<div id="footer">Copyright © 中国福利彩票 </div>
</body>
</html>
