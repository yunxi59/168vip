<?php
	//echo $this->userType;
	$para=$_GET;
	// 时间限制
	if($para['fromTime'] && $para['toTime']){
		$wheres .= ' and b.ordertime between '.strtotime($para['fromTime']).' and '.strtotime($para['toTime']);
	}elseif($para['fromTime']){
		$wheres .= ' and b.ordertime>='.strtotime($para['fromTime']);
	}elseif($para['toTime']){
		$wheres .= ' and b.ordertime<'.strtotime($para['toTime']);
		
	}else{
		if($GLOBALS['fromTime'] && $GLOBALS['toTime']){
			$wheres .= ' and b.ordertime between '.$GLOBALS['fromTime'].' and '.$GLOBALS['toTime'].' ';
		}
	}
	
	// 投注状态限制
	if($para['state']=intval($para['state'])){
	switch($para['state']){
		case 1:
			// 已派奖
			$whereStr .= ' and b.zjCount>0';
		break;
		case 2:
			// 未中奖
			$whereStr .= " and b.zjCount=0 and b.lotteryNo!='' and b.isDelete=0";
		break;
		case 3:
			// 未开奖
			$whereStr .= " and b.lotteryNo=''";
		break;
		case 4:
			// 追号
			$whereStr .= ' and b.zhuiHao=1';
		break;
		case 5:
			// 撤单
			$whereStr .= ' and b.isDelete=1';
		break;
		}
	}
	// 用户名限制
		if($para['username'] && $para['username']!='用户名'){
			$para['username']=wjStrFilter($para['username']);
			if(!ctype_alnum($para['username'])) throw new Exception('用户名包含非法字符,请重新输入');
			 $wheres .= " and b.username like '%{$para['username']}%'";
		}
        //订单号查询
		if($para['betId'] && $para['betId']!='输入单号'){
			$para['betId']=wjStrFilter($para['betId']);
			if(!ctype_alnum($para['betId'])) throw new Exception('用户名包含非法字符,请重新输入');
			 $wheres .= " and b.codeid = {$para['betId']}";
		}
		if(is_numeric($para['kai'])){
			$wheres .= " and b.kai = {$para['kai']}";
		}
		if(is_numeric($para['balls'])) {
			$wheres .= " and b.ball = {$para['balls']}";
		}

		switch($para['utype']=intval($para['utype'])){
			case 1:
				//我自己
				$wheres .= " and b.uid={$this->user['uid']}";
				break;
			case 2:
				//直属下线
				$whereStr .= " and u.parentId={$this->user['uid']}";
				break;
			case 3:
				// 所有下级
				$whereStr .= " and concat(',',u.parents,',') like '%,{$this->user['uid']},%' and u.uid!={$this->user['uid']}";
				break;
			default:
				// 所有人
				$whereStr .= " and concat(',',u.parents,',') like '%,{$this->user['uid']},%'";
			break;
		}
	$sqls="select b.*, u.username from {$this->prename}orderssc b, {$this->prename}members u where b.uid=u.uid";
	$sqls.= $wheres;
	$sqls.=' order by id desc, ordertime desc';
	$datas=$this->getPage($sqls, $this->page, $this->pageSize);
	$params=http_build_query($para, '', '&');
	
	$modeName=array('2.00'=>'元', '0.20'=>'角', '0.02'=>'分');
?>
<div style="width: 100%;overflow-x: auto">
<table class='table_b' style="width: 1200px;overflow: hidden;">
	<thead>
		<tr class="table_b_th">
			<td>编号</td>
			<td>订单号</td>
            <td>用户</td>
			<td>投注时间</td>
			<td>彩种</td>
			<td>期号</td>
			<td>玩法</td>
			<td>倍数模式</td>
			<td>总额(元)</td>
			<td>投注号码</td>
			<td>开奖号码</td>
			<td>中奖金额</td>
			<td>状态</td>
		</tr>
	</thead>
	<tbody class="table_b_tr">
   <?php
         	if($datas['data']){
            foreach ($datas['data'] as $key => $value) {
              ?>
                    <tr>
                      <td>
	    		      	<?=$value['id']?>
	    		      </td>
	    		      <td>
	    		      	<?=$value['codeid']?>
	    		      </td>
	    		      <td>
	    		      	 <?php
	    		      	  if($value['username']){
	    		      	  	echo $value['username'];
	    		      	  }else{
	    		      	  	echo '--';
	    		      	  }
	    		      	 ?>
	    		      </td>
	    		      <td>
	    		      	<?=date('m-d H:i:s', $value['ordertime'])?>
	    		      </td>
	    		      <td>阿里分分彩</td>
	    		      <td><?=$value['qishu']?></td>
	    		      <td>
	    		      	<?php
	    		      	  if($value['type'] == 1){
	    		      	  	echo "大小";
	    		      	  }elseif($value['type'] == 2){
	    		      	  	echo "单双";
	    		      	  }elseif($value['type'] == 3){
	    		      	  	echo "龙虎";
	    		      	  }else{
	    		      	    echo "直选";
	    		      	  }
	    		      	?>
	    		      </td>
	    		      <td>
	    		      	<?=$value['mom']?>&nbsp;
	    		      	<?php
	    		      	    if($value['ball'] == 0){
	    		      	    	echo "[元]";
	    		      	    }elseif($value['ball'] == 1){
	    		      	    	echo "[角]";
	    		      	    }else{
	    		      	    	echo "[分]";
	    		      	    }
	    		      	?>	
	    		      </td>
	    		      <td><?=$value['money']?></td>
	    		      <td><?=$value['class']?></td>
	    		      <td>
	    		      	<?php
	    		      	  $sql = $this->getRow("select * from {$this->prename}datassc where qishu={$value['qishu']}") ;
	    		      	  if($sql['status'] == 1){
		    		      	  echo $sql['ball1'];echo "&nbsp";
		    		      	  echo $sql['ball2'];echo "&nbsp";
		    		          echo $sql['ball3'];echo "&nbsp";
		    		          echo $sql['ball4'];echo "&nbsp";
		    		          echo $sql['ball5'];echo "&nbsp";
	    		      	  }else{
	    		      	  	echo "--";
	    		      	  }
	    		      	?>
	    		      </td>
	    		      <td>
		    		      	<?php
		    		      	  if($value['kai']!=0){
		    		      	  	if($value['winmoney']){
		    		      	  	  echo $value['winmoney'];
		    		      	  	}else{
		    		      	  	  echo 0;
		    		      	  	}
		    		      	  }else{
		    		      	  	 echo "未开奖";
		    		      	  } 
		    		      	?>	
		    		  </td>
	    		      <td style="color:#009900">	
	    		      	<?php
	    		      	   if($value['kai'] == 0){
	    		      	   	  echo "未开奖";
	    		      	   }elseif($value['kai'] == 1){
                              echo "已派奖";
	    		      	   }else{
	    		      	   	  echo "未中奖";
	    		      	   }
	    		      	?>
	    		      </td>
	    		   </tr>
              <?php
            }
         }else{
         	?>
            <tr><td colspan="14">暂无投注信息</td></tr>
            <?php 
         }
   ?>		

	</tbody>
</table>

</div>
<?php 
	$this->display('inc_page.php',0,$datas['total'],$this->pageSize, "/index.php/{$this->controller}/{$this->action}-{page}/?$params");
?>